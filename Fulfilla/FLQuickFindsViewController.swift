//
//  FLQuickFindsViewController.swift
//  Fulfilla
//
//  Created by Appzoc on 13/11/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit
import Kingfisher
import Alamofire

enum FLSegueFrom: Int{
    case quickFinds
    case categories
    case featuredServices
}

class FLQuickFindsViewController: UIViewController {

    
    @IBOutlet var containerView: UIView!
    @IBOutlet var listingCollection: UICollectionView!
    @IBOutlet var listingCollectionHeight: NSLayoutConstraint!
    @IBOutlet var containerViewHeight: NSLayoutConstraint!
    
    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet weak var searchField: UITextField!
    
    var objectList:[QuickFindsModel] = []
    var ListingArray:[BusinessListingModel] = []
    
    var segueFrom: FLSegueFrom = .quickFinds
    let serviceImagesDataSource = [#imageLiteral(resourceName: "travel"),#imageLiteral(resourceName: "nurse"),#imageLiteral(resourceName: "reaest"),#imageLiteral(resourceName: "spa"),#imageLiteral(resourceName: "food"),#imageLiteral(resourceName: "wedding"),#imageLiteral(resourceName: "house-keeping"),#imageLiteral(resourceName: "auto"),#imageLiteral(resourceName: "advertising"),#imageLiteral(resourceName: "maintanance"),#imageLiteral(resourceName: "educa"),#imageLiteral(resourceName: "24x7")]
    let serviceNamesDataSource = ["Travel & Tourism","Health & Medical","Real Estate", "Spa & Beauty", "Food & Beverages", "Wedding Services","House Keeping","Automobile","Advertising","Maintenance","Education & Training","24/7 Services"]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        listingCollection.register(UINib(nibName: "FLHomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FLHomeCollectionViewCell")
        setUpInterface()
        
        
        
        //Code for performing search and passing data to ListingController
//        DispatchQueue.global().sync {
//            performSearch()
//            let businessListingViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLBusinsessListingViewController") as! FLBusinsessListingViewController
//            businessListingViewController.objectList = ListingArray
//            businessListingViewController.externalDataSource = true
//            self.navigationController?.pushViewController(businessListingViewController, animated: true)
//        }
       
    }

    
    func setUpInterface(){
        
        switch segueFrom {
        case .quickFinds:
            self.titleLabel.text = "Quick Finds"
        case .categories:
            self.titleLabel.text = "Categories"
        case .featuredServices:
            self.titleLabel.text = "Featured Services"
            
        }
        
        containerView.setShadow(withColor: UIColor.gray, opacity: 0.7, andShadowRadius: 4)
        
    }
    
//    func fetchData(){
//        var dataResponse:[String:Any] = [:]
//        JSONParser.parseData(url: "", parameters: ["user_id":""], httpMethod: .get, completion: {isComplete,jsonData in
//            dataResponse = jsonData!})
//        let data = dataResponse["Data"] as! [String:Any]
//        let categoryList = data["categoryList"] as! [[String:Any]]
//        
//        for item in categoryList{
//            let object = QuickFindsModel(source: item)
//            objectList.append(object)
//        }
//        
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        objectList.removeAll()
        categorySearchLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //categorySearchLoad()
        changeHeightOfContainerViewAndCollectionView()
    }
    
    override func viewDidLayoutSubviews(){
        
        changeHeightOfContainerViewAndCollectionView()
    }
    
    func changeHeightOfContainerViewAndCollectionView(){
        
        listingCollectionHeight.constant = listingCollection.contentSize.height
        containerViewHeight.constant = listingCollectionHeight.constant + listingCollection.frame.origin.y + 25
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func searchCategoryAction(_ sender: Any) {
    }
    
    
    @IBAction func notificationAction(_ sender: Any) {
        
        let notificationViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLNotificationViewController") as! FLNotificationViewController
        self.navigationController?.pushViewController(notificationViewController, animated: true)
        
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    //Call to perform Quick Finds search
    func performSearch(){
        if let text = searchField.text{
            if FLUnwraper.isNotEmpty(string: text){
                //Listing API POST Type Call
                let callParameter = ["category":"\(text)","subcategory":"\(text)"]
                var dataResponse:[String:Any] = [:]
                    JSONParser.parseData(url: "http://fulfilla.dev.webcastle.in/service-list", parameters: callParameter, completion: {isComplete,jsonValue in
                        dataResponse = jsonValue!
                })
                let data = dataResponse["Data"] as! [String:Any]
                let services = data["services"] as! [[String:String]]
                
                for item in services{
                    
                    let modelItem = BusinessListingModel(with: item)
                    ListingArray.append(modelItem)
                }
                //Set transferredBusinessListing property as ListingArray at segue
            }
        }
    }
    
    
    func categorySearchLoad(){
       let parameters:[String:Any?] = ["user_id":"110",
                                        "search_key":"",
                                        "location_latitude":"",
                                        "location_longitude":"",
                                        "category":"",
                                        "subcategory":"",
                                        "isfulltime":false,
                                        "favourite":false]
        
        JSONParser.parseData(url: "http://fulfilla.dev.webcastle.in/api/category-list", parameters: ["user_id": "133"], httpMethod: HTTPMethod.get, completion: {
            isComplete, jsonParsed in
            //print(jsonParsed)
            var tempObjectList:[QuickFindsModel] = []
            if let jsonData = jsonParsed{
                //print(jsonData)
                let data = jsonData["Data"] as? [[String:Any]]
                for item in data!{
                    let object = QuickFindsModel(source: item)
                    print("\n\nItem\n\(object)")
                    //let object = BusinessListingModel(with: item)
                    tempObjectList.append(object)
                }
                //self.objectList.removeAll()
                self.objectList = tempObjectList
                DispatchQueue.main.async {
                    
                 self.listingCollection.reloadData()
                    self.listingCollection.layoutIfNeeded()
                    self.viewDidLayoutSubviews()
                }
                
            }
            
        })
    }
}



// Mark: - Listing collection view data source and delegates

extension FLQuickFindsViewController: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("objectlist",objectList.count)
        return objectList.count//Change to QuickFindsModelList count
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cell.appearanceStyle = .twoDimentionalYAxis
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let collectionCell = collectionView.dequeueReusableCell(withReuseIdentifier:"FLHomeCollectionViewCell", for: indexPath as IndexPath) as? FLHomeCollectionViewCell
        
        //Comment to remove
       // collectionCell?.serviceImage.image = serviceImagesDataSource[indexPath.row]
       // collectionCell?.serviceName.text = serviceNamesDataSource[indexPath.row]
        
        if let imageLink = URL(string: objectList[indexPath.row].imageURL){
        let imageResource = ImageResource(downloadURL: imageLink, cacheKey: "\(imageLink)")
        collectionCell?.serviceImage.kf.setImage(with: imageResource)
        }
        collectionCell?.serviceName.text = objectList[indexPath.row].name
        
        collectionCell?.layer.shadowColor = UIColor.lightGray.cgColor
        collectionCell?.layer.cornerRadius = 4
        collectionCell?.layer.shadowOffset = CGSize(width: 0, height: 0.0)
        collectionCell?.layer.shadowRadius = 2.0
        collectionCell?.layer.shadowOpacity = 1.0
        collectionCell?.layer.masksToBounds = false

        /*  Comment out
        collectionCell?.serviceName.text = objectList[indexPath.row].name
        collectionCell?.serviceImage.kf.setImage(with: ImageResource(downloadURL: objectList[indexPath.row].imageURL))
        collectionCell?.serviceImage.kf.indicatorType = .activity
        */
        
        return collectionCell!
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let businessListingViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLBusinsessListingViewController") as! FLBusinsessListingViewController
        self.navigationController?.pushViewController(businessListingViewController, animated: true)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(2 - 1))
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(2))
        let height = Int(Double(size) * 0.825)
        return CGSize(width: size, height: height)
        
    }
    
    
    
}
