//
//  FLMyAccountViewController.swift
//  Fulfilla
//
//  Created by Appzoc on 13/11/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit

class FLMyAccountViewController: UIViewController {

    
    @IBOutlet var profileImage: UIImageView!
    
    @IBOutlet var profileName: UILabel!
    
    @IBOutlet var profileEmail: UILabel!
    
    @IBOutlet var editButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpInterface()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpInterface(){
//        profileImage.layer.cornerRadius = profileImage.frame.size.width / 2
        profileImage.clipsToBounds = true
        editButton.setShadow(withColor: UIColor.gray, opacity: 0.7, andShadowRadius: 3)
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated : true)
    }
    
    @IBAction func notificationAction(_ sender: Any) {
        let notificationViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLNotificationViewController") as! FLNotificationViewController
        self.navigationController?.pushViewController(notificationViewController, animated: true)
    }
    
    @IBAction func searchAction(_ sender: Any) {
        let searchViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLSearchViewController") as! FLSearchViewController
        self.navigationController?.pushViewController(searchViewController, animated: true)
    }
    
    
    @IBAction func myAppointmentsAction(_ sender: Any) {
        let myAppointmentsViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLMyAppointmentsViewController") as! FLMyAppointmentsViewController
        self.navigationController?.pushViewController(myAppointmentsViewController, animated: true)
    }
    
    
    @IBAction func myMessagesAction(_ sender: Any) {
        let segueViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLFavouritesListingViewController") as! FLFavouritesListingViewController
        
        segueViewController.isChat = true
        self.navigationController?.pushViewController(segueViewController, animated: true)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
