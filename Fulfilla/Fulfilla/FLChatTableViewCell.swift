//
//  FLChatTableViewCell.swift
//  Fulfilla
//
//  Created by Appzoc on 21/11/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit

class FLChatTableViewCell: UITableViewCell {

    @IBOutlet var profileImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
        self.profileImage.cornerRadius = self.profileImage.frame.width / 2
    }
    
}
