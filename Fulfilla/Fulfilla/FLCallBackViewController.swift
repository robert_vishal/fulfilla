//
//  FLCallBackViewController.swift
//  Fulfilla
//
//  Created by Appzoc on 07/11/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit

class FLCallBackViewController: UIViewController {

    @IBOutlet var containerView: UIView!
    @IBOutlet var phoneNumberContainer: UIView!
    @IBOutlet var bodyView: UIView!
    @IBOutlet var countryCodeLabel: UILabel!
    @IBOutlet var phoneNumberTextField: UITextField!
    
    var phoneNumberString: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        FLAnimations.popUpAnimation(applyToView: containerView, animationStyle: .zoomOut)
        setUpInterface()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpInterface(){
        
        bodyView.setFLShadow()
        
        phoneNumberContainer.layer.borderWidth = 1
        phoneNumberContainer.layer.borderColor = UIColor.lightGray.cgColor
        phoneNumberContainer.layer.cornerRadius = 4
        phoneNumberContainer.layer.masksToBounds = true

    }
    
    @IBAction func closeAction(_ sender: Any) {

            self.dismiss(animated: false, completion: nil)
        
    }

    
    @IBAction func submitAction(_ sender: Any) {
        
        
        if FLUnwraper.isNotEmpty(string: FLUnwraper.unwrap(string: phoneNumberString)){
            if FLUnwraper.isValid(phone: phoneNumberString){
                print("valid phone")
                // make a web call here
                self.dismiss(animated: false, completion: nil)

            }else{
                print("invalid phone")
                phoneNumberTextField.text = "Invalid phone number"
                phoneNumberTextField.textColor = .red
            }
        }else{
            
//            FLAnimations.shakeView(view: phoneNumberContainer, isHorizontally: true)
            FLAnimations.showAlert(withTitle: "", message: "Invalid phone number", andPresentOn: self)
        }
        

    }
    
}



// Mark: - Handling textfield delegates

extension FLCallBackViewController: UITextFieldDelegate {
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {

        FLUnwraper.invalidStringCheck(withTextField: textField, assigningString: phoneNumberString)
        
        return true
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        phoneNumberString = textField.text!.trimmingCharacters(in: .whitespaces)
        if phoneNumberString.trimmingCharacters(in: .whitespaces).isEmpty{
            phoneNumberString = ""
            textField.text = nil
        }
        
    }
    
}

