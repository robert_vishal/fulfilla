//
//  FLAssistant.swift
//  Fulfilla
//
//  Created by Appzoc on 12/10/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import Foundation
import UIKit
import GoogleSignIn

public class FLAssistant{
    
    static var storyBoard = UIStoryboard(name: "Main", bundle: nil)
    
    class var appDelegate: AppDelegate{ get {return UIApplication.shared.delegate as! AppDelegate}}
    
    
    class func extractUserInfo(ofGoogleUser user: GIDGoogleUser) -> FLUser { return FLUser(Google: user) }
    
    class func extractUserInfo(ofFacebookUser user: [String : Any]) -> FLUser { return FLUser(Facebook: user) }
    
    class func addChild(viewController childVC: UIViewController , fromParentViewController parentVC: UIViewController) {
        parentVC.addChildViewController(childVC)
        parentVC.view.addSubview(childVC.view)
        childVC.didMove(toParentViewController: parentVC)
    }
    
    class func removeChild(viewController childVC: UIViewController) {
        childVC.willMove(toParentViewController: nil)
        childVC.view.removeFromSuperview()
        childVC.removeFromParentViewController()
    }
    
 }


// Mark: - setting semaphores

//let semaphore = DispatchSemaphore(value: 0)


// Mark:- FLUnwrapper for unwrapping strings integers double values and validations

public class FLUnwraper{
    
    
    class func unwrap(string: String?)-> String{
        guard let nonEmptyValue = string else {
            return ""
        }
        return nonEmptyValue
    }
    
    
    class func unwrap(integer: Int?)-> Int{
        guard let nonEmptyValue = integer else {
            return 0
        }
        return nonEmptyValue
    }
    
    
    class func unwrap(double: Double?)-> Double{
        guard let nonEmptyValue = double else {
            return 0.0
        }
        return nonEmptyValue
    }
    
    

    
    class func isValid(email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTester = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTester.evaluate(with: email)
    }
    
    
    class func isValid(phone: String)->Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "+0123456789").inverted
        let inputString = phone.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        
        if phone == filtered {
            let phoneRegex = "[235689][0-9]{6}([0-9]{3})?"
            let phoneNumberTester = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
            return  phoneNumberTester.evaluate(with: phone)
        }else {
            return false
        }
    }
    
    class func isValid(digits: String)->Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "+0123456789").inverted
        let inputString = digits.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  digits == filtered
    }
    
    
    class func isNotEmpty(string: String) -> Bool
    {
        guard !string.trimmingCharacters(in: .whitespaces).isEmpty else {
            return false
        }
        return true
    }

    class func invalidStringCheck(withTextField: UITextField, assigningString: String){
        DispatchQueue.main.async {
            withTextField.textColor = UIColor.FLpopupGray
            withTextField.text = nil
            if !assigningString.trimmingCharacters(in: .whitespaces).isEmpty{
                withTextField.text = assigningString.trimmingCharacters(in: .whitespaces)
            }
        }
        
    }

}










//public class func setAttributedPlaceHolderFor(textField: UITextField?, withplaceHolderText placeHolderText: String, stringSymbol: String, andSpace space: String) {
//    
//    if let textField = textField{
//        let passwordAttriburedString = NSMutableAttributedString(string: placeHolderText)
//        let spaceBetweenTextandSymbol = NSMutableAttributedString(string: space)
//        
//        let asterix = NSAttributedString(string: stringSymbol, attributes: [NSForegroundColorAttributeName: UIColor.red])
//        passwordAttriburedString.append(spaceBetweenTextandSymbol)
//        passwordAttriburedString.append(asterix)
//        textField.attributedPlaceholder = passwordAttriburedString
//    }
//    
//}


