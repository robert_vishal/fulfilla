//
//  FLMyAppointmentsTableViewCell.swift
//  Fulfilla
//
//  Created by Appzoc on 14/11/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit

class FLMyAppointmentsTableViewCell: UITableViewCell {

    @IBOutlet var baseView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpInterface()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpInterface(){
        baseView.setFLShadow()
    }
    
    
}
