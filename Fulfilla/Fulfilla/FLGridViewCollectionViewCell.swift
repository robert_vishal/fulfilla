//
//  FLGridViewCollectionViewCell.swift
//  Fulfilla
//
//  Created by Appzoc on 24/10/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit
import Kingfisher

class FLGridViewCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet var baseView: UIView!
    
    @IBOutlet var profileImageView: UIImageView!
    @IBOutlet var ratingView: FloatRatingView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var reviewLabel: UILabel!
    
    var messageContact:String = ""
    var contactNumber:String = ""
    var messageComposer:MessageComposer = MessageComposer(number:"")
    var delegate:FLBusinsessListingViewController!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        messageComposer = MessageComposer(number: messageContact)
        setUpCellUI()
        setRating()
    }
    
    func setUpCellUI(){
//        profileImageView.layer.cornerRadius = profileImageView.frame.width / 2
//        baseView.setShadow(scale: true, withColor: UIColor.darkGray, opacity: 0.7, andRadius: 4)
    }
    
    func setRating(){
        // Reset float rating view's background color
        ratingView.backgroundColor = UIColor.clear
        
        /** Note: With the exception of contentMode, type and delegate,
         all properties can be set directly in Interface Builder **/
        //        ratingView.delegate = self
        ratingView.contentMode = UIViewContentMode.scaleAspectFit
        ratingView.type = .wholeRatings
        ratingView.minRating = 0
        
    }
    
    
    @IBAction func sendMessageAction(_ sender: UIButton) {
        if (messageComposer.canSendText()) {
            let messageComposeVC = messageComposer.configuredMessageComposeViewController()
            delegate.present(messageComposeVC, animated: true, completion: nil)
            // presentViewController(messageComposeVC, animated: true, completion: nil)
        } else {
            // Let the user know if his/her device isn't able to send text messages
            let errorAlert = UIAlertView(title: "Cannot Send Text Message", message: "Your device is not able to send text messages.", delegate: self, cancelButtonTitle: "OK")
            errorAlert.show()
        }
    }
    
    @IBAction func callAction(_ sender: UIButton) {
        print(contactNumber)
        guard let number = URL(string: "tel://\(contactNumber)") else { return }
        UIApplication.shared.canOpenURL(number)
        UIApplication.shared.open(number, options: [:], completionHandler: nil)
    }
    
    func configureCell(with dataObject: FLProperty?){
        if let propertyData = dataObject{
            print("configureCell : ListView :",dataObject as Any)
            
            self.profileImageView.sd_setImage(with: URL(string: propertyData.iconPicture), placeholderImage: UIImage(named: ""))
            
            self.titleLabel.text = propertyData.propertyTitle
            self.ratingView.minRating = propertyData.ratingCount
            self.locationLabel.text = propertyData.location
            self.reviewLabel.text = "\(propertyData.reviewCount)" + " Reviews"
            
        }
        
    }
    
    func setUpCell(with modelObject:BusinessListingModel){
        self.titleLabel.text = modelObject.title
        if let rating = modelObject.rating as? Int{
        self.ratingView.minRating = rating
        }
        self.locationLabel.text = modelObject.location
        self.reviewLabel.text = "\(modelObject.reviewCount) Reviews"
        
        let username = "dev"
        let password = "dev"
        let loginString = String(format: "%@:%@", username, password)
        let loginData = loginString.data(using: String.Encoding.utf8)!
        let base64LoginString = loginData.base64EncodedString()
        
        let modifier = AnyModifier { request in
            var r = request
            r.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
            return r
        }
        
        if let url = URL(string: modelObject.profileImageLink){
        let image = ImageResource(downloadURL: url, cacheKey: modelObject.profileImageLink)
        
        self.profileImageView.kf.setImage(with: image, placeholder: nil, options: [.requestModifier(modifier)], progressBlock: nil, completionHandler: nil)
        }else{
            print("Invalid Image Source")
        }
        //self.profileImageView.kf.setImage(with: modelObject.profileImage)
    }
    

}
