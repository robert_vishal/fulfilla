//
//  Enums.swift
//  Fulfilla
//
//  Created by Appzoc on 12/10/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import Foundation

enum FLLoginType :String {
    case normal
    case google
    case facebook
    case logout
}

enum FLPropertyType: Int {
    case listview
}

enum FLSegueType: Int{
    case businessListing
    case mapListing
    case fromLanding
    case none
}


