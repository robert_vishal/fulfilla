
//
//  FLRegisterViewController.swift
//  Fulfilla
//
//  Created by Appzoc on 12/10/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit

class FLRegisterViewController: UIViewController {
    
    
    @IBOutlet weak var userNameTextField: UITextField!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var mobileTextField: UITextField!
    
    @IBOutlet weak var landlineTextField: UITextField!
    
    @IBOutlet weak var checkImageView: UIImageView!
    

    //    @IBOutlet var confirmPasswordTextField: UITextField!
    //    @IBOutlet var confirmPasswordLabel: UILabel!
    
    var segueType: FLSegueType = .businessListing
    var isChecked: Bool = false
    let userNameAttriburedString = NSMutableAttributedString(string: "  Username")
    let emailAttriburedString = NSMutableAttributedString(string: "  Email")
    let passwordAttriburedString = NSMutableAttributedString(string: "  Password")
    //    let confirmPasswordAttriburedString = NSMutableAttributedString(string: "  Confirm Password")
    let mobileAttriburedString = NSMutableAttributedString(string: "  Mobile Number")
    let landlineAttriburedString = NSMutableAttributedString(string: "  Landline Number")
    
    let asterixSymbol = NSAttributedString(string: " *", attributes: [NSForegroundColorAttributeName: UIColor.red])
    var textFieldValueHolder:String?
    let userNamePlaceHolder = "   Username *"
    let emailPlaceHolder = "   Email *"
    let passwordPlaceHolder = "   Password *"
    //    let confirmPasswordPlaceHolder = "   Confirm Password *"
    let mobileNumberPlaceHolder = "  Mobile Number *"
    let landlineNumberPlaceHolder = "  Landline Number *"
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpInterface()
        
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        resignAllFirstResponders()
    }
    
    
    func setUpInterface(){
        userNameTextField.layer.borderColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.3).cgColor
        userNameTextField.layer.borderWidth = 1.5
        userNameTextField.layer.cornerRadius = 3
        
        emailTextField.layer.borderColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.3).cgColor
        emailTextField.layer.borderWidth = 1.5
        emailTextField.layer.cornerRadius = 3

        passwordTextField.layer.borderColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.3).cgColor
        passwordTextField.layer.borderWidth = 1.5
        passwordTextField.layer.cornerRadius = 3

        mobileTextField.layer.borderColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.3).cgColor
        mobileTextField.layer.borderWidth = 1.5
        mobileTextField.layer.cornerRadius = 3

        landlineTextField.layer.borderColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.3).cgColor
        landlineTextField.layer.borderWidth = 1.5
        landlineTextField.layer.cornerRadius = 3

    }
    
    //To call in Login/Sign Up action
    func checkFields() -> Bool{
        //Check field validity
        var allValid:[Bool] = [false,false,false,false,false]
        if let username = userNameTextField.text{
            if FLUnwraper.isNotEmpty(string: username){
                allValid[0] = true
            }
        }
        if let email = emailTextField.text{
            if FLUnwraper.isValid(email: email){
                allValid[1] = true
            }
        }
        if let password = passwordTextField.text{
            if FLUnwraper.isNotEmpty(string: password){
                allValid[2] = true
            }
        }
        if let mobile = mobileTextField.text{
            if FLUnwraper.isValid(phone: mobile){
                allValid[3] = true
            }
        }
        if let landline = landlineTextField.text{
            if FLUnwraper.isValid(phone: landline){
                allValid[4] = true
            }
        }
        
        var count:Int = 0
        for instance in allValid{
            if instance{
                count += 1
            }
        }
        if count == 5 {
            return true
        }else{
            return false
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func checkAction(_ sender: Any) {
        if isChecked{
            checkImageView.image = UIImage(named: "uncheck")
            isChecked = false
        }else{
            checkImageView.image = UIImage(named: "check")
            isChecked = true
        }
        
         }
    
    
    @IBAction func termsAction(_ sender: Any) {
        
    }
    
    @IBAction func loginNowAction(_ sender: Any) {
        resignAllFirstResponders()
        loginType = .normal
        //        FLWebService
        UserDefaults.standard.set(loginType.rawValue, forKey: "loggedType")
        if segueType == .mapListing {
            segueToMapListing()
        }else{
            segueToBusinessListing()
        }
    }
    
    func segueToBusinessListing(){
        let businessListingViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLBusinsessListingViewController") as! FLBusinsessListingViewController
        self.navigationController?.pushViewController(businessListingViewController, animated: true)
    }
    
    func segueToMapListing(){
        let mapListingViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLMapListingViewController") as! FLMapListingViewController
        self.navigationController?.pushViewController(mapListingViewController, animated: true)
    }
    
    @IBAction func registeredUser(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
}





// Mark: - Receiving textfield delegates and making animations

extension FLRegisterViewController: UITextFieldDelegate{
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        //        UIView.animate(withDuration: 0.25, delay: 0.0, options: [], animations: {
        //
        //            self.view.frame.origin.y -= 125
        //
        //        }, completion: { (finished: Bool) in
        //
        //        })
        
        textFieldValueHolder = ""
        textFieldValueHolder = textField.text
        textField.attributedPlaceholder = nil
       // animateTextField(duringShouldBeginEditing: textField)
        textField.textColor = UIColor.black
        textField.contentVerticalAlignment = .center
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //animateTextField(duringDidBeginEditing: textField)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        resignAllFirstResponders()
        return true
    }
    
    func resignAllFirstResponders(){
        userNameTextField.resignFirstResponder()
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        //        confirmPasswordTextField.resignFirstResponder()
        mobileTextField.resignFirstResponder()
        landlineTextField.resignFirstResponder()
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        //
        //        UIView.animate(withDuration: 0.25, delay: 0.0, options: [], animations: {
        //
        //            self.view.frame.origin.y += 125
        //
        //        }, completion: { (finished: Bool) in
        //
        //        })
        //animateTextField(duringShouldEndEditing: textField)
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
/*
    
    // Animation functions for textfield
    
    func  animateTextField(duringDidBeginEditing textField: UITextField){
        if textField == userNameTextField{
            if textFieldValueHolder != nil && textFieldValueHolder! != userNamePlaceHolder{
                textField.text = textFieldValueHolder
            }
        }else if textField == emailTextField {
            if textFieldValueHolder != nil && textFieldValueHolder! != emailPlaceHolder{
                textField.text = textFieldValueHolder
                
            }
        }else if textField == passwordTextField{
            if textFieldValueHolder != nil && textFieldValueHolder! != passwordPlaceHolder{
                textField.text = textFieldValueHolder
                
            }
            //        }else if textField == confirmPasswordTextField{
            //            if textFieldValueHolder != nil && textFieldValueHolder! != confirmPasswordPlaceHolder{
            //                textField.text = textFieldValueHolder
            //
            //            }
        }else if textField == mobileTextField {
            if textFieldValueHolder != nil && textFieldValueHolder! != mobileNumberPlaceHolder {
                textField.text = textFieldValueHolder
                
            }
        }else{
            if textFieldValueHolder != nil && textFieldValueHolder! != landlineNumberPlaceHolder {
                textField.text = textFieldValueHolder
                
            }
        }
    }
    
    
    func animateTextField(duringShouldEndEditing textField :UITextField) {
        
        if textField == userNameTextField{
            if userNameTextField.text == nil || userNameTextField.text == "" || userNameTextField.text == userNamePlaceHolder{
                userNameTextField.contentVerticalAlignment = .top
                UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: .beginFromCurrentState, animations: ({
                    self.userNameLabel.alpha = 1
                }), completion: { _ in
                })
                self.userNameTextField.attributedPlaceholder = nil

            }else{
                self.userNameLabel.isHidden = false
                
            }
        }else if textField == emailTextField{
            if emailTextField.text == nil || emailTextField.text == "" || emailTextField.text == emailPlaceHolder{
                emailTextField.contentVerticalAlignment = .top
                UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: .beginFromCurrentState, animations: ({
                    self.emailLabel.alpha = 1
                }), completion: { _ in
                })
                self.emailTextField.attributedPlaceholder = nil
                UIView.animate(withDuration: 0.4, animations: {
                    self.emailLabel.alpha = 0.0
                    self.emailLabel.isHidden = true
                    self.emailTextField.attributedPlaceholder = self.emailAttriburedString
                    
                })
            }else{
                self.emailLabel.isHidden = false
                
            }
        }else if textField == passwordTextField{
            if passwordTextField.text == nil || passwordTextField.text! == "" || passwordTextField.text! == passwordPlaceHolder {
                passwordTextField.contentVerticalAlignment = .top
                UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: .beginFromCurrentState, animations: ({
                    self.passwordLabel.alpha = 1
                }), completion: { _ in
                })
                self.passwordTextField.attributedPlaceholder = nil
                UIView.animate(withDuration: 0.4, animations: {
                    self.passwordLabel.alpha = 0.0
                    self.passwordLabel.isHidden = true
                    self.passwordTextField.attributedPlaceholder = self.passwordAttriburedString
                    
                })
            }else{
                self.passwordLabel.isHidden = false
                
            }
            //        }else if textField == confirmPasswordTextField{
            //            if confirmPasswordTextField.text == nil || confirmPasswordTextField.text == "" || confirmPasswordTextField.text == confirmPasswordPlaceHolder {
            //                confirmPasswordTextField.contentVerticalAlignment = .top
            //                UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: .beginFromCurrentState, animations: ({
            //                    self.confirmPasswordLabel.alpha = 1
            //                }), completion: { _ in
            //                })
            //                self.confirmPasswordTextField.attributedPlaceholder = nil
            //                UIView.animate(withDuration: 0.4, animations: {
            //                    self.confirmPasswordLabel.alpha = 0.0
            //                    self.confirmPasswordLabel.isHidden = true
            //                    self.confirmPasswordTextField.attributedPlaceholder = self.confirmPasswordAttriburedString
            //
            //                })
            //            }else{
            //                self.confirmPasswordLabel.isHidden = false
            //
            //            }
        }else if textField == mobileTextField{
            if mobileTextField.text == nil || mobileTextField.text == "" || mobileTextField.text == mobileNumberPlaceHolder {
                mobileTextField.contentVerticalAlignment = .top
                UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: .beginFromCurrentState, animations: ({
                    self.mobileNumberLabel.alpha = 1
                }), completion: { _ in
                })
                self.mobileTextField.attributedPlaceholder = nil
                UIView.animate(withDuration: 0.4, animations: {
                    self.mobileNumberLabel.alpha = 0.0
                    self.mobileNumberLabel.isHidden = true
                    self.mobileTextField.attributedPlaceholder = self.mobileAttriburedString
                    
                })
            }else{
                self.mobileNumberLabel.isHidden = false
                
            }
        }else{
            if landlineTextField.text == nil || landlineTextField.text == "" || landlineTextField.text == landlineNumberPlaceHolder {
                landlineTextField.contentVerticalAlignment = .top
                UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: .beginFromCurrentState, animations: ({
                    self.landlineNumberLabel.alpha = 1
                }), completion: { _ in
                })
                self.landlineTextField.attributedPlaceholder = nil
                UIView.animate(withDuration: 0.4, animations: {
                    self.landlineNumberLabel.alpha = 0.0
                    self.landlineNumberLabel.isHidden = true
                    self.landlineTextField.attributedPlaceholder = self.landlineAttriburedString
                    
                })
            }else{
                self.landlineNumberLabel.isHidden = false
                
            }
        }
        
    }
    
    
    func animateTextField(duringShouldBeginEditing textField :UITextField) {
        
        if textField == userNameTextField{
            userNameLabel.isHidden = false
            if userNameTextField.text == nil || userNameTextField.text == userNamePlaceHolder || userNameTextField.text == "" {
                UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: .beginFromCurrentState, animations: ({
                    self.userNameLabel.alpha = 0
                }), completion: { _ in
                })
                UIView.animate(withDuration: 0.4, animations: {
                    self.userNameLabel.alpha = 1.0
                })
                
            }
            
        }else if textField ==  emailTextField{
            emailLabel.isHidden = false
            if emailTextField.text == nil || emailTextField.text == "" || emailTextField.text == emailPlaceHolder{
                UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: .beginFromCurrentState, animations: ({
                    self.emailLabel.alpha = 0
                }), completion: { _ in
                })
                UIView.animate(withDuration: 0.4, animations: {
                    self.emailLabel.alpha = 1.0
                    
                })
            }
            
        }else if textField ==  passwordTextField{
            passwordLabel.isHidden = false
            if passwordTextField.text == nil || passwordTextField.text! == "" || passwordTextField.text! == passwordPlaceHolder {
                UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: .beginFromCurrentState, animations: ({
                    self.passwordLabel.alpha = 0
                }), completion: { _ in
                })
                UIView.animate(withDuration: 0.4, animations: {
                    self.passwordLabel.alpha = 1.0
                    
                })
            }
            
            //        }else if textField ==  confirmPasswordTextField{
            //            confirmPasswordLabel.isHidden = false
            //            if confirmPasswordTextField.text == nil || confirmPasswordTextField.text == "" || confirmPasswordTextField.text == confirmPasswordPlaceHolder {
            //                UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: .beginFromCurrentState, animations: ({
            //                    self.confirmPasswordLabel.alpha = 0
            //                }), completion: { _ in
            //                })
            //                UIView.animate(withDuration: 0.4, animations: {
            //                    self.confirmPasswordLabel.alpha = 1.0
            //
            //                })
            //            }
            
        }else if textField == mobileTextField{
            mobileNumberLabel.isHidden = false
            if mobileTextField.text == nil || mobileTextField.text == "" || mobileTextField.text == mobileNumberPlaceHolder {
                UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: .beginFromCurrentState, animations: ({
                    self.mobileNumberLabel.alpha = 0
                }), completion: { _ in
                })
                UIView.animate(withDuration: 0.4, animations: {
                    self.mobileNumberLabel.alpha = 1.0
                    
                })
            }
        }else{
            landlineNumberLabel.isHidden = false
            if landlineTextField.text == nil || landlineTextField.text == "" || landlineTextField.text == landlineNumberPlaceHolder {
                UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: .beginFromCurrentState, animations: ({
                    self.landlineNumberLabel.alpha = 0
                }), completion: { _ in
                })
                UIView.animate(withDuration: 0.4, animations: {
                    self.landlineNumberLabel.alpha = 1.0
                    
                })
            }
        }
        
    }
  */  
    
}




