//
//  FLMapListingViewController.swift
//  Fulfilla
//
//  Created by Appzoc on 14/10/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit
import MapKit
class FLMapListingViewController: UIViewController {

    
    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet var listingMapView: MKMapView!
    var locationsMarker: MarkerWithImage!
    let locationManager = CLLocationManager()
    var locationsMarkerView:MKPinAnnotationView!
    var segueType: FLSegueType = .none
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpInterface()
        
    }

    func setUpInterface(){
        listingMapView.setShadow(withColor: UIColor.black, opacity: 0.5, andShadowRadius: 2)
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        listingMapView.delegate = self
        listingMapView.mapType = MKMapType.standard
        listingMapView.showsUserLocation = true
        
        self.listingMapView.isUserInteractionEnabled = true
//        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTapOnMap))
//        self.listingMapView.addGestureRecognizer(gestureRecognizer)

        
        
        setDefaultLocation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        
        if segueType == .fromLanding {
            let landingViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLLandingViewController") as! FLLandingViewController
            self.transitionStyle = .leftToRight
            self.navigationController?.pushViewController(landingViewController, animated: false)
        }else{
            self.navigationController?.popViewController(animated: true)

        }
        
        
        
    }

    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension FLMapListingViewController: MKMapViewDelegate, CLLocationManagerDelegate{
    
    //MARK: - Custom Marker
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        
        
        if (annotation is MKUserLocation) {
            return nil
        }
        let reuseIdentifier = "customMarker"
        var markerView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)
        
        if markerView == nil {
            markerView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
            markerView?.canShowCallout = true
        } else {
            markerView?.annotation = annotation
        }
        
        let customMarker = annotation as! MarkerWithImage
        markerView?.image = UIImage(named: customMarker.imageName)
        return markerView

    }
    
    func setMarkerOnMap(markerCoordinate: CLLocationCoordinate2D ){
        locationsMarker = MarkerWithImage()
        locationsMarker.imageName = "marker"
        locationsMarker.coordinate = markerCoordinate
        locationsMarker.title = "Title"
        locationsMarker.subtitle = "subtitle"
        
        locationsMarkerView = MKPinAnnotationView(annotation: locationsMarker, reuseIdentifier: "customMarker")
        listingMapView.addAnnotation(locationsMarkerView.annotation!)
        
    }
    
    func setDefaultLocation(){
        // Qatar location 25.354756, 51.210509
        
        let locationDummyArray = [["lat":25.342345, "lon":50.945464],["lat":25.235561, "lon":51.049834],["lat":22.922795, "lon":51.376479],["lat":25.782745, "lon":51.139130],["lat":27.844878, "lon":51.464568],["lat":23.364684, "lon":50.950957],["lat":25.088286, "lon":51.088286],["lat":25.280273,"lon":51.376678],["lat":25.993410,"lon":51.242095],["lat":26.1049297,"lon":51.3482599]]
        
        
        let location2D = CLLocationCoordinate2D(latitude: 25.354756, longitude: 51.210509)
        let center = location2D
        let region = MKCoordinateRegionMake(center, MKCoordinateSpan(latitudeDelta: 0.85, longitudeDelta: 0.85))
        listingMapView.setRegion(region, animated: true)
        for locationItem in locationDummyArray{
            
            setMarkerOnMap(markerCoordinate: CLLocationCoordinate2D(latitude: locationItem["lat"]!, longitude: locationItem["lon"]!))

        }
//        setMarkerOnMap(markerCoordinate: location2D)
    }
    
//    func handleTapOnMap(gestureReconizer: UITapGestureRecognizer) {
//        
//        let touchedLocation = gestureReconizer.location(in: listingMapView)
//        let touchedCoordinate = listingMapView.convert(touchedLocation,toCoordinateFrom: listingMapView)
//        
//        // Change marker position/Add marker
//        
//        
//            self.setMarkerOnMap(markerCoordinate: touchedCoordinate)
//        
//        
//        
//    }



    
    
    
}
