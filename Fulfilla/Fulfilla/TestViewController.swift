//
//  TestViewController.swift
//  Fulfilla
//
//  Created by Appzoc on 21/10/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit

class TestViewController: UIViewController, UITextFieldDelegate{
    @IBOutlet var testTextField: HoshiTextField!

    @IBOutlet var customTextField: UITextField!
    
    @IBOutlet var customTextLabel: UILabel!
    let passwordAttriburedString = NSMutableAttributedString(string: "     User Name")
    let asterix = NSAttributedString(string: "*", attributes: [NSForegroundColorAttributeName: UIColor.red])
    
    override func viewDidLoad() {
        super.viewDidLoad()

        customTextField.delegate = self
        
      
        passwordAttriburedString.append(asterix)
        
        self.customTextField.attributedPlaceholder = passwordAttriburedString
        customTextLabel.isHidden = true

        // Do any additional setup after loading the view.
    }

    func setUpUI(){
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("textFieldShouldBeginEditing")
        self.customTextField.attributedPlaceholder = nil
        customTextLabel.isHidden = false
        animateViewsForTextEntry()
        
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        // animate label
        print("textFieldDidBeginEditing")
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("textFieldShouldReturn")
        customTextField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("textFieldShouldEndEditing")
        if customTextField.text == nil || customTextField.text == "" {
            customTextLabel.isHidden = false
            UIView.animate(withDuration: 0.4, animations: {
                self.customTextLabel.isHidden = true
                
            })
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("hjkhkfhfhsfh::::::::",textField.text as Any)
         if customTextField.text == nil || customTextField.text == "" {
            animateViewsForTextNull()
        }
    }
    
    
    func animateViewsForTextNull(){
        UIView.animate(withDuration: 0.3, animations: {
            self.customTextField.attributedPlaceholder = self.passwordAttriburedString
        })

    }
    
     func animateViewsForTextEntry() {
       
            UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: .beginFromCurrentState, animations: ({
                self.customTextLabel.alpha = 0
            }), completion: { _ in
            })
        
        
        
        UIView.animate(withDuration: 0.4, animations: {
            self.customTextLabel.alpha = 1.0
        })
        
    }
    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
