//
//  FLEnquirayFormViewController.swift
//  Fulfilla
//
//  Created by Appzoc on 07/11/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit

class FLEnquirayFormViewController: UIViewController {
    
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var phoneTextField: UITextField!
    @IBOutlet var commentsTextView: UITextView!
    
    @IBOutlet var sendButton: UIButton!
     @IBOutlet var containerView: UIView!
    
    @IBOutlet var scrollContainer: UIScrollView!
//    var defaultPoint:CGFloat = 0.0
    @IBOutlet var topViewContraintHeightWidth: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpInterface()
//        self.scrollContainer.delegate = self
//        defaultPoint = topViewContraintHeightWidth.constant
    }

    
    func setUpInterface(){
        containerView.setFLShadow()
        
        nameTextField.layer.borderWidth = 1
        nameTextField.layer.borderColor = UIColor.lightGray.cgColor
        nameTextField.layer.cornerRadius = 5
        nameTextField.layer.masksToBounds = true
        
        emailTextField.layer.borderWidth = 1
        emailTextField.layer.borderColor = UIColor.lightGray.cgColor
        emailTextField.layer.cornerRadius = 5
        emailTextField.layer.masksToBounds = true
        
        phoneTextField.layer.borderWidth = 1
        phoneTextField.layer.borderColor = UIColor.lightGray.cgColor
        phoneTextField.layer.cornerRadius = 5
        phoneTextField.layer.masksToBounds = true
        
        commentsTextView.layer.borderWidth = 1
        commentsTextView.layer.borderColor = UIColor.lightGray.cgColor
        commentsTextView.layer.cornerRadius = 5
        commentsTextView.layer.masksToBounds = true
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.transitionStyle = .leftToRight
        self.dismiss(animated: false, completion: nil)

    }
    
    /*
     user_id
     service_id
     name
     email
     phone
     message
 */

    @IBAction func sendAction(_ sender: Any) {
        if let name = nameTextField.text, let email = emailTextField.text, let phoneNumber = phoneTextField.text, let comments = commentsTextView.text{
            if FLUnwraper.isValid(email: email) && FLUnwraper.isValid(phone: phoneNumber){
                let parameters = ["user_id":"",
                                  "service_id":"",
                                  "name":"\(name)",
                                  "email":"\(email)",
                                  "phone":"\(phoneNumber)",
                                  "message":"\(comments)"]
                var data:[String:Any] = [:]
                JSONParser.parseData(url: "", parameters: parameters, completion: {isComplete,jsonData in
                    data = jsonData!}) as! [String:Any]
                print(data)
            }
        }
        
    }


}

//    var scrolledAbove: Bool = false
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        print(topViewContraintHeightWidth.constant,"scrolling",scrollView.contentOffset.y)
//
//        if scrollView.contentOffset.y > 50{
//            topViewContraintHeightWidth.constant += 1
//            scrolledAbove = true
//        }else{
//
//            if defaultPoint < topViewContraintHeightWidth.constant{
//
//                topViewContraintHeightWidth.constant -= 1
//
//            }
//            if  scrollView.contentOffset.y == 0{
//                UIView.animate(withDuration: 0.2, animations: {
//                    self.topViewContraintHeightWidth.constant = self.defaultPoint
//                })
//            }
//
//
//        }
//    }

