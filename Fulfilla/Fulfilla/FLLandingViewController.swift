//
//  FLLandingViewController.swift
//  Fulfilla
//
//  Created by Appzoc on 11/10/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit
import DropDown
import GooglePlaces

class FLLandingViewController: UIViewController {
    
    @IBOutlet weak var placeSearchTextField: UITextField!
    
    @IBOutlet weak var autoCompleteFieldAnchor: UIView!
    
    @IBOutlet weak var placesAutoCompleteView: UIView!
    
    
    @IBOutlet var searchTextField: UITextField!
    @IBOutlet var selectLocationButton: UIButton!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var nextButton: UIButton!
    
    
    var searchText: String = ""
    let locationDropDown = DropDown()
    var locationsList = [""]
    
    var dropDownList:[String] = []
    let dropDown = DropDown()
    var fetcher: GMSAutocompleteFetcher?
    
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var resultView: UITextView?
    
    @IBAction func placesSearchEdited(_ sender: UITextField) {
        fetcher?.sourceTextHasChanged(placeSearchTextField.text!)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.setUpUserInterface()
        self.setUpPlacesAutocomplete()
        
        setUpAutoComplete()
        setUpDropDown()
        DropDown.startListeningToKeyboard()
        // self.navigationController?.viewControllers.removeAll()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        setUpPlacesAutocomplete()
        //DropDown.stopListeningToKeyboard()
    }
    
    func setUpUserInterface(){
        nextButton.layer.cornerRadius = nextButton.frame.size.width / 2
        nextButton.clipsToBounds = true
        self.searchTextField.delegate = self
        setUpDropDownUserInterface()
    }
    
    func setUpDropDownUserInterface() {
        
        let appearance = DropDown.appearance()
        
        appearance.cellHeight = 30
        appearance.backgroundColor = UIColor.white
        appearance.selectionBackgroundColor = UIColor.lightGray
        appearance.separatorColor = UIColor.lightGray
        appearance.cornerRadius = 5
        appearance.shadowColor = UIColor.darkGray
        appearance.shadowOpacity = 0.9
        appearance.shadowRadius = 2
        appearance.animationduration = 0.25
        appearance.textColor = .black
        locationDropDown.dismissMode = .onTap
        locationDropDown.direction = .bottom
        locationDropDown.anchorView = locationLabel
        locationDropDown.bottomOffset = CGPoint(x: 0, y: locationLabel.bounds.height)
        
        locationsList.removeAll()
        
        let dummyLocations = ["city_name": ["Doha","Al Khor","Al Wakrah","Al Rayyan","Al Daayen","Al Shahaniya","Umm Salal","Al Shamal"]]
        
        locationsList = dummyLocations["city_name"]!
        
        locationDropDown.dataSource = locationsList
        
        
        // Action triggered on selection
        locationDropDown.selectionAction = { [unowned self] (index, item) in
            self.locationLabel.text = item
        }
        
        
    }
    
    @IBAction func selectLocationAction(_ sender: Any) {
        
        locationDropDown.show()
        
    }
    
    
    
    @IBAction func nextButtonAction(_ sender: Any) {
        
        if let currentLoggedType = UserDefaults.standard.string(forKey: "loggedType") {
            print("Landing Page : loggedType: ",currentLoggedType)
            if currentLoggedType == FLLoginType.logout.rawValue {
                
                let loginViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLLoginViewController") as! FLLoginViewController
                loginViewController.segueType = .businessListing
                self.navigationController?.pushViewController(loginViewController, animated: true)
                
            }else{
                
                let businessListingViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLBusinsessListingViewController") as! FLBusinsessListingViewController
                self.navigationController?.pushViewController(businessListingViewController, animated: true)
                
            }
        }else{
            print("Landing Page : loggedType: nil")
        }
        
        
    }
    
    @IBAction func searchUsingMapsAction(_ sender: Any) {
        
        if let currentLoggedType = UserDefaults.standard.string(forKey: "loggedType") {
            print("Landing Page : loggedType: ",currentLoggedType)
            
            if currentLoggedType == FLLoginType.logout.rawValue {
                let loginViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLLoginViewController") as! FLLoginViewController
                loginViewController.segueType = .mapListing
                self.navigationController?.pushViewController(loginViewController, animated: true)
            }else{
                let mapListingViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLMapListingViewController") as! FLMapListingViewController
                mapListingViewController.segueType = .fromLanding
                self.navigationController?.pushViewController(mapListingViewController, animated: true)
            }
            
        }else{
            print("Landing Page : loggedType: nil")
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}


// Mark: - searchTextField delegate functions

extension FLLandingViewController: UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        searchText = FLUnwraper.unwrap(string: textField.text)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        searchText = ""
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        searchTextField.resignFirstResponder()
        return true
    }
    
}
/*
 
 import UIKit
 
 import GooglePlaces
 
 import DropDown
 
 
 
 class ViewController: UIViewController {
 
 @IBOutlet weak var dropDownAnchor: UIView!
 
 
 
 @IBOutlet weak var PlaceTextField: UITextField!
 
 
 
 
 
 @IBAction func editingChanged(_ sender: UITextField) {
 
 fetcher?.sourceTextHasChanged(PlaceTextField.text!)
 
 }
 
 
 
 var dropDownList:[String] = []
 
 let dropDown = DropDown()
 
 
 
 var fetcher: GMSAutocompleteFetcher?
 
 
 
 func setUpDropDown(){
 
 dropDown.anchorView = dropDownAnchor
 
 dropDown.dataSource = dropDownList
 
 dropDown.selectionAction = {
 
 index,item in
 
 self.PlaceTextField.text = item
 
 self.dropDown.hide()
 
 }
 
 }
 
 
 
 override func viewDidLoad() {
 
 super.viewDidLoad()
 
 setUpDropDown()
 
 self.view.backgroundColor = UIColor.blue
 
 self.edgesForExtendedLayout = .all
 
 fetcher = GMSAutocompleteFetcher()
 
 fetcher?.delegate = self
 
 }
 
 
 
 }
 
 */

extension FLLandingViewController: GMSAutocompleteFetcherDelegate {
    
    func setUpAutoComplete(){
        fetcher = GMSAutocompleteFetcher()
        
        fetcher?.delegate = self
    }
    
    func setUpDropDown(){
        
        dropDown.anchorView = autoCompleteFieldAnchor
        dropDown.dataSource = dropDownList
        dropDown.direction = .bottom
        dropDown.selectionAction = {
            index,item in
            self.placeSearchTextField.text = item
            self.dropDown.hide()
        }
        
    }
    
    func didAutocomplete(with predictions: [GMSAutocompletePrediction]) {
        dropDownList.removeAll()
        for prediction in predictions {
            dropDownList.append(prediction.attributedPrimaryText.string)
            //print("\n",prediction.attributedFullText.string)
            //print("\n",prediction.attributedPrimaryText.string)
            //print("\n********")
        }
        dropDown.dataSource = dropDownList
        dropDown.show()
    }
    
    
    func didFailAutocompleteWithError(_ error: Error) {
        //resultText?.text = error.localizedDescription
        print(error.localizedDescription)
    }
}



extension FLLandingViewController: GMSAutocompleteResultsViewControllerDelegate {
    
    func setUpPlacesAutocomplete() {
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        resultsViewController?.tableCellBackgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.6)
        resultsViewController?.view.backgroundColor = UIColor.clear
        
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        
        
        placesAutoCompleteView.addSubview((searchController?.searchBar)!)
        print("Places AutoComplete Working")
        
        searchController?.searchBar.sizeToFit()
        
        searchController?.searchBar.barTintColor = UIColor.white
        //searchController?.searchBar.tintColor = UIColor.yellow
        
        searchController?.hidesNavigationBarDuringPresentation = false
        definesPresentationContext = false
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didAutocompleteWith place: GMSPlace) {
        searchController?.isActive = false
        // Do something with the selected place.
        searchController?.searchBar.text = place.name
        searchController?.searchBar.setImage(UIImage(), for: .search, state:.normal)
        searchController?.searchBar.showsScopeBar = false
        searchController?.obscuresBackgroundDuringPresentation = false
        
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Place attributions: \(place.attributions)")
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didFailAutocompleteWithError error: Error){
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}




/*
 
 extension FLLandingViewController: GMSAutocompleteResultsViewControllerDelegate {
 
 func setUpPlacesAutocomplete() {
 resultsViewController = GMSAutocompleteResultsViewController()
 resultsViewController?.delegate = self
 resultsViewController?.tableCellBackgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.6)
 resultsViewController?.view.backgroundColor = UIColor.clear
 
 searchController = UISearchController(searchResultsController: resultsViewController)
 searchController?.searchResultsUpdater = resultsViewController
 
 
 placesAutoCompleteView.addSubview((searchController?.searchBar)!)
 print("Places AutoComplete Working")
 
 searchController?.searchBar.sizeToFit()
 
 searchController?.searchBar.barTintColor = UIColor.white
 //searchController?.searchBar.tintColor = UIColor.yellow
 
 searchController?.hidesNavigationBarDuringPresentation = false
 definesPresentationContext = false
 }
 
 func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
 didAutocompleteWith place: GMSPlace) {
 searchController?.isActive = false
 // Do something with the selected place.
 searchController?.searchBar.text = place.name
 searchController?.searchBar.setImage(UIImage(), for: .search, state:.normal)
 searchController?.searchBar.showsScopeBar = false
 searchController?.obscuresBackgroundDuringPresentation = false
 
 print("Place name: \(place.name)")
 print("Place address: \(place.formattedAddress)")
 print("Place attributions: \(place.attributions)")
 }
 
 func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
 didFailAutocompleteWithError error: Error){
 // TODO: handle the error.
 print("Error: ", error.localizedDescription)
 }
 
 // Turn the network activity indicator on and off again.
 func didRequestAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
 UIApplication.shared.isNetworkActivityIndicatorVisible = true
 }
 
 func didUpdateAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
 UIApplication.shared.isNetworkActivityIndicatorVisible = false
 }
 }
 
 
 
 */

/*
 
 import UIKit
 import GooglePlaces
 
 class ViewController3: UIViewController {
 
 var resultsViewController: GMSAutocompleteResultsViewController?
 var searchController: UISearchController?
 var resultView: UITextView?
 
 
 var displayController:UISearchController = UISearchController()
 
 override func viewDidLoad() {
 super.viewDidLoad()
 
 resultsViewController = GMSAutocompleteResultsViewController()
 resultsViewController?.delegate = self
 
 //        searchController = UISearchController(searchResultsController: resultsViewController)
 //        searchController?.searchResultsUpdater = resultsViewController
 
 displayController = UISearchController(searchResultsController: resultsViewController)
 displayController.searchResultsUpdater = resultsViewController
 
 
 
 -----------------------**********************------------------------------
 
 let subView = UIView(frame: CGRect(x: 0, y: 65.0, width: 250.0, height: 45.0))
 subView.addSubview(displayController.searchBar)
 
 
 -----------------------**********************------------------------------
 
 
 displayController.searchBar.tintColor = UIColor.yellow
 view.addSubview(subView)
 
 
 displayController.searchBar.sizeToFit()
 displayController.hidesNavigationBarDuringPresentation = false
 //        subView.addSubview((searchController?.searchBar)!)
 //        searchController?.searchBar.tintColor = UIColor.yellow
 //       // searchController?.searchBar.barTintColor = UIColor.blue
 
 //        view.addSubview(subView)
 
 //        searchController?.searchBar.sizeToFit()
 //        searchController?.hidesNavigationBarDuringPresentation = false
 
 // When UISearchController presents the results view, present it in
 // this view controller, not one further up the chain.
 definesPresentationContext = true
 }
 }
 
 // Handle the user's selection.
 extension ViewController3: GMSAutocompleteResultsViewControllerDelegate {
 func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
 didAutocompleteWith place: GMSPlace) {
 searchController?.isActive = false
 // Do something with the selected place.
 print("Place name: \(place.name)")
 print("Place address: \(place.formattedAddress)")
 print("Place attributions: \(place.attributions)")
 displayController.searchBar.text = place.name
 }
 
 func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
 didFailAutocompleteWithError error: Error){
 // TODO: handle the error.
 print("Error: ", error.localizedDescription)
 }
 
 // Turn the network activity indicator on and off again.
 func didRequestAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
 UIApplication.shared.isNetworkActivityIndicatorVisible = true
 }
 
 func didUpdateAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
 UIApplication.shared.isNetworkActivityIndicatorVisible = false
 }
 }
 
 
 
 
 */










/*
 
 
 
 //
 //  FLLandingViewController.swift
 //  Fulfilla
 //
 //  Created by Appzoc on 11/10/17.
 //  Copyright © 2017 Appzoc. All rights reserved.
 //
 
 import UIKit
 import DropDown
 
 class FLLandingViewController: UIViewController {
 
 @IBOutlet var searchTextField: UITextField!
 @IBOutlet var selectLocationButton: UIButton!
 @IBOutlet var locationLabel: UILabel!
 @IBOutlet var nextButton: UIButton!
 
 var searchText: String = ""
 let locationDropDown = DropDown()
 var locationsList = [""]
 
 
 override func viewDidLoad() {
 super.viewDidLoad()
 // Do any additional setup after loading the view, typically from a nib.
 self.setUpUserInterface()
 DropDown.startListeningToKeyboard()
 //        self.navigationController?.viewControllers.removeAll()
 }
 
 override func viewDidDisappear(_ animated: Bool) {
 
 DropDown.stopListeningToKeyboard()
 }
 
 func setUpUserInterface(){
 nextButton.layer.cornerRadius = nextButton.frame.size.width / 2
 nextButton.clipsToBounds = true
 self.searchTextField.delegate = self
 setUpDropDownUserInterface()
 }
 
 func setUpDropDownUserInterface() {
 
 let appearance = DropDown.appearance()
 
 appearance.cellHeight = 30
 appearance.backgroundColor = UIColor.white
 appearance.selectionBackgroundColor = UIColor.lightGray
 appearance.separatorColor = UIColor.lightGray
 appearance.cornerRadius = 5
 appearance.shadowColor = UIColor.darkGray
 appearance.shadowOpacity = 0.9
 appearance.shadowRadius = 2
 appearance.animationduration = 0.25
 appearance.textColor = .black
 locationDropDown.dismissMode = .onTap
 locationDropDown.direction = .bottom
 locationDropDown.anchorView = locationLabel
 locationDropDown.bottomOffset = CGPoint(x: 0, y: locationLabel.bounds.height)
 
 locationsList.removeAll()
 
 let dummyLocations = ["city_name": ["Doha","Al Khor","Al Wakrah","Al Rayyan","Al Daayen","Al Shahaniya","Umm Salal","Al Shamal"]]
 
 locationsList = dummyLocations["city_name"]!
 
 locationDropDown.dataSource = locationsList
 
 
 // Action triggered on selection
 locationDropDown.selectionAction = { [unowned self] (index, item) in
 self.locationLabel.text = item
 }
 
 
 }
 
 @IBAction func selectLocationAction(_ sender: Any) {
 
 locationDropDown.show()
 
 }
 
 
 
 @IBAction func nextButtonAction(_ sender: Any) {
 
 if let currentLoggedType = UserDefaults.standard.string(forKey: "loggedType") {
 print("Landing Page : loggedType: ",currentLoggedType)
 if currentLoggedType == FLLoginType.logout.rawValue {
 
 let loginViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLLoginViewController") as! FLLoginViewController
 loginViewController.segueType = .businessListing
 self.navigationController?.pushViewController(loginViewController, animated: true)
 
 }else{
 
 let businessListingViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLBusinsessListingViewController") as! FLBusinsessListingViewController
 self.navigationController?.pushViewController(businessListingViewController, animated: true)
 
 }
 }else{
 print("Landing Page : loggedType: nil")
 }
 
 
 }
 
 @IBAction func searchUsingMapsAction(_ sender: Any) {
 
 if let currentLoggedType = UserDefaults.standard.string(forKey: "loggedType") {
 print("Landing Page : loggedType: ",currentLoggedType)
 
 if currentLoggedType == FLLoginType.logout.rawValue {
 let loginViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLLoginViewController") as! FLLoginViewController
 loginViewController.segueType = .mapListing
 self.navigationController?.pushViewController(loginViewController, animated: true)
 }else{
 let mapListingViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLMapListingViewController") as! FLMapListingViewController
 mapListingViewController.segueType = .fromLanding
 self.navigationController?.pushViewController(mapListingViewController, animated: true)
 }
 
 }else{
 print("Landing Page : loggedType: nil")
 }
 
 }
 
 
 override func didReceiveMemoryWarning() {
 super.didReceiveMemoryWarning()
 // Dispose of any resources that can be recreated.
 }
 
 
 }
 
 
 // Mark: - searchTextField delegate functions
 
 extension FLLandingViewController: UITextFieldDelegate{
 
 func textFieldDidEndEditing(_ textField: UITextField) {
 searchText = FLUnwraper.unwrap(string: textField.text)
 }
 
 func textFieldDidBeginEditing(_ textField: UITextField) {
 searchText = ""
 }
 func textFieldShouldReturn(_ textField: UITextField) -> Bool {
 searchTextField.resignFirstResponder()
 return true
 }
 
 }
 */

