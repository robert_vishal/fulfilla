//
//  FLImageSlideCollectionViewCell.swift
//  Fulfilla
//
//  Created by Appzoc on 24/11/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit

class FLImageSlideCollectionViewCell: UICollectionViewCell {

    @IBOutlet var slidingImage: UIImageView!
    
    @IBOutlet var slidingTitle: UILabel!
    
    @IBOutlet var slidingSubtitle: UILabel!
    
    @IBOutlet var slidingOffer: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
