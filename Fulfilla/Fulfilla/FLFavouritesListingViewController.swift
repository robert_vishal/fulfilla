//
//  FLFavouritesListingViewController.swift
//  Fulfilla
//
//  Created by Appzoc on 25/10/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit
import Alamofire

class FLFavouritesListingViewController: UIViewController {

    
    
    @IBOutlet var listingTable: UITableView!
    
    @IBOutlet var titleLabel: UILabel!
    
    var isChat: Bool = false
    var favouritesList:[BusinessListingModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if isChat{
            self.titleLabel.text = "Chats"
        }else{
            self.titleLabel.text = "Favourites"

        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
      //  fetchFavouritesList()
       // listingTable.reloadData()
        loadFavouritesList()
    }

    @IBAction func notificationAction(_ sender: Any) {
        let notificationViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLNotificationViewController") as! FLNotificationViewController
        self.navigationController?.pushViewController(notificationViewController, animated: true)
    }

    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func loadFavouritesList(){
        let parameters:[String:Any?] = ["user_id":"110",
                                        "search_key":"",
                                        "location_latitude":"",
                                        "location_longitude":"",
                                        "category":"",
                                        "subcategory":"",
                                        "isfulltime":false,
                                        "favourite":false]
        
        JSONParser.parseData(url: "http://fulfilla.dev.webcastle.in/api/service-list", parameters: ["user_id": "133"], httpMethod: HTTPMethod.post, completion: {
            isComplete, jsonParsed in
            var tempObjectList:[BusinessListingModel] = []
            if let jsonData = jsonParsed{
                let data = jsonData["Data"] as? [[String:Any]]
                for item in data!{
                    let object = BusinessListingModel(with: item)
                    tempObjectList.append(object)
                }
                self.favouritesList = tempObjectList
                self.resfreshAfterAPICall()
            }
            
        })
        
    }
    
    func resfreshAfterAPICall(){
        self.listingTable.reloadData()
       // listingTableView.reloadData()
       // listingCollectionView.reloadData()
    }
    
    func fetchFavouritesList(){
        JSONParser.parseData(url: "http://fulfilla.dev.webcastle.in/api/service-list", parameters: ["user_id":"133",                                                                                                "search_key":"",                                                                                                "location_latitude":"",                                                                                                "location_longitude":"",                                                                                                "category":"",                                                                                                "subcategory":"",                                                                                                "isfulltime":"",                                                                                                "favourite":""], completion: {isComplete,parsedJSON in
            
            if let jsonData = parsedJSON {
                let errorCode = jsonData["ErrorCode"]
                let message = jsonData["Message"]
                let tempDataCover = jsonData["Data"]
                let dataCover = tempDataCover as? [[String:String]]
                if let data = dataCover{
                print("\n\n\n\n\n Data Cover \n\n\n",data)
                for element in data{
                    var modelObject = BusinessListingModel(id: element["service_id"]!, title: element["service_title"]!, location: element["service_location"]!, rating: element["service_rating"]!, city: element["service_city"]!, reviewCount: element["service_review_count"]!, contactPhone: element["service_contact_phone"]!, contactMessage: element["service_contact_Message"]!, latitude: element["service_latitude"]!, longitude: element["service_longitude"]!, profileImageLink: element["service_profile_image"]!)
                    self.favouritesList.append(modelObject)
                }
                    
                }
                
            }else{
                print("Invalid Response")
            }

        })
    }
   

}


// Mark:- Receiving tableview delegates

extension FLFavouritesListingViewController: UITableViewDataSource, UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favouritesList.count
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        cell.appearanceStyle = .threeDimenstional
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 106
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        
        if isChat{
            
            var tableCell = tableView.dequeueReusableCell(withIdentifier: "FLChatTableViewCell") as? FLChatTableViewCell
           // FLChatTableViewCell
            if tableCell == nil {
                tableView.register(UINib(nibName: "FLChatTableViewCell", bundle: nil), forCellReuseIdentifier: "FLChatTableViewCell")
                print("registering tableviewcell")
                tableCell = tableView.dequeueReusableCell(withIdentifier: "FLChatTableViewCell") as? FLChatTableViewCell
            }
            
            print("chat")
            
//            tableCell?.titleLabel.text = "Your account validity lasts on tomorrow Please update your account."
//            tableCell?.timeLabel.text = "09.39 am"
//            tableCell?.notificationButton.setTitle("Chat Now", for: .normal)
//            tableCell?.profileImage.image = UIImage(named: "sample")
            
            tableCell?.selectionStyle = .none
            return tableCell!
        }else{
            var tableCell = tableView.dequeueReusableCell(withIdentifier: "FLListViewBusinessListingTableViewCell") as? FLListViewBusinessListingTableViewCell
            
            if tableCell == nil {
                tableView.register(UINib(nibName: "FLListViewBusinessListingTableViewCell", bundle: nil), forCellReuseIdentifier: "FLListViewBusinessListingTableViewCell")
                print("registering tableviewcell")
                tableCell = tableView.dequeueReusableCell(withIdentifier: "FLListViewBusinessListingTableViewCell") as? FLListViewBusinessListingTableViewCell
            }
            
            tableCell?.setUpCell(with: favouritesList[indexPath.row])
            
            print("not chat")
            
            
            /*
            tableCell?.titleLabel.text = "Rumailah Hospital"
            tableCell?.profileImageView.image = UIImage(named: "samplefavourite")
            
            tableCell?.selectionStyle = .none
            */
            
            return tableCell!
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isChat{
            
        }else{
            let detailsBusinessViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLDetailsBusinessViewController") as! FLDetailsBusinessViewController
            
            self.navigationController?.pushViewController(detailsBusinessViewController, animated: true)
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let tableCell  = tableView.cellForRow(at: indexPath as IndexPath)
        tableCell!.contentView.backgroundColor = UIColor.FLtableSelection
    }
    
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell  = tableView.cellForRow(at: indexPath as IndexPath)
        cell!.contentView.backgroundColor = .clear
    }
    
}
