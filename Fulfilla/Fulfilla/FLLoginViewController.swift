//
//  FLLoginViewController.swift
//  Fulfilla
//
//  Created by Appzoc on 12/10/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit
import GoogleSignIn
import FacebookLogin
import FacebookCore

var loginType: FLLoginType = .normal
class FLLoginViewController: UIViewController {

    @IBOutlet var baseView: UIView!
    @IBOutlet var userNameTextField: ImageTextField!
    @IBOutlet var passwordTextField: ImageTextField!
    
    @IBOutlet var googleLoginLabel: UILabel!
    
    @IBOutlet var facebookLoginLabel: UILabel!
    
    var segueType: FLSegueType = .businessListing
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        userNameTextField.delegate = self
        passwordTextField.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func loginNowAction(_ sender: Any) {
        
        if segueType == .mapListing {
            segueToMapListing()
        }else{
            segueToBusinessListing()
        }
        
        
    }
    
    
    @IBAction func forgotPasswordAction(_ sender: UIButton) {
        let getEmailViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLGetEmailViewController") as! FLGetEmailViewController
        //self.navigationController?.pushViewController(getEmailViewController, animated: true)
        getEmailViewController.modalPresentationStyle = .overCurrentContext
        self.present(getEmailViewController, animated: false, completion: nil)
        //FLGetEmailViewController
    }
    
    
    func segueToBusinessListing(){
        let businessListingViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLBusinsessListingViewController") as! FLBusinsessListingViewController
        self.navigationController?.pushViewController(businessListingViewController, animated: true)
    }
    
    func segueToMapListing(){
        let mapListingViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLMapListingViewController") as! FLMapListingViewController
        mapListingViewController.segueType = .fromLanding
        self.navigationController?.pushViewController(mapListingViewController, animated: true)
    }
    
    
    @IBAction func googleLoginAction(_ sender: Any) {
        
        print("googleLoginAction")
        googleLoginLabelAnimation()
        loginType = .google
        GIDSignIn.sharedInstance().clientID = "520683271711-6r9chbrgbqeikn4peb6f6ogm7k5hcvpn.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().signIn()
    }
    
    
    @IBAction func facebookLoginAction(_ sender: Any) {
        facebookLoginLabelAnimation()
        loginType = .facebook
        let facebookLoginManager = LoginManager()
        facebookLoginManager.logIn([.email], viewController: self, completion: {
            (LoginResult) in
            
            switch LoginResult{
            case .success(grantedPermissions: _, declinedPermissions: _, token: let accessTocken):
                
                self.fetchFacebookData(withAccessToken: accessTocken)
            case .failed(let ErrorRecvd):
                print("err",ErrorRecvd)
                
            case .cancelled:
                print("cancelled")
            }
            
        })

    }
    
    
    @IBAction func registerNowAction(_ sender: Any) {
//        loginType = .normal
        let registerViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLRegisterViewController") as! FLRegisterViewController
        self.navigationController?.pushViewController(registerViewController, animated: true)


        
    }
    
    
    @IBAction func skipAction(_ sender: Any) {
        
        if segueType == .mapListing {
            segueToMapListing()
        }else{
            segueToBusinessListing()
        }
    }
    
    
    func googleLoginLabelAnimation(){
        
        UIView.transition(with: self.googleLoginLabel, duration: 0.2, options: .transitionCrossDissolve, animations: {         self.googleLoginLabel.textColor = UIColor.FLbuttonOrange
        }, completion: {(finished: Bool) in
            UIView.transition(with: self.googleLoginLabel, duration: 0.2, options: .transitionCrossDissolve, animations: {         self.googleLoginLabel.textColor = UIColor.white
            }, completion:nil)
        })
    }
    
    func facebookLoginLabelAnimation(){
        
        UIView.transition(with: self.facebookLoginLabel, duration: 0.2, options: .transitionCrossDissolve, animations: {         self.facebookLoginLabel.textColor = UIColor.FLbuttonBlue
        }, completion: {(finished: Bool) in
            UIView.transition(with: self.googleLoginLabel, duration: 0.2, options: .transitionCrossDissolve, animations: {         self.googleLoginLabel.textColor = UIColor.white
            }, completion:nil)
        })
    }
    
}


//  Mark: - Google sign in delegate methods and facebook user data receivers

extension FLLoginViewController: GIDSignInDelegate,GIDSignInUIDelegate{
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error == nil{
            print("signin properly")
            var userInfo = FLAssistant.extractUserInfo(ofGoogleUser: user)
            print("======Google======")
            print("email:",userInfo.usersEmail)
            print("name:",userInfo.usersName)
            print("pictureURL",userInfo.usersPicture)
            loginType = .google
            UserDefaults.standard.set(loginType.rawValue, forKey: "loggedType")
            
            if segueType == .mapListing {
                segueToMapListing()
            }else{
                segueToBusinessListing()
            }
            
            GIDSignIn.sharedInstance().signOut()
        }else{
            print("not signin properly")
        }
    }
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        print("inWillDispatch")
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        print("preset google sign in view")
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        print("dismiss google sign in view")
        dismiss(animated: true, completion: nil)
        let businessListingViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLBusinsessListingViewController") as! FLBusinsessListingViewController
        self.navigationController?.pushViewController(businessListingViewController, animated: true)
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        print("diddisconnectwithuser")
    }
    
    /// facebook information retrieval
    func fetchFacebookData(withAccessToken accessToken: AccessToken){
        
        let graphRequest = GraphRequest(graphPath: "me", parameters: ["fields":"id,name,email,first_name,last_name,gender,picture.type(large)"], accessToken: accessToken, httpMethod: GraphRequestHTTPMethod(rawValue: "GET")!)
        
        graphRequest.start({ (connection, result) in
            switch result {
            case .failed(let error):
                print(error)
                
            case .success(let graphResponse):
                if let responseDictionary = graphResponse.dictionaryValue {
                    var userInfo = FLAssistant.extractUserInfo(ofFacebookUser: responseDictionary)
                    print("======Facebook======")
                    print("email:",userInfo.usersEmail)
                    print("name:",userInfo.usersName)
                    print("pictureURL",userInfo.usersPicture)
                    
                    
                    loginType = .facebook
                    UserDefaults.standard.set(loginType.rawValue, forKey: "loggedType")
                    
                    if self.segueType == .mapListing {
                        self.segueToMapListing()
                    }else{
                        self.segueToBusinessListing()
                    }
                }
            }
        })
        
        
    }
    
    
    
}


// Mark: - Receiving text field delegates

extension FLLoginViewController: UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        userNameTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
}

