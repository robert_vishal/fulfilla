//
//  FLNotificationViewController.swift
//  Fulfilla
//
//  Created by Appzoc on 09/11/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit


/// For resusing notification view controller
enum FLNotificationListingType: Int{
    case notifications
    case allReviews
}


class FLNotificationViewController: UIViewController {

    @IBOutlet var listingTable: UITableView!
    @IBOutlet var titleLabel: UILabel!
    
    var tableDataSource = [Any]()
    var listingType: FLNotificationListingType = FLNotificationListingType.notifications
    var selectedIndexPath: [IndexPath] = []
    var reviewTextHeight: CGFloat = 138
    var selectedIndexPathHeight: [CGFloat] = []
    var selectedIndexTextHeight:[CGFloat] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if listingType == .notifications{
            listingTable.separatorStyle = .none
            titleLabel.text = "Notifications"

        }else{
            listingTable.separatorStyle = .singleLine
            titleLabel.text = "Reviews"

        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
}


// Mark:- Receiving tableview delegates

extension FLNotificationViewController: UITableViewDataSource, UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableDataSource.isEmpty{
            
        }
        
        return 10
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        cell.appearanceStyle = .twoDimentionalYAxis
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if listingType == .notifications {
            return 106
            
        }else{

            
            
            if selectedIndexPath.contains(indexPath){

                return selectedIndexPathHeight[selectedIndexPath.index(of: indexPath)!]
            }else{
                return 138

            }
            
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if listingType == .notifications{
            
        }else{
            
        }
        
        if listingType == .notifications{
            
            var tableCell = tableView.dequeueReusableCell(withIdentifier: "FLNotificationTableViewCell") as? FLNotificationTableViewCell
            
            if tableCell == nil {
                tableView.register(UINib(nibName: "FLNotificationTableViewCell", bundle: nil), forCellReuseIdentifier: "FLNotificationTableViewCell")
                print("registering tableviewcell")
                tableCell = tableView.dequeueReusableCell(withIdentifier: "FLNotificationTableViewCell") as? FLNotificationTableViewCell
            }
            
            // Handle data for each cell
            
            tableCell?.titleLabel.text = "Your account validity lasts on tomorrow Please update your account."
            tableCell?.timeLabel.text = "09.39 am"
            tableCell?.notificationButton.setTitle("Chat Now", for: .normal)
            tableCell?.profileImage.image = UIImage(named: "sample")
            
            
            tableCell?.selectionStyle = .none
            return tableCell!

            
        }else{
            
            var tableCell = tableView.dequeueReusableCell(withIdentifier: "FLAllReviewTableViewCell") as? FLAllReviewTableViewCell
            
            if tableCell == nil {
                tableView.register(UINib(nibName: "FLAllReviewTableViewCell", bundle: nil), forCellReuseIdentifier: "FLAllReviewTableViewCell")
                print("registering tableviewcell FLAllReviewTableViewCell")
                tableCell = tableView.dequeueReusableCell(withIdentifier: "FLAllReviewTableViewCell") as? FLAllReviewTableViewCell

            }
            
            
            // Handle data for each cell
            
            if selectedIndexPath.contains(indexPath){
                
                let indexValue = selectedIndexPath.index(of: indexPath)
                
                tableCell?.commentLabel.numberOfLines = 0

                tableCell?.commentLabel.frame.size.height = selectedIndexTextHeight[indexValue!]

            }
            
           
            
            tableCell?.selectionStyle = .none
            
            return tableCell!
            
        }

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if listingType == .notifications{
            
        }else{
            
            let selectedCell = tableView.cellForRow(at: indexPath) as! FLAllReviewTableViewCell

            if selectedIndexPath.contains(indexPath){
                
                selectedCell.commentLabel.numberOfLines = 3
                selectedCell.commentLabel.sizeToFit()
                let indexValue = selectedIndexPath.index(of: indexPath)
                selectedIndexPathHeight.remove(at: indexValue!)
                selectedIndexPath.remove(at: indexValue!)
                selectedIndexTextHeight.remove(at: indexValue!)
                tableView.beginUpdates()
                tableView.endUpdates()
                tableView.scrollToRow(at: indexPath, at: .none, animated: false)
                
            }else{
                
                reviewTextHeight = getHeightOf(text: selectedCell.commentLabel.text!, havingWidth: selectedCell.commentLabel.frame.size.width, andFont: UIFont(name: "HelveticaNeue", size: 12.0)!).height
                reviewTextHeight -= selectedCell.commentLabel.frame.size.height
                selectedCell.commentLabel.numberOfLines = 0
//                selectedCell.commentLabel.frame.size.height = reviewTextHeight   //200
                selectedIndexPath.append(indexPath)
                selectedIndexPathHeight.append(reviewTextHeight + selectedCell.frame.size.height)
                selectedIndexTextHeight.append(reviewTextHeight)

                tableView.beginUpdates()
                tableView.endUpdates()
                tableView.scrollToRow(at: indexPath, at: .none, animated: false)
                
            }
            
            
        }
        
        
        
        
    }
    
    func getHeightOf(text: String, havingWidth widthValue: CGFloat, andFont font: UIFont) -> CGSize {
        var size = CGSize.zero
        if text.isEmpty == false {
            let frame = text.boundingRect(with: CGSize(width: widthValue, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
            size = CGSize(width:frame.size.width,height: ceil(frame.size.height))
        }
        return size
    }
    
    
 
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let tableCell  = tableView.cellForRow(at: indexPath as IndexPath)
        tableCell!.contentView.backgroundColor = UIColor.FLtableSelection
    }
    
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell  = tableView.cellForRow(at: indexPath as IndexPath)
        cell!.contentView.backgroundColor = .clear
    }
    
}
