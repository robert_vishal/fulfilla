//
//  FLAllReviewTableViewCell.swift
//  Fulfilla
//
//  Created by Appzoc on 24/11/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit

class FLAllReviewTableViewCell: UITableViewCell {

    @IBOutlet var profileImage: UIImageView!
    @IBOutlet var profileName: UILabel!
    @IBOutlet var profileDescription: UILabel!
    @IBOutlet var ratingView: FloatRatingView!
    @IBOutlet var commentLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var likesCount: UILabel!
    @IBOutlet var dislikesCount: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
