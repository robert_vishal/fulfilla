//
//  FLAboutUsViewController.swift
//  Fulfilla
//
//  Created by Appzoc on 22/11/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit

class FLAboutUsViewController: UIViewController {

    
    @IBOutlet var containerVIew: UIView!
    @IBOutlet var aboutFullfillaView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpInterface()
    }

    
    func setUpInterface(){
        containerVIew.setFLShadow()
        aboutFullfillaView.setShadow(withColor: UIColor.lightGray, opacity: 0.7, andShadowRadius: 3)

        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
