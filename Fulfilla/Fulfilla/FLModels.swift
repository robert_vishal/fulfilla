//
//  FLModels.swift
//  Fulfilla
//
//  Created by Appzoc on 17/10/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import Foundation
import GoogleSignIn
import Kingfisher


// Mark: - Login user model for normal user, google and facebook

public struct FLUser {
    
    lazy var usersName:String = ""
    lazy var usersEmail:String = ""
    lazy var usersPicture: String = ""
    
    init(Google userData: GIDGoogleUser?){
        if let userData = userData {
            self.usersName = FLUnwraper.unwrap(string: userData.profile.name)
            self.usersEmail = FLUnwraper.unwrap(string: userData.profile.email)
            if userData.profile.hasImage{
                self.usersPicture = FLUnwraper.unwrap(string: userData.profile.imageURL(withDimension: 750).absoluteString)
            }else{
                self.usersPicture = ""
            }
         }
    }
    
    init(Facebook userData: [String : Any]?){
        if let userData = userData{
            
            self.usersName = FLUnwraper.unwrap(string: userData["name"] as? String!)
            self.usersEmail = FLUnwraper.unwrap(string: userData["email"] as? String)
            if let pictureURLTemp1 = userData["picture"] as! [String : AnyObject]! {
                self.usersPicture = ""
                if let pictureURLTemp2 = pictureURLTemp1["data"] as! [String : AnyObject]! {
                    self.usersPicture = FLUnwraper.unwrap(string: pictureURLTemp2["url"] as? String)
                }
            }else{
                self.usersPicture = ""
            }
        }
        
 
    
    }
    
    

}

//MARK:- Business Listing Model

struct BusinessListingModel{
    
    let id:String
    let title:String
    let location:String
    let rating:String
    let city:String
    let reviewCount:String
    let contactPhone:String
    let contactMessage:String
    let latitude:String
    let longitude:String
    let profileImageLink:String
    
//    var profileImage: ImageResource{
//        let resource = ImageResource(downloadURL: URL(string: profileImageLink)! , cacheKey: "key-\(profileImageLink)")
//        return resource
//    }
    
    init(id:String = "",title:String = "",location:String = "",rating:String = "",city:String = "",reviewCount:String = "",contactPhone:String = "",contactMessage:String = "",latitude:String = "",longitude:String = "",profileImageLink:String) {
        self.id = id
        self.title = title
        self.location = location
        self.rating = rating
        self.city = city
        self.reviewCount = reviewCount
        self.contactPhone = contactPhone
        self.contactMessage = contactMessage
        self.latitude = latitude
        self.longitude = longitude
        self.profileImageLink = profileImageLink
        
    }
    
    init(with data:[String:Any]) {
        self.id = "\(data["service_id"]!)"
        self.title = "\(data["service_title"]!)"
        
        let tempLocation = data["service_location"]! as? [[String]]
        self.location = (tempLocation?[0].first)!
        //MARK:- To change:
        self.rating = "3.5"
        self.city = "\(data["service_city"]!)"
        //print(city)
        self.reviewCount = "\(data["service_review_count"]!)"
        
        let tempContact = data["service_contact_phone"]!
        //let subTempContact = tempContact as? [String]
        self.contactPhone = ""
        //self.contactPhone = "\(tempContact?[0].first!)"
        print(tempContact)
        let tempContactMessage = data["service_contact_Message"]! as? [[String]]
        print(tempContactMessage?[0])
        self.contactMessage = "\(tempContactMessage?[0].first)"
        
        self.latitude = "\(data["service_lat_lon"]!)"
        self.longitude = "\(data["service_lat_lon"]!)"
        self.profileImageLink = "\(data["service_profile_image"]!)"
        
    }
    
    
}


//MARK:- Quick Finds Model
struct QuickFindsModel{
    let id:Int
    let name:String
    let imageURL:String
    
    init(source:Dictionary<String,Any>) {
        self.id = source["category_id"] as! Int
        self.name = source["category_name"] as! String
        self.imageURL = source["category_image"] as! String
    }
}


struct HomePageModel {
    let id:String
    let name:String
    let email:String
    let phone:String
    let imageLink:String
    var trendingServicesList: [TrendingServices]
    var categories:[CategoryModel]
    
    struct TrendingServices {
        let id:String
        let title:String
        let location:String
        let category:String
        let imageLink:String
        /*
         service_id":12,
         "service_title":"title",
         "service_location":"description",
         "service_category":"wedding_service",
         "service_image
 */
        
        init(with data:[String:Any]) {
            self.id = "\(data["service_id"]!)"
            self.title = "\(data["service_title"]!)"
            let tempLocationA = data["service_location"]!
            let tempLocationB = tempLocationA as? [[String]]
            self.location = (tempLocationB?[0].first!)!
            print(self.location)
            self.category = "\(data["service_category"]!)"
            self.imageLink = "\(data["service_image"]!)"
        }
    }
    
    init(with data:[String:Any]) {
        self.id = "\(data["user_id"]!)"
        self.name = "\(data["user_name"]!)"
        self.email = "\(data["user_email"]!)"
        self.phone = "\(data["phone"]!)"
        self.imageLink = "\(data["user_image"]!)"
        
      //  print(imageLink)
      //  print(phone)
       // print(id)
      //  print(name)
        self.trendingServicesList = []
        let trendingServicesWrapper = data["trending_services"]!
        let servicesWrapper = trendingServicesWrapper as! [[String:Any]]
      // print(servicesWrapper)
        
        for element in servicesWrapper{
            let object = TrendingServices(with: element as! [String : Any])
            trendingServicesList.append(object)
        }
        //print(trendingServicesList)
        
        self.categories = []
        let tempCategoriesWrapper = data["categories"]!
        let categoriesWrapper = tempCategoriesWrapper as! [[String:Any]]
       // print(categoriesWrapper)
        for element in categoriesWrapper{
            let object = CategoryModel(with: element)
            categories.append(object)
        }
       // print(categories)
        
        
        
    }
    
    
}

struct CategoryModel {
    let id:String
    let name:String
    let imageLink:String
    
    /*
     "category_id":23,
     "category_name":"name",
     "category_image"
 */
    
    init(with data:[String:Any]) {
        self.id = "\(data["category_id"]!)"
        self.name = "\(data["category_name"]!)"
        self.imageLink = "\(data["category_image"]!)"
    }
}




// Mark: - structure of property to list // list view cell

public class FLProperty{
    
    //    private var kPropertyId : String = "property_id"
    //    private let kPropertyTitle : String = ""
    //    private let kPropertyReferenceNo : String = ""
    //    private let kPostedDate : String = ""
    //    private let kCity : String = ""
    //    private let kLocation : String = ""
    //    private let kLatitude : String = ""
    //    private let kLongitude : String = ""
    //    private let kEmail : String = ""
    //    private let kPhone : String = ""
    //    private let kMessage : String = ""
    //    private let kPrice : String = ""
    //    private let kPictures : String = ""
    //    private let kIconPicture: String = ""
    //    private let kVideos: String = ""
    //    private let kPhotoCount : String = ""
    //    private let kArea : String = ""
    //    private let kRatingCount : String = ""
    //    private let kReviewCount : String = ""
    //    private let kBathroomCount : String = ""
    //    private let kBedroomCount : String = ""
    
    enum JsonKeys: String {
        case kPropertyId = "property_id"
        case kPropertyTitle = ""
        case kPropertyReferenceNo = "a"
        case kPostedDate = "b"
        case kCity = "c"
        case kLocation = "d"
        case kLatitude = "e"
        case kLongitude = "f"
        case kEmail = "g"
        case kPhone = "h"
        case kMessage = "i"
        case kPrice = "j"
        case kPictures = "k"
        case kIconPicture = "l"
        case kVideos = "m"
        case kPhotoCount = "n"
        case kArea = "o"
        case kRatingCount = "p"
        case kReviewCount = "q"
        case kBathroomCount = "r"
        case kBedroomCount = "s"
    }

    /// Data variables
    var propertyId : String = ""
    var propertyTitle : String = ""
    var propertyReferenceNo : String = ""
    var postedDate : String = ""
    var city : String = ""
    var location : String = ""
    var latitude : String = ""
    var longitude : String = ""
    var email : String = ""
    var phone : String = ""
    var message : String = ""
    var price : String = ""
    var area : String = ""
    var iconPicture: String = ""
    var ratingCount:Int = 0
    var reviewCount:Int = 0
    var bedroomCount : String = ""
    var bathroomCount : String = ""
    var pictures : [String] = [""]
    var videos: [String] = [""]
    
    
    
//    /// Deniening initialisation outside of the class
//    private init(){
//    }
    
    /// Extact the webservice result
    
   
    
    
    class func extractJSON(ofSearchResult respondsObject: FLJsonResponds?, completion: @escaping (Bool, FLProperty) -> Void){
        let FLPropertyObject = FLProperty()
        if let jsonResult = respondsObject{
            print("FLProperty : jsonResult : ",jsonResult)
            
            if let resultObject: NSDictionary = jsonResult.object as? NSDictionary {
                
                FLPropertyObject.propertyId = FLUnwraper.unwrap(string: resultObject[JsonKeys.kPropertyId.rawValue] as? String)
                
                FLPropertyObject.propertyTitle = FLUnwraper.unwrap(string: resultObject[JsonKeys.kPropertyTitle.rawValue] as? String)
                
                FLPropertyObject.propertyReferenceNo = FLUnwraper.unwrap(string: resultObject[JsonKeys.kPropertyReferenceNo.rawValue] as? String)
                
                FLPropertyObject.postedDate = FLUnwraper.unwrap(string: resultObject[JsonKeys.kPostedDate.rawValue] as? String)
                
                FLPropertyObject.city = FLUnwraper.unwrap(string: resultObject[kSecPropertyTypeTitle] as? String)
                
                FLPropertyObject.location = FLUnwraper.unwrap(string: resultObject[JsonKeys.kLocation.rawValue] as? String)
                
                FLPropertyObject.latitude = FLUnwraper.unwrap(string: resultObject[JsonKeys.kLatitude.rawValue] as? String)
                
                FLPropertyObject.longitude = FLUnwraper.unwrap(string: resultObject[JsonKeys.kLongitude.rawValue] as? String)
                
                FLPropertyObject.email = FLUnwraper.unwrap(string: resultObject[JsonKeys.kEmail.rawValue] as? String)
                
                FLPropertyObject.phone = FLUnwraper.unwrap(string: resultObject[JsonKeys.kPhone.rawValue] as? String)
                
                FLPropertyObject.message = FLUnwraper.unwrap(string: resultObject[JsonKeys.kMessage.rawValue] as? String)
                
                FLPropertyObject.price = FLUnwraper.unwrap(string: resultObject[JsonKeys.kPrice.rawValue] as? String)
                
                FLPropertyObject.area = FLUnwraper.unwrap(string: resultObject[JsonKeys.kArea.rawValue] as? String)
                
                FLPropertyObject.iconPicture = FLUnwraper.unwrap(string: resultObject[JsonKeys.kIconPicture.rawValue] as? String)
                
                FLPropertyObject.ratingCount = FLUnwraper.unwrap(integer: resultObject[JsonKeys.kRatingCount.rawValue] as? Int)
                
                FLPropertyObject.reviewCount = FLUnwraper.unwrap(integer: resultObject[JsonKeys.kReviewCount.rawValue] as? Int)
                
                FLPropertyObject.bedroomCount = FLUnwraper.unwrap(string: resultObject[JsonKeys.kBedroomCount.rawValue] as? String)
                
                FLPropertyObject.bathroomCount = FLUnwraper.unwrap(string: resultObject[JsonKeys.kBathroomCount.rawValue] as? String)
                
                FLPropertyObject.pictures = resultObject[JsonKeys.kPictures.rawValue]  as! [String]
                
                FLPropertyObject.videos =  resultObject[JsonKeys.kVideos.rawValue]  as! [String]
                completion(true, FLPropertyObject)
            }else{
                completion(false, FLPropertyObject)
            }
        }else{
            completion(false, FLPropertyObject)
        }
        
    }
    
    

  
//    init(initWith object: FLJsonResponds?) {
//        
//        if let jsonResult = object{
//           print("FLProperty : jsonResult : ",jsonResult)
//        
//           if let resultObject: NSDictionary = jsonResult.object as? NSDictionary {
//            
//                self.propertyId = FLUnwraper.unwrap(string: resultObject[kPropertyId] as? String)
//
//                self.propertyTitle = FLUnwraper.unwrap(string: resultObject[kPropertyTitle] as? String)
//            
//                self.propertyReferenceNo = FLUnwraper.unwrap(string: resultObject[kPropertyReferenceNo] as? String)
//
//                self.postedDate = FLUnwraper.unwrap(string: resultObject[kPostedDate] as? String)
//
//                self.city = FLUnwraper.unwrap(string: resultObject[kSecPropertyTypeTitle] as? String)
//
//                self.location = FLUnwraper.unwrap(string: resultObject[kLocation] as? String)
//
//                self.latitude = FLUnwraper.unwrap(string: resultObject[kLatitude] as? String)
//            
//                self.longitude = FLUnwraper.unwrap(string: resultObject[kLongitude] as? String)
//            
//                self.email = FLUnwraper.unwrap(string: resultObject[kEmail] as? String)
//            
//                self.phone = FLUnwraper.unwrap(string: resultObject[kPhone] as? String)
//            
//                self.message = FLUnwraper.unwrap(string: resultObject[kMessage] as? String)
//
//                self.price = FLUnwraper.unwrap(string: resultObject[kPrice] as? String)
//
//                self.area = FLUnwraper.unwrap(string: resultObject[kArea] as? String)
//
//                self.iconPicture = FLUnwraper.unwrap(string: resultObject[kIconPicture] as? String)
//            
//                self.ratingCount = FLUnwraper.isEmptyInteger(integer: resultObject[kRatingCount] as? Int)
//            
//                self.reviewCount = FLUnwraper.isEmptyInteger(integer: resultObject[kReviewCount] as? Int)
//            
//                self.bedroomCount = FLUnwraper.unwrap(string: resultObject[kBedroomCount] as? String)
//            
//                self.bathroomCount = FLUnwraper.unwrap(string: resultObject[kBathroomCount] as? String)
//
//                self.pictures = resultObject[kPictures]  as! [String]
//            
//                self.videos =  resultObject[kVideos]  as! [String]
//
//            }
//
//        }
    
  //  }
    
}


