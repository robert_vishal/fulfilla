//
//  FLAnimations.swift
//  Fulfilla
//
//  Created by Appzoc on 12/12/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import Foundation
import UIKit


// Mark: - Animated apearance to popup views , alert , activity indicator

public enum FLPopUpAnimationStyle: Int{
    case zoomIn
    case zoomOut
}


public enum FLDirection: Int{
    case top
    case bottom
    case left
    case right
}

public enum FLAxis: Int {
    case x
    case y
}

public class  FLAnimations{
    
    class func popUpAnimation(applyToView animatingView: UIView, animationStyle: FLPopUpAnimationStyle){
        
        switch animationStyle {
        case .zoomIn:
            animatingView.isHidden = false
            UIView.animate(withDuration: 0.25, animations: {
                animatingView.transform = CGAffineTransform.identity.scaledBy(x: 1.1, y: 1.1)
            }, completion: {finished in
                UIView.animate(withDuration: 0.205, animations: {
                    animatingView.transform = CGAffineTransform.identity.scaledBy(x: 0.9, y: 0.9);
                }, completion: {finished in
                    UIView.animate(withDuration: 0.205, animations: {
                        animatingView.transform = CGAffineTransform.identity
                    })
                })
            })
        case .zoomOut:
            animatingView.isHidden = false
            UIView.animate(withDuration: 0.25, animations: {
                animatingView.transform = CGAffineTransform.identity.scaledBy(x: 1.1, y: 1.1)
            }, completion: {finished in
                UIView.animate(withDuration: 0.21, animations: {
                }, completion: {finished in
                    UIView.animate(withDuration: 0.21, animations: {
                        animatingView.transform = CGAffineTransform.identity
                    })
                })
            })
        }
        
    }
    
    
    // show normal alert with "OK" button.
    
    class func showAlert(withTitle title: String, message: String, andPresentOn viewContoller: UIViewController)
    {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
            alert.view.tintColor = UIColor.FLlightBlue

            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            viewContoller.present(alert, animated: true, completion: nil)
            
        }
    }
    
    
    // show automatically dismissing alert.
    
   class func showAutoDismissAlert(withTitle: String, message: String, dismissTime: Double,  andPresentOn viewController: UIViewController){
    
        DispatchQueue.main.async {
            let alert = UIAlertController(title: withTitle, message: message, preferredStyle: UIAlertControllerStyle.alert)
            viewController.present(alert, animated: true, completion: nil)
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + dismissTime){
            alert.dismiss(animated: true, completion: nil)
            }
        }
    
    }

    
    
    // show alert with controll go back to the view controller which it is presented on
    
    class func showEscapingAlert(withTitle title: String, message: String, andPresentOn viewContoller: UIViewController, escapingFuncName: @escaping () -> Void)
    {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
            
            alert.view.tintColor = UIColor.FLlightBlue

            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: { (action) in
                alert.dismiss(animated: true, completion: nil)
            }))

            alert.addAction(UIAlertAction(title: "Try again", style: UIAlertActionStyle.default, handler: { (action) in
                escapingFuncName()
            }))

            
            viewContoller.present(alert, animated: true, completion: nil)
            
        }
    }

    
    // activity indicator object
    
    static let activity = FLActivityIndicator()
    
    
    // presenting a view with animation from any side - bottom/ top/ left / right
    
    class func presentView(view animatingView : UIView, originOfView animatingViewOrigin: CGPoint, inView: UIView, direction: FLDirection, isDismiss: Bool){
        
        inView.clipsToBounds = true
        DispatchQueue.main.async {
            if isDismiss{
                // hiding animating view
                animatingView.isHidden = false
                switch direction {
                    
                case .bottom:
                    animatingView.isHidden = false
                    UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
                        animatingView.frame.origin.y = inView.frame.height + 1
                    }, completion: {(completed: Bool) in
                        animatingView.isHidden = true
                        
                    })
                    
                default:
                    break
                }
                
            }else{
                // showing animating view
                animatingView.isHidden = true
                switch direction {
                    
                case .bottom:
                    animatingView.frame.origin.y = inView.frame.height + 1
                    animatingView.isHidden = false
                    UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
                        animatingView.frame.origin.y = animatingViewOrigin.y
                    }, completion: nil)
                    
                default:
                    break
                }
            }
        }
    }

    
    // shake animation for a view
    
    class func shakeView(view viewToShake: UIView , isHorizontally: Bool) {
        
        DispatchQueue.main.async {
            
            if isHorizontally{
                let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
                animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
                animation.duration = 0.5
                animation.values = [-8.0, 8.0, -8.0, 8.0, -5.0, 5.0, -2.0, 2.0, 0.0 ]
                viewToShake.layer.add(animation, forKey: "shakeHorizontally")

            }else{
                let animation = CAKeyframeAnimation(keyPath: "transform.translation.y")
                animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
                animation.duration = 0.5
//                animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
                animation.values = [-8.0, 8.0, -8.0, 8.0, -5.0, 5.0, -2.0, 2.0, 0.0 ]

                viewToShake.layer.add(animation, forKey: "shakeVertically")

            }

        }
        
        
    }
}



// Mark: - Rotating activity indicator

public class FLActivityIndicator{
    private let indicator = UIImageView(image: #imageLiteral(resourceName: "activityindicator"))
    private let indicatorBackground = UIView()
    let label = UILabel()
    private let alertTime: Int = 2
    private var isShowingAlert: Bool = false
    
    func show(inView: UIView){
        DispatchQueue.main.async {
            self.indicator.frame = CGRect(x: 0, y: 0, width: 38, height: 38)
            self.indicator.center = CGPoint(x: inView.frame.size.width/2.0, y: inView.frame.size.height/2.0)
            let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
            rotateAnimation.fromValue = 0.0
            rotateAnimation.toValue = CGFloat(.pi * 2.0)
            rotateAnimation.duration = 0.6
            rotateAnimation.isCumulative = true
            rotateAnimation.repeatCount = Float.greatestFiniteMagnitude
            self.indicator.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            self.indicator.layer.add(rotateAnimation, forKey: nil)
            
            self.indicatorBackground.frame = inView.frame
            self.indicatorBackground.backgroundColor = UIColor(colorLiteralRed: 0/255, green: 0/255, blue: 0/255, alpha: 0.25)
            self.indicatorBackground.center = inView.center
            self.indicatorBackground.addSubview(self.indicator)
            inView.addSubview(self.indicatorBackground)
        }
    }
    
    func show(inView: UIView, ofWidth: CGFloat, andHeight: CGFloat){
        
        DispatchQueue.main.async {
            self.indicator.frame = CGRect(x: 0, y: 0, width: ofWidth, height: andHeight)
            self.indicator.center = CGPoint(x: inView.frame.size.width/2.0, y: inView.frame.size.height/2.0)
            let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
            rotateAnimation.fromValue = 0.0
            rotateAnimation.toValue = CGFloat(.pi * 2.0)
            rotateAnimation.duration = 0.6
            rotateAnimation.isCumulative = true
            rotateAnimation.repeatCount = Float.greatestFiniteMagnitude
            self.indicator.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            self.indicator.layer.add(rotateAnimation, forKey: nil)
            self.indicatorBackground.frame = inView.frame
            self.indicatorBackground.backgroundColor = UIColor(colorLiteralRed: 0/255, green: 0/255, blue: 0/255, alpha: 0.25)
            self.indicatorBackground.center = inView.center
            self.indicatorBackground.addSubview(self.indicator)
            inView.addSubview(self.indicatorBackground)
        }
    }

    
    func hide(){
        DispatchQueue.main.async {
            if self.isShowingAlert{
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                    self.indicator.removeFromSuperview()
                    self.label.removeFromSuperview()
                    self.isShowingAlert = false
                }
            }else{
                self.indicator.removeFromSuperview()

            }
        }
    }
    
    
    
//    func alert(withMessage: String = "Submitted successfully."){
//        DispatchQueue.main.async {
//            self.isShowingAlert = true
//            self.label.font = UIFont.systemFont(ofSize: 12.0)
//            self.label.textAlignment = NSTextAlignment.center
//            self.label.text = withMessage
//            self.label.frame = CGRect(x: 0, y: 0, width: self.indicatorBackground.frame.width, height: 35)
//            self.label.center = CGPoint(x: self.indicatorBackground.frame.size.width/2.0, y: self.indicatorBackground.frame.size.height/2.0)
//            self.label.frame.origin.y = self.indicator.frame.origin.y + self.indicator.frame.height + 8
//            self.label.backgroundColor = .white
//            self.label.layer.cornerRadius = 4.0
//            self.label.layer.masksToBounds = true
////            self.label.sizeToFit()
//            self.indicatorBackground.addSubview(self.label)
//        }
//        
//    }
    
    
//    private func getHeightOf(text: String, havingWidth widthValue: CGFloat, andFont font: UIFont) -> CGSize {
//        var size = CGSize.zero
//        if text.isEmpty == false {
//            let frame = text.boundingRect(with: CGSize(width: widthValue, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
//            size = CGSize(width:frame.size.width,height: ceil(frame.size.height))
//        }
//        return size
//    }
    
    deinit {
        
    }

    
    

    
    
    
}

 
 


