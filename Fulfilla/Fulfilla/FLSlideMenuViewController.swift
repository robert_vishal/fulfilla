//
//  FLSlideMenuViewController.swift
//  Fulfilla
//
//  Created by Appzoc on 13/11/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//a

import UIKit

class FLSlideMenuViewController: UIViewController {

    @IBOutlet var profileImage: UIImageView!
    
    @IBOutlet var profileName: UILabel!
    
    @IBOutlet var profileEmail: UILabel!
    
    @IBOutlet var menuTable: UITableView!
    
    let menuTitles = ["My Account","Categories","Quick finds","Featured Services","Chat","Membership Plans","Favourites", "About","Contact Us","Logout"]
    let menuImages = [#imageLiteral(resourceName: "myaccount"),#imageLiteral(resourceName: "cate"),#imageLiteral(resourceName: "quick"),#imageLiteral(resourceName: "featured"),#imageLiteral(resourceName: "chatmenu"),#imageLiteral(resourceName: "member"),#imageLiteral(resourceName: "favmenu"),#imageLiteral(resourceName: "about"),#imageLiteral(resourceName: "contact"),#imageLiteral(resourceName: "logouteditted")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpInterface()
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func setUpInterface(){
        profileImage.cornerRadius = profileImage.frame.size.width / 2
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}



// Mark: - Menu table view data source and delegates

extension FLSlideMenuViewController: UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuTitles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let tableCell = tableView.dequeueReusableCell(withIdentifier: "FLSlideMenuTableViewCell") as! FLSlideMenuTableViewCell
        
            tableCell.menuTitle.text = menuTitles[indexPath.row]
            tableCell.menuImage.image = menuImages[indexPath.row]
        
            tableCell.selectionStyle = .none
            return tableCell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            /// My account
            let segueViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLMyAccountViewController") as! FLMyAccountViewController
            self.navigationController?.pushViewController(segueViewController, animated: true)
        case 1:
            /// Categories
            let segueViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLQuickFindsViewController") as! FLQuickFindsViewController
            segueViewController.segueFrom = .categories
            self.navigationController?.pushViewController(segueViewController, animated: true)
        case 2:
            /// Quick finds
            let segueViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLQuickFindsViewController") as! FLQuickFindsViewController
            segueViewController.segueFrom = .quickFinds
            self.navigationController?.pushViewController(segueViewController, animated: true)
        case 3:
            /// Featured services
            let segueViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLQuickFindsViewController") as! FLQuickFindsViewController
            segueViewController.segueFrom = .featuredServices
            self.navigationController?.pushViewController(segueViewController, animated: true)
        case 4:
            /// chat
            let segueViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLFavouritesListingViewController") as! FLFavouritesListingViewController
            
            segueViewController.isChat = true
            self.navigationController?.pushViewController(segueViewController, animated: true)
        case 5:
            /// Membership plans
//            let segueViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLDetailsBusinessViewController") as! FLDetailsBusinessViewController
//            self.navigationController?.pushViewController(segueViewController, animated: true)
            break
        case 6:
            /// Favourites
            let segueViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLFavouritesListingViewController") as! FLFavouritesListingViewController
            segueViewController.isChat = false
            self.navigationController?.pushViewController(segueViewController, animated: true)
        case 7:
            /// About us
            let segueViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLAboutUsViewController") as! FLAboutUsViewController
            self.navigationController?.pushViewController(segueViewController, animated: true)
        case 8:
            /// Contact us
            let segueViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLContactUsViewController") as! FLContactUsViewController
            self.navigationController?.pushViewController(segueViewController, animated: true)
        case 9:
            /// Logout
            let segueViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLLoginViewController") as! FLLoginViewController
            self.navigationController?.pushViewController(segueViewController, animated: true)
        default:
            print("default")
        }
       
        
    }
    
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell  = tableView.cellForRow(at: indexPath as IndexPath) as! FLSlideMenuTableViewCell
        cell.contentView.backgroundColor = UIColor.FLbuttonBlue
        cell.menuTitle.textColor = UIColor.white
        
    }
    
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell  = tableView.cellForRow(at: indexPath as IndexPath) as! FLSlideMenuTableViewCell
        cell.contentView.backgroundColor = .clear
        cell.menuTitle.textColor = UIColor(colorLiteralRed: 76/255, green: 76/255, blue: 76/255, alpha: 1.0)
    }
    
}







