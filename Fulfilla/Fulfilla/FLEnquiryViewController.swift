//
//  FLEnquiryViewController.swift
//  Fulfilla
//
//  Created by Appzoc on 09/11/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit

class FLEnquiryViewController: UIViewController {

    @IBOutlet var containerView: UIView!
    @IBOutlet var bodyView: UIView!
    @IBOutlet var detailsView: UIView!
    @IBOutlet var profileImage: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var categoryLabel: UILabel!
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var phoneTextField: UITextField!
    @IBOutlet var messageTextView: UITextView!
    
    var nameString: String = ""
    var emailString: String = ""
    var phoneString: String = ""
    var messageString: String = ""
    let messagePlaceHolder = "  Message"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        FLAnimations.popUpAnimation(applyToView: containerView, animationStyle: .zoomOut)
        setUpInterface()

        
    }

    func setUpInterface(){
        
        bodyView.setFLShadow()
        profileImage.layer.cornerRadius = profileImage.frame.width / 2
        nameTextField.setFLBorder()
        emailTextField.setFLBorder()
        phoneTextField.setFLBorder()
        messageTextView.setFLBorder()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)

    }
    @IBAction func loadEnquiryFormAction(_ sender: Any) {
        let enquirayFormViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLEnquirayFormViewController") as! FLEnquirayFormViewController
        self.transitionStyle = .rightToLeft
        self.present(enquirayFormViewController, animated: false, completion: nil)

        
        
        
    }
    
    
    @IBAction func submitAction(_ sender: Any) {
//        self.dismiss(animated: false, completion: nil)
        print("Name:", nameString)
        print("email:", emailString)
        print("phone:", phoneString)
        print("message:", messageString)
        
        if FLUnwraper.isNotEmpty(string: nameString) && FLUnwraper.isNotEmpty(string: phoneString){
            FLAnimations.activity.show(inView: self.view)
            DispatchQueue.main.asyncAfter(deadline: .now() + FLAssistant.waitingTime) {
                FLAnimations.activity.hide()
                self.showWebserviceAlert()
                
            }
            
        }

    }
    

    func showWebserviceAlert(){
        let alert = UIAlertController(title: "Fullfilla", message: "Submitted successfully.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: { action in
            alert.dismiss(animated: true, completion: nil)
            self.dismiss(animated: false, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Try again", style: UIAlertActionStyle.default, handler: { action in
            print("call web service again!")
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
    

    
}


// Mark: - Handling textfield and textview events

extension FLEnquiryViewController: UITextViewDelegate, UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == nameTextField {
            nameString = FLUnwraper.unwrap(string: nameTextField.text)
        }else if textField == emailTextField {
            emailString = FLUnwraper.unwrap(string: emailTextField.text)
            if !FLUnwraper.isValid(email: emailString){
                FLAnimations.showAutoDismissAlert(withTitle: "", message: "Please enter valid email", dismissTime: 1.5, andPresentOn: self)
                
            }
        }else {
            phoneString = FLUnwraper.unwrap(string: phoneTextField.text)
            if !FLUnwraper.isValid(phone: phoneString){
                
                FLAnimations.showAutoDismissAlert(withTitle: "", message: "Please enter valid phone number", dismissTime: 1.5, andPresentOn: self)
            }
        }
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        messageTextView.textColor = .black
        if textView.text == messagePlaceHolder{
            messageTextView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if messageTextView.text == "" {
            messageTextView.textColor = .FLplaceHolderGray
            messageTextView.text = messagePlaceHolder
        }
        if messageTextView.text != messagePlaceHolder{
            messageString = FLUnwraper.unwrap(string: messageTextView.text)
        }
    }
    
}



