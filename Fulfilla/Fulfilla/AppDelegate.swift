//
//  AppDelegate.swift
//  Fulfilla
//
//  Created by Appzoc on 11/10/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit
import GoogleSignIn
import FacebookCore
import FacebookLogin
import IQKeyboardManagerSwift
import GooglePlaces


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    //Changes

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        GMSPlacesClient.provideAPIKey("AIzaSyC2me8BgXS-Gizo2FhKbGPZ2ZLe0PElUds")
        
        
        if let currentLoggedType = UserDefaults.standard.string(forKey: "loggedType") {
            print("loggedType: ",currentLoggedType)
            if currentLoggedType != FLLoginType.logout.rawValue{
                UserDefaults.standard.set("logout", forKey: "loggedType")
            }
        }else{
            UserDefaults.standard.set("logout", forKey: "loggedType")
            print("loggedType: nil")
        }
       
        IQKeyboardManager.sharedManager().enable = true

        return SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
    }

    
    
    //Mark: - For Login via Google or facebook
    
//    /// ios 8 and before support
//    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
//        print("sourceApplication")
//        
//        
//        var _: [String: AnyObject] = [UIApplicationOpenURLOptionsKey.sourceApplication.rawValue: sourceApplication as AnyObject,UIApplicationOpenURLOptionsKey.annotation.rawValue: annotation as AnyObject]
//        
//        if loginType == .google{
//            return GIDSignIn.sharedInstance().handle(url, sourceApplication: sourceApplication,annotation: annotation)
//            
//        }else if loginType == .facebook{
//            
//            return SDKApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
//        }else{
//            return true
//        }
//        
//        
//    }
    /// iOS after 8
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        print("openURL")
        
        if loginType == .google {
            print("googleType")
            return GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        }else if loginType == .facebook {
//            let appId = SDKSettings.appId
//            if url.scheme != nil && url.scheme!.hasPrefix("fb\(appId)") && url.host ==  "authorize" { // facebook
//            }
            print("facebookType")
            return SDKApplicationDelegate.shared.application(app, open: url, options: options)

        }else{
            print("normalType")
            return true
        }
        
     }
    
    
    
    
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        AppEventsLogger.activate(application)

    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


    
    
    
}

