//
//  FLContactUsViewController.swift
//  Fulfilla
//
//  Created by Appzoc on 22/11/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit
import MapKit

class FLContactUsViewController: UIViewController,MKMapViewDelegate,CLLocationManagerDelegate {

    
    @IBOutlet var contactMapView: MKMapView!
    var locationsMarker: MarkerWithImage!
    let locationManager = CLLocationManager()
    var locationsMarkerView:MKPinAnnotationView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setDefaultLocation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    
    
    @IBAction func enquireAction(_ sender: UIButton) {
        let enquiryViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLEnquirayFormViewController") as! FLEnquirayFormViewController
        
        self.navigationController?.pushViewController(enquiryViewController, animated: true)

        //EnquiryFormController
    }
    
    
    //MARK: - Custom Marker
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if (annotation is MKUserLocation) {
            return nil
        }
        let reuseIdentifier = "customMarker"
        var markerView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)
        
        if markerView == nil {
            markerView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
            markerView?.canShowCallout = true
        } else {
            markerView?.annotation = annotation
        }
        
        let customMarker = annotation as! MarkerWithImage
        markerView?.image = UIImage(named: customMarker.imageName)
        return markerView
        
    }
    
    func setMarkerOnMap(markerCoordinate: CLLocationCoordinate2D ){
        locationsMarker = MarkerWithImage()
        locationsMarker.imageName = "marker"
        locationsMarker.coordinate = markerCoordinate
        locationsMarker.title = "Title"
        locationsMarker.subtitle = "subtitle"
        
        locationsMarkerView = MKPinAnnotationView(annotation: locationsMarker, reuseIdentifier: "customMarker")
        contactMapView.addAnnotation(locationsMarkerView.annotation!)
        
    }
    
    func setDefaultLocation(){
        // Qatar location 25.354756, 51.210509
        
        let locationQuatar = ["lat":25.342345, "lon":50.945464]
        let location2D = CLLocationCoordinate2D(latitude: 25.342345, longitude: 50.945464)
        let center = location2D
        let region = MKCoordinateRegionMake(center, MKCoordinateSpan(latitudeDelta: 0.85, longitudeDelta: 0.85))
        contactMapView.setRegion(region, animated: true)
        setMarkerOnMap(markerCoordinate: CLLocationCoordinate2D(latitude: locationQuatar["lat"]!, longitude: locationQuatar["lon"]!))
            
    }

}


    
    
    
