//
//  FLGridViewBusinessListingTableViewCell.swift
//  Fulfilla
//
//  Created by Appzoc on 24/10/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit

class FLGridViewBusinessListingTableViewCell: UITableViewCell {

    @IBOutlet var baseView: UIView!
    @IBOutlet var profileImageView: UIImageView!
    @IBOutlet var ratingView: FloatRatingView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var reviewLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUpCellUI()
        setRating()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setUpCellUI(){
        baseView.setFLShadow()
    }
    
    func setRating(){
        // Reset float rating view's background color
        ratingView.backgroundColor = UIColor.clear
        
        /** Note: With the exception of contentMode, type and delegate,
         all properties can be set directly in Interface Builder **/
        //        ratingView.delegate = self
        ratingView.contentMode = UIViewContentMode.scaleAspectFit
        ratingView.type = .wholeRatings
        ratingView.minRating = 0
        
    }
    
    func configureCell(with dataObject: FLProperty?){
        if let propertyData = dataObject{
            print("configureCell : ListView :",dataObject as Any)
            
            self.profileImageView.sd_setImage(with: URL(string: propertyData.iconPicture), placeholderImage: UIImage(named: ""))
            
            self.titleLabel.text = propertyData.propertyTitle
            self.ratingView.minRating = propertyData.ratingCount
            self.locationLabel.text = propertyData.location
            self.reviewLabel.text = "\(propertyData.reviewCount)" + " Reviews"
            
        }
        
    }
    
}
