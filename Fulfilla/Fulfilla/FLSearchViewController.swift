//
//  FLSearchViewController.swift
//  Fulfilla
//
//  Created by Appzoc on 15/11/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit
import GooglePlaces
import DropDown

class FLSearchViewController: UIViewController {
    
    
    @IBOutlet weak var placeSearchTextField: UITextField!
    
    @IBOutlet weak var serviceSearchTextField: UITextField!
    
    
    @IBOutlet weak var dropDownAnchor: UIView!
    
    @IBOutlet var containerView: UIView!
    
    @IBOutlet var searchTextContainer: UIView!
    
    @IBOutlet var searchLocationContainer: UIView!
    
    
    
    //MARK:- Properties
    var dropDownList:[String] = []
    let dropDown = DropDown()
    var fetcher: GMSAutocompleteFetcher?
    
    var placeID:[String] = []
    var latitude:String = ""
    var longitude:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpInterface()
        setUpDropDown()
        fetcher = GMSAutocompleteFetcher()
        fetcher?.delegate = self
    }
    
    
    @IBAction func placeSearchEdited(_ sender: UITextField) {
        fetcher?.sourceTextHasChanged(placeSearchTextField.text!)
    }
    
    
    @IBAction func submitForSearch(_ sender: UIButton) {
        if let searchKey = serviceSearchTextField.text, let location = placeSearchTextField.text{
            if FLUnwraper.isNotEmpty(string: searchKey) && FLUnwraper.isNotEmpty(string: location){
                let postParameters = ["user_id":"",
                                "search_key":"\(searchKey)",
                                "location_latitude":"\(latitude)",
                                "location_longitude":"\(longitude)",
                                "category":"",
                                "subcategory":"",
                                "isfulltime:boolean":"",
                                "favourite:boolean":""]
                /*
                 user_id,
                 search_key,
                 location_latitude,
                 location_longitude,
                 category,
                 subcategory,
                 isfulltime:boolean,
                 favourite:boolean
                 */
                var responseData:[String:Any] = [:]
                    JSONParser.parseData(url: "", parameters: postParameters, completion: {isComplete, jsonData in
                        responseData = jsonData!
                    })
                print(responseData)
                
            }
        }
    }
    
    
    func setUpInterface(){
        containerView.setFLShadow()
        searchTextContainer.layer.borderWidth = 1
        searchTextContainer.layer.borderColor = UIColor.lightGray.cgColor
        searchTextContainer.layer.cornerRadius = 4
        searchTextContainer.layer.masksToBounds = true
        
        searchLocationContainer.layer.borderWidth = 1
        searchLocationContainer.layer.borderColor = UIColor.lightGray.cgColor
        searchLocationContainer.layer.cornerRadius = 4
        searchLocationContainer.layer.masksToBounds = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setUpDropDown(){
        dropDown.anchorView = dropDownAnchor
        dropDown.dataSource = dropDownList
        dropDown.direction = .bottom
        dropDown.selectionAction = {
            index,item in
            self.placeSearchTextField.text = item
            self.dropDown.hide()
            
            GMSPlacesClient.shared().lookUpPlaceID(self.placeID[index]) { (place, error) in
                if let error = error {
                    print("lookup place id query error: \(error.localizedDescription)")
                    return
                }
                
                guard let place = place else {
                    print("No place details for \(self.placeID)")
                    return
                }
                
                self.latitude = "\(place.coordinate.latitude)"
                self.longitude = "\(place.coordinate.longitude)"
                
                print("Place name \(place.name)")
                print("Place address \(place.formattedAddress)!")
                //print(place.coordinate.latitude)
               // print("Place placeID \(place.placeID)!")
               // print("Place attributions \(place.attributions)!")
               // print(self.placeID)
            }
        }
        
    }
    
    
}


extension FLSearchViewController: GMSAutocompleteFetcherDelegate {
    
    func didAutocomplete(with predictions: [GMSAutocompletePrediction]) {
        dropDownList.removeAll()
        
        var tempPlaceID:[String] = []
        for prediction in predictions {
            dropDownList.append(prediction.attributedPrimaryText.string)
            
            tempPlaceID.append(prediction.placeID!)
            //print("Place ID:\(prediction.placeID)")
            //print("\n",prediction.attributedFullText.string)
            //print("\n",prediction.attributedPrimaryText.string)
            //print("\n********")
        }
        placeID = tempPlaceID
        
        dropDown.dataSource = dropDownList
        dropDown.show()
    }
    
    
    func didFailAutocompleteWithError(_ error: Error) {
        //resultText?.text = error.localizedDescription
        print(error.localizedDescription)
    }
}


