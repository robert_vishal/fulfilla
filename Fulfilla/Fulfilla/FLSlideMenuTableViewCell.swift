//
//  FLSlideMenuTableViewCell.swift
//  Fulfilla
//
//  Created by Appzoc on 13/11/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit

class FLSlideMenuTableViewCell: UITableViewCell {

    @IBOutlet var menuImage: UIImageView!
    @IBOutlet var menuTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}


// Mark: - 
