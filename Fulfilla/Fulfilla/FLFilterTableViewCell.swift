//
//  FLFilterTableViewCell.swift
//  Fulfilla
//
//  Created by Appzoc on 26/10/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit

class FLFilterTableViewCell: UITableViewCell {

    @IBOutlet var listingLabel: UILabel!
    
    @IBOutlet var radioButtonImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
