//
//  FLFirstPostBusinessViewController.swift
//  Fulfilla
//
//  Created by Appzoc on 17/11/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit

class FLFirstPostBusinessViewController: UIViewController {

    @IBOutlet var heightBaseView: NSLayoutConstraint!
    @IBOutlet var CompanyView: UIView!
    @IBOutlet var businessNameTextField: UITextField!
    @IBOutlet var setWorkingHoursLabel: UILabel!
    @IBOutlet var individualView: UIView!
    @IBOutlet var descriptionTextView: UITextView!
    @IBOutlet var daySelectionView: UIView!
    @IBOutlet var startingTimeView: UIView!
    @IBOutlet var closingTimeView: UIView!
    
    @IBOutlet var businessNameView: UIView!
    @IBOutlet var uploadImageCollections: UICollectionView!
    @IBOutlet var uploadVideoCollections: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpInterface()
        uploadImageCollections.register(UINib(nibName: "FLMediaCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FLMediaCollectionViewCell")
        uploadVideoCollections.register(UINib(nibName: "FLMediaCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FLMediaCollectionViewCell")

    }

    func setUpInterface(){
        CompanyView.setBorder(withcolor: UIColor.lightGray, width: 1, andCornerRadius: 4)
        individualView.setBorder(withcolor: UIColor.lightGray, width: 1, andCornerRadius: 4)
        descriptionTextView.setBorder(withcolor: UIColor.lightGray, width: 1, andCornerRadius: 4)
        daySelectionView.setBorder(withcolor: UIColor.lightGray, width: 1, andCornerRadius: 4)
        startingTimeView.setBorder(withcolor: UIColor.lightGray, width: 1, andCornerRadius: 4)
        closingTimeView.setBorder(withcolor: UIColor.lightGray, width: 1, andCornerRadius: 4)
        businessNameView.setBorder(withcolor: UIColor.lightGray, width: 1, andCornerRadius: 4)


    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func companySelectionAction(_ sender: Any) {
        
    }
    
    @IBAction func individualSelectionView(_ sender: Any) {
    }
    
    
    
    @IBAction func nextAction(_ sender: Any) {
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}



extension FLFirstPostBusinessViewController : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cell.appearanceStyle = .twoDimentionalXAxis
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let collectionCell = collectionView.dequeueReusableCell(withReuseIdentifier:"FLMediaCollectionViewCell", for: indexPath as IndexPath) as? FLMediaCollectionViewCell
        
        collectionCell?.playImage.isHidden = true
        if collectionView == uploadImageCollections{
            collectionCell?.cellImage.image = UIImage(named: "postimage")
            
        }else{
            collectionCell?.cellImage.image = UIImage(named: "postvideo")
        }
        
        if indexPath.row == 3 {
            collectionCell?.cellImage.image = UIImage(named: "postplus")
        }
        
        
        return collectionCell!
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(4 - 1))
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(4))
        return CGSize(width: size, height: size)
    }
    
}

