//
//  FLPostBusinessContainerViewController.swift
//  Fulfilla
//
//  Created by Appzoc on 16/11/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit

enum FLPostBusinessStepsSegueType : Int{
    case firstStep
    case secondStep
    case thirdStep
}

class FLPostBusinessContainerViewController: UIViewController {

    @IBOutlet var profilePicture: UIImageView!
    @IBOutlet var businessInfoButton: UIButton!
    @IBOutlet var assignCategoryButton: UIButton!
    @IBOutlet var contactInfoButton: UIButton!
    @IBOutlet var stepsLoadingView: UIView!
    @IBOutlet var stepsLoadingViewHeight: NSLayoutConstraint!
    @IBOutlet var firstStepView: UIView!
    @IBOutlet var secondStepView: UIView!
    @IBOutlet var thirdStepView: UIView!
    @IBOutlet var buttonsView: UIView!
    
    // step one
    @IBOutlet var step1CompanyView: UIView!
    @IBOutlet var step1CompanyImage: UIImageView!
    @IBOutlet var step1IndividualView: UIView!
    @IBOutlet var step1IndividualImage: UIImageView!
    @IBOutlet var step1BusinessNameView: UIView!
    @IBOutlet var step1BusinessNameTextField: UITextField!
    @IBOutlet var step1DescriptionTextView: UITextView!
    @IBOutlet var step1WorkingHoursView: UIView!
    @IBOutlet var step1WorkingHoursImage: UIImageView!
    @IBOutlet var step1DaySelectionView: UIView!
    @IBOutlet var step1SelectDayLabel: UILabel!
    @IBOutlet var step1StartingTimeView: UIView!
    @IBOutlet var step1StartingTimeLabel: UILabel!
    @IBOutlet var step1ClosingTimeView: UIView!
    @IBOutlet var step1ClosingTimeLabel: UILabel!
    @IBOutlet var imageCollectionView: UICollectionView!
    @IBOutlet var videoCollectionView: UICollectionView!
    @IBOutlet var step1Nextbutton: UIButton!
    
    // step two
    @IBOutlet var categoryView: UIView!
    @IBOutlet var categoryLabel: UILabel!
    @IBOutlet var subcategoryView: UIView!
    @IBOutlet var subcategoryLabel: UILabel!
    @IBOutlet var step2NextButton: UIButton!
    
    // step three
    @IBOutlet var phoneView: UIView!
    @IBOutlet var phoneTextField: UITextField!
    @IBOutlet var mobileView: UIView!
    @IBOutlet var mobileTextField: UITextField!
    @IBOutlet var emailView: UIView!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var websiteView: UIView!
    @IBOutlet var websiteTextField: UITextField!
    @IBOutlet var locationView: UIView!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var cityView: UIView!
    @IBOutlet var cityLabel: UILabel!
    @IBOutlet var areaView: UIView!
    @IBOutlet var areaTextField: UITextField!
    @IBOutlet var addressTextView: UITextView!
    @IBOutlet var facebookView: UIView!
    @IBOutlet var facebookTextField: UITextField!
    @IBOutlet var twitterView: UIView!
    @IBOutlet var twitterTextField: UITextField!
    @IBOutlet var linkedInView: UIView!
    @IBOutlet var linkedInTextField: UITextField!
    @IBOutlet var youtubeView: UIView!
    @IBOutlet var youtubeTextField: UIView!
    @IBOutlet var saveButton: UIButton!
    @IBOutlet var submitButton: UIButton!

    @IBOutlet var scrollingView: UIScrollView!
    
//    let secondStepView = FLSecondPostBusinessView.instanceFromNib()
//    let thirdStepView = FLThirdPostBusinessView.instanceFromNib()
    
//    var stepType: FLPostBusinessStepsSegueType = .firstStep
    let kheightFirstStepView : CGFloat = 840
    let kheightSecondStepView : CGFloat = 347
    let kheightThirdStepView : CGFloat = 898
    let koriginStespsView: CGFloat = 228
    
    var isCompany: Bool = true
    var isWorkingHours24 : Bool = true
    
    override func viewDidLoad() {

        super.viewDidLoad()
        
        imageCollectionView.register(UINib(nibName: "FLMediaCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FLMediaCollectionViewCell")
        videoCollectionView.register(UINib(nibName: "FLMediaCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FLMediaCollectionViewCell")
        
        firstStepView.isHidden = false
        secondStepView.isHidden = true
        thirdStepView.isHidden = true
        
        setUpInterface()
        
        businessInfoButton.isUserInteractionEnabled = true
        assignCategoryButton.isUserInteractionEnabled = true
        contactInfoButton.isUserInteractionEnabled = true

//        setSecondPageScreenHeight()
        
//        print(self.view.frame.size,"viewdidload",firstStepView.frame.size)

    }
    

    func setUpInterface(){
        
        profilePicture.layer.cornerRadius = profilePicture.frame.width / 2
        step1CompanyView.setFLBorder()
        step1IndividualView.setFLBorder()
        step1BusinessNameView.setFLBorder()
        step1CompanyView.setFLBorder()
        step1DescriptionTextView.setFLBorder()
        step1WorkingHoursView.setFLBorder()
        step1DaySelectionView.setFLBorder()
        step1StartingTimeView.setFLBorder()
        step1ClosingTimeView.setFLBorder()
        step1Nextbutton.setFLShadow()

        categoryView.setFLBorder()
        subcategoryView.setFLBorder()
        step2NextButton.setFLShadow()

        phoneView.setFLBorder()
        phoneView.setFLBorder()
        mobileView.setFLBorder()
        emailView.setFLBorder()
        websiteView.setFLBorder()
        locationView.setFLBorder()
        cityView.setFLBorder()
        areaView.setFLBorder()
        addressTextView.setFLBorder()
        facebookView.setFLBorder()
        twitterView.setFLBorder()
        linkedInView.setFLBorder()
        youtubeView.setFLBorder()
        youtubeView.setFLShadow()
        saveButton.setFLShadow()
        submitButton.setFLShadow()
        
    }
    
    
    
//    func setSecondPageScreenHeight(){
//        
//        let screenHeight = UIScreen.main.bounds.height
//        if screenHeight >= 568 && screenHeight < 667 {
//            kheightSecondStepView = 272
//        }else if screenHeight >= 736 && screenHeight < 814 {
//            kheightSecondStepView = 412
//        }
//    }
    
    override func viewDidAppear(_ animated: Bool) {

        print("viewDidAppear")
    }
    
    override func viewDidLayoutSubviews(){
        print("viewDidLayoutSubviews")
        print(self.view.frame.size,"viewdidload",firstStepView.frame.size)

    }
   
  
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    /// show first step view
    
    @IBAction func businessInfoAction(_ sender: Any) {

        businessInfoButton.isUserInteractionEnabled = false
        businessInfoButton.setBackgroundImage(#imageLiteral(resourceName: "buttonbgblue"), for: .normal)
        businessInfoButton.setTitleColor(UIColor.white, for: .normal)
        
        firstStepView.isHidden = false
        secondStepView.isHidden = true
        thirdStepView.isHidden = true
        scrollingView.contentSize.height = koriginStespsView + kheightFirstStepView
        
        assignCategoryButton.isUserInteractionEnabled = true
        contactInfoButton.isUserInteractionEnabled = true
        assignCategoryButton.setBackgroundImage(#imageLiteral(resourceName: "buttonbgoutline"), for: .normal)
        assignCategoryButton.setTitleColor(UIColor.darkGray, for: .normal)
        contactInfoButton.setBackgroundImage(#imageLiteral(resourceName: "buttonbgoutline"), for: .normal)
        contactInfoButton.setTitleColor(UIColor.darkGray, for: .normal)

        print(self.view.frame.size,"businessInfoAction",firstStepView.frame.size)

    }
    
    
    /// show second step view
    
    @IBAction func assignCategoryAction(_ sender: Any) {
        
        assignCategoryButton.isUserInteractionEnabled = false
        assignCategoryButton.setBackgroundImage(#imageLiteral(resourceName: "buttonbgblue"), for: .normal)
        assignCategoryButton.setTitleColor(UIColor.white, for: .normal)
        
        secondStepView.isHidden = false
        thirdStepView.isHidden = true
        firstStepView.isHidden = true
        scrollingView.contentSize.height = koriginStespsView + kheightSecondStepView

        
        businessInfoButton.isUserInteractionEnabled = true
        contactInfoButton.isUserInteractionEnabled = true
        
        businessInfoButton.setBackgroundImage(#imageLiteral(resourceName: "buttonbgoutline"), for: .normal)
        businessInfoButton.setTitleColor(UIColor.darkGray, for: .normal)
        
        contactInfoButton.setBackgroundImage(#imageLiteral(resourceName: "buttonbgoutline"), for: .normal)
        contactInfoButton.setTitleColor(UIColor.darkGray, for: .normal)
        print(self.view.frame.size,"assignCategoryAction",secondStepView.frame.size)

    }
    
    
    /// show third step view

    @IBAction func contactInfoAction(_ sender: Any) {
        
        contactInfoButton.isUserInteractionEnabled = false
        contactInfoButton.setBackgroundImage(#imageLiteral(resourceName: "buttonbgblue"), for: .normal)
        contactInfoButton.setTitleColor(UIColor.white, for: .normal)
        
        secondStepView.isHidden = true
        firstStepView.isHidden = true
        thirdStepView.isHidden = false

        scrollingView.contentSize.height = koriginStespsView + kheightThirdStepView

        
        
        businessInfoButton.isUserInteractionEnabled = true
        assignCategoryButton.isUserInteractionEnabled = true
        
        businessInfoButton.setBackgroundImage(#imageLiteral(resourceName: "buttonbgoutline"), for: .normal)
        businessInfoButton.setTitleColor(UIColor.darkGray, for: .normal)
        
        assignCategoryButton.setBackgroundImage(#imageLiteral(resourceName: "buttonbgoutline"), for: .normal)
        assignCategoryButton.setTitleColor(UIColor.darkGray, for: .normal)

        print(self.view.frame.size,"contact info",secondStepView.frame.size)

    }
    
    /// step one actions
    
    @IBAction func stepOneCompanyAction(_ sender: Any) {
        isCompany = true
        step1CompanyImage.image = #imageLiteral(resourceName: "radiofilled")
        step1IndividualImage.image = #imageLiteral(resourceName: "radiounfilled")
    }
    
    
    @IBAction func stepOneIndividualAction(_ sender: Any) {
        isCompany = false
        step1CompanyImage.image = #imageLiteral(resourceName: "radiounfilled")
        step1IndividualImage.image = #imageLiteral(resourceName: "radiofilled")
    }
    
    
    @IBAction func setWorkingHoursAction(_ sender: Any) {
        if isWorkingHours24{
            isWorkingHours24 = false
            step1WorkingHoursImage.image = #imageLiteral(resourceName: "radiounfilled")
        }else{
            isWorkingHours24 = true
            step1WorkingHoursImage.image = #imageLiteral(resourceName: "radiofilled")
        }
    }
    
    
    @IBAction func step1SelectDayAction(_ sender: Any) {
    }
    
    
    @IBAction func step1StartingTimeAction(_ sender: Any) {
    }
    
    
    @IBAction func step1ClosingTimeAction(_ sender: Any) {
    }
    
    
    @IBAction func step1NextAction(_ sender: Any) {
        assignCategoryAction(Any.self)
    }
    
    
    /// step two actions
    
    @IBAction func categoryAction(_ sender: Any) {
        
        let categorySelectionViewController = self.storyboard?.instantiateViewController(withIdentifier: "FLCategorySelectionViewController") as! FLCategorySelectionViewController
        
        categorySelectionViewController.tableTag = 0
        let sideMenuController = UISideMenuNavigationController(rootViewController: categorySelectionViewController)
        
        //        sideMenuController.leftSide = false
        sideMenuController.navigationBar.isHidden = true
        
        self.present(sideMenuController, animated: true, completion: nil)
        
    }
    
    
    @IBAction func subcategoryAction(_ sender: Any) {
        
        let subcategorySelectionViewController = self.storyboard?.instantiateViewController(withIdentifier: "FLCategorySelectionViewController") as! FLCategorySelectionViewController
        subcategorySelectionViewController.tableTag = 1
        
        let sideMenuController = UISideMenuNavigationController(rootViewController: subcategorySelectionViewController)
        
        //        sideMenuController.leftSide = false
        sideMenuController.navigationBar.isHidden = true
        
        self.present(sideMenuController, animated: true, completion: nil)
        
    }
    
    
    @IBAction func step2NextAction(_ sender: Any) {
        contactInfoAction(Any.self)
    }
    
    
    /// step three actions
    
    @IBAction func locationAction(_ sender: Any) {
        
    }
    
    
    
    @IBAction func cityAction(_ sender: Any) {
    }
    
    
    
    @IBAction func saveAction(_ sender: Any) {
        
    }
    
    
    @IBAction func submitAction(_ sender: Any) {
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}


extension FLPostBusinessContainerViewController: UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cell.appearanceStyle = .twoDimentionalXAxis
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let collectionCell = collectionView.dequeueReusableCell(withReuseIdentifier:"FLMediaCollectionViewCell", for: indexPath as IndexPath) as? FLMediaCollectionViewCell
        
        collectionCell?.playImage.isHidden = true
        if collectionView == imageCollectionView{
            collectionCell?.cellImage.image = UIImage(named: "postimage")
            
        }else{
            collectionCell?.cellImage.image = UIImage(named: "postvideo")
        }
        
        if indexPath.row == 3 {
            collectionCell?.cellImage.image = UIImage(named: "postplus")
        }
        
        
        return collectionCell!
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let displayCellCount = 4
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(displayCellCount - 1))
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(displayCellCount))
        return CGSize(width: size, height: size)
    }

}



