//
//  FLLocationListingViewController.swift
//  Fulfilla
//
//  Created by Appzoc on 22/11/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit


protocol FLLocationSelectionDelegate{
    func didTappedLocation(selectedLocation: String, selectedIndexpaths: IndexPath)
}

class FLLocationListingViewController: UIViewController {

    @IBOutlet var listingTable: UITableView!
    
    let locationsDataSource = ["Doha","Al Khor","Al Wakrah","Al Rayyan","Al Daayen","Al Shahaniya","Umm Salal","Al Shamal"]
    var selectedIndexPath: IndexPath? = nil
    var delegate: FLLocationSelectionDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)

    }
    

}


extension FLLocationListingViewController: UITableViewDataSource, UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return locationsDataSource.count
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0
        
        UIView.animate(withDuration: 0.25) {
            cell.alpha = 1
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FLFilterTableViewCell", for: indexPath) as! FLFilterTableViewCell
        cell.selectionStyle = .none
        
        cell.listingLabel.text = locationsDataSource[indexPath.row]
        
        if selectedIndexPath != nil{
            if selectedIndexPath == indexPath{
                cell.radioButtonImage.image = #imageLiteral(resourceName: "radiofilled")
            }else{
                cell.radioButtonImage.image = #imageLiteral(resourceName: "radiounfilled")
            }
        }else{
            cell.radioButtonImage.image = #imageLiteral(resourceName: "radiounfilled")
        }
        
        
        
        
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! FLFilterTableViewCell
        
        if selectedIndexPath != nil {
            let previousSelectedCell = tableView.cellForRow(at: self.selectedIndexPath!) as! FLFilterTableViewCell
            previousSelectedCell.radioButtonImage.image = #imageLiteral(resourceName: "radiounfilled")
        }

        cell.radioButtonImage.image = #imageLiteral(resourceName: "radiofilled")
        

        
        self.selectedIndexPath = indexPath


        self.delegate?.didTappedLocation(selectedLocation: cell.listingLabel.text!, selectedIndexpaths: selectedIndexPath!)
        self.dismiss(animated: true, completion: nil)
        
    }
    
}
