//
//  FLEditProfileViewController.swift
//  Fulfilla
//
//  Created by Appzoc on 22/12/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit

class FLEditProfileViewController: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var profileImage: UIImageView!
    
    
    @IBOutlet weak var userNameField: UITextField!
    
    @IBOutlet weak var emailField: UITextField!
    
    @IBOutlet weak var passwordField: UITextField!
    
    @IBOutlet weak var companyNameField: UITextField!
    
    @IBOutlet weak var designationField: UITextField!
    
    @IBOutlet weak var mobileNumberField: UITextField!
    
    @IBOutlet weak var landNumberField: UITextField!
    
    @IBOutlet weak var scrollViewRef: UIScrollView!
    
    
    
    //MARK:- Actions
    
    @IBAction func saveProfileAction(_ sender: UIButton) {
    }
    
    @IBAction func changeProfileImageAction(_ sender: UIButton) {
    }
    
    @IBAction func backAction(_ sender: UIButton) {
      /*  let segueViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLMyAccountViewController") as! FLMyAccountViewController
        
        //segueViewController.isChat = true
        self.navigationController?.pushViewController(segueViewController, animated: true)
 */
        self.navigationController?.popViewController(animated : true)
    }
    
    
    //MARK:- Methods
    func setUpUIElements(){
         scrollViewRef.contentSize = CGSize(width: 375, height: 732)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUIElements()
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
}
