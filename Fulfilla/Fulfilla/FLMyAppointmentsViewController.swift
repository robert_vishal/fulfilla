//
//  FLMyAppointmentsViewController.swift
//  Fulfilla
//
//  Created by Appzoc on 14/11/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit

class FLMyAppointmentsViewController: UIViewController {

    @IBOutlet var listingTable: UITableView!
    @IBOutlet var receivedButton: UIButton!
    
    @IBOutlet var respondedButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func receivedAction(_ sender: Any) {
        receivedButton.setBackgroundImage(#imageLiteral(resourceName: "buttonbground"), for: .normal)
        receivedButton.setTitleColor(UIColor.white, for: .normal)
        respondedButton.setBackgroundImage(nil, for: .normal)
        respondedButton.setTitleColor(UIColor.darkGray, for: .normal)
        listingTable.reloadData()
    }
    
    @IBAction func respondedAction(_ sender: Any) {
        respondedButton.setBackgroundImage(#imageLiteral(resourceName: "buttonbground"), for: .normal)
        respondedButton.setTitleColor(UIColor.white, for: .normal)
        receivedButton.setBackgroundImage(nil, for: .normal)
        receivedButton.setTitleColor(UIColor.darkGray, for: .normal)
        listingTable.reloadData()

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}



// Mark: -  table view data source and delegates

extension FLMyAppointmentsViewController: UITableViewDataSource, UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 14
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var tableCell = tableView.dequeueReusableCell(withIdentifier: "FLMyAppointmentsTableViewCell") as? FLMyAppointmentsTableViewCell
        
        if tableCell == nil {
            tableView.register(UINib(nibName: "FLMyAppointmentsTableViewCell", bundle: nil), forCellReuseIdentifier: "FLMyAppointmentsTableViewCell")
            tableCell = tableView.dequeueReusableCell(withIdentifier: "FLMyAppointmentsTableViewCell") as? FLMyAppointmentsTableViewCell
        }
        
        tableCell?.selectionStyle = .none
        return tableCell!
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let detailsBusinessViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLDetailsBusinessViewController") as! FLDetailsBusinessViewController
//        
//        self.navigationController?.pushViewController(detailsBusinessViewController, animated: true)
//    }
    
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell  = tableView.cellForRow(at: indexPath as IndexPath) as! FLMyAppointmentsTableViewCell
        cell.contentView.backgroundColor = UIColor.FLtableSelection
    }
    
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell  = tableView.cellForRow(at: indexPath as IndexPath) as! FLMyAppointmentsTableViewCell
        cell.contentView.backgroundColor = .clear
    }
    
}
