//
//  FLDetailsBusinessViewController.swift
//  Fulfilla
//
//  Created by Appzoc on 23/10/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import ImageSlideshow

class FLDetailsBusinessViewController: UIViewController {

    @IBOutlet var aboutUsTable: UITableView!
    @IBOutlet var videoCollections: UICollectionView!
    @IBOutlet var dView: UIView!
    @IBOutlet var heightConstraintDView: NSLayoutConstraint!
    @IBOutlet var showMoreVideosButton: UIButton!
    @IBOutlet var heightConstraintAboutServicesTable: NSLayoutConstraint!
    @IBOutlet var heightContraintAboutUs: NSLayoutConstraint!
    @IBOutlet var imageSlideShowView: ImageSlideshow!
    @IBOutlet var imageSlideButton: UIButton!
    
    
    
    let localSource = [#imageLiteral(resourceName: "samplevideo"),#imageLiteral(resourceName: "samplefavourite"),#imageLiteral(resourceName: "sample")]
    var slideShowDataSource: [ImageSource] = [ImageSource(image: #imageLiteral(resourceName: "sample"))]
    var slideShowCurrentPage:Int = 0
    var tempSlideShowPageValue: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        aboutUsTable.estimatedRowHeight = 60.0
        aboutUsTable.rowHeight = UITableViewAutomaticDimension
        
        videoCollections.register(UINib(nibName: "FLMediaCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FLMediaCollectionViewCell")

        settingImagesSlideShow()
        
    }
    
    
    func settingImagesSlideShow(){
        
        imageSlideShowView.backgroundColor = UIColor.white
        imageSlideShowView.slideshowInterval = 5.0
        imageSlideShowView.pageControlPosition = PageControlPosition.hidden
        imageSlideShowView.pageControl.currentPageIndicatorTintColor = UIColor.green
        imageSlideShowView.pageControl.pageIndicatorTintColor = UIColor.red
        imageSlideShowView.contentScaleMode = UIViewContentMode.scaleAspectFill
        imageSlideShowView.currentPageChanged = { page in
            //print("ImageSlideShow current page:", page)
            self.slideShowCurrentPage = page
        }
        
        if !localSource.isEmpty{
            for image in localSource{
                slideShowDataSource.append(ImageSource(image: image))
            }
        }
        
        imageSlideShowView.setImageInputs(slideShowDataSource)
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(imageSlideAction(_:)))
        imageSlideShowView.addGestureRecognizer(recognizer)

        let swipeButtonRight: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.didSwipeSlideShowView(gesture:)))
        swipeButtonRight.direction = UISwipeGestureRecognizerDirection.right
        self.imageSlideButton.addGestureRecognizer(swipeButtonRight)
        
        let swipeButtonLeft: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.didSwipeSlideShowView(gesture:)))
        swipeButtonLeft.direction = UISwipeGestureRecognizerDirection.left
        self.imageSlideButton.addGestureRecognizer(swipeButtonLeft)
        
    }
    
    func didSwipeSlideShowView(gesture: UIGestureRecognizer) {
        
        let tempCurrentPage = slideShowCurrentPage
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                slideShowCurrentPage -= 1
                if slideShowCurrentPage >= 0 {
                    imageSlideShowView.setNextPage(slideShowCurrentPage, animated: true)
                }else{
                    slideShowCurrentPage = tempCurrentPage
                }
            case UISwipeGestureRecognizerDirection.left:
                slideShowCurrentPage += 1
                if slideShowCurrentPage <= slideShowDataSource.count && slideShowCurrentPage >= 0 && tempSlideShowPageValue < slideShowDataSource.count{
                    imageSlideShowView.setNextPage(slideShowCurrentPage, animated: true)
                }else{
                    slideShowCurrentPage = tempCurrentPage
                    tempSlideShowPageValue = 0
                }
                if (slideShowCurrentPage + 1) == slideShowDataSource.count {
                    tempSlideShowPageValue = slideShowDataSource.count
                }
            default:
                break
            }
        }
        
        
    }
    
    
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func backAction(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
 
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
       changeHeightAboutServiceTable()
    }
    
    override func viewDidLayoutSubviews(){
        
//        heightConstraintAboutServicesTable.constant = aboutUsTable.contentSize.height
//        heightContraintAboutUs.constant = aboutUsTable.frame.height + 10 + 78
       
        changeHeightAboutServiceTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        videoCollections.isUserInteractionEnabled = false
        showMoreVideosButton.isHidden = false
    }
    
    @IBAction func imageSlideAction(_ sender: Any) {
        print("Tapped slideshow")

        imageSlideShowView.presentFullScreenController(from: self)
    }
    
    @IBAction func showMoreVideosAction(_ sender: Any) {
        videoCollections.isUserInteractionEnabled = true
        showMoreVideosButton.isHidden = true
    }
    
    @IBAction func callBackAction(_ sender: Any) {
        
        let callBackViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLCallBackViewController") as! FLCallBackViewController
        
        callBackViewController.modalPresentationStyle = .overCurrentContext
        self.present(callBackViewController, animated: false, completion: nil)
        
        
    }
    
    
    @IBAction func makeAppointmentAction(_ sender: Any) {
        let makeAppointmentViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLMakeAppointmentViewController") as! FLMakeAppointmentViewController
        
        makeAppointmentViewController.modalPresentationStyle = .overCurrentContext
        self.present(makeAppointmentViewController, animated: false, completion: nil)
    }
    
    
    @IBAction func rateAndReviewAction(_ sender: Any) {
        let postReviewViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLPostReviewViewController") as! FLPostReviewViewController
        
        postReviewViewController.modalPresentationStyle = .overCurrentContext
        self.present(postReviewViewController, animated: false, completion: nil)
    }
    
    
    @IBAction func viewAllReviewsAction(_ sender: Any) {
        let notificationViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLNotificationViewController") as! FLNotificationViewController
        
        notificationViewController.listingType = .allReviews

        //        self.navigationController?.transitionStyle = .topToBottom
        self.navigationController?.pushViewController(notificationViewController, animated: true)

    }
    
    
    
    @IBAction func enquiryAction(_ sender: Any) {
        let enquiryViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLEnquiryViewController") as! FLEnquiryViewController
        
        enquiryViewController.modalPresentationStyle = .overCurrentContext
        self.present(enquiryViewController, animated: false, completion: nil)
    }
    
    
    @IBAction func directionAction(_ sender: Any) {
        
    }
    
    
    @IBAction func chatAction(_ sender: Any) {
        
        let chatListingViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLFavouritesListingViewController") as! FLFavouritesListingViewController
        
        chatListingViewController.isChat = true
        
        self.navigationController?.pushViewController(chatListingViewController, animated: true)
        
    }
    
    
    func changeHeightAboutServiceTable(){
        heightConstraintAboutServicesTable.constant = aboutUsTable.contentSize.height
        heightContraintAboutUs.constant = heightConstraintAboutServicesTable.constant + aboutUsTable.frame.origin.y
    }
    
    

    
    
    
    
    func generateVideoThumnail(videoURL videoURLString : String) -> UIImage {
        
        let videoURL = URL(string: videoURLString)
        let asset = AVURLAsset(url: videoURL!)
        let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        assetImgGenerate.requestedTimeToleranceAfter = kCMTimeZero;
        assetImgGenerate.requestedTimeToleranceBefore = kCMTimeZero;
        let timeStamp = CMTime(seconds: 1, preferredTimescale: 60)
        var thumbnailImage : CGImage?
        var capturedImage :UIImage?
        do {
            thumbnailImage = try assetImgGenerate.copyCGImage(at:timeStamp, actualTime: nil)
        } catch {
        }
        if thumbnailImage != nil {
            capturedImage = UIImage(cgImage: thumbnailImage!)
            return capturedImage!
        } else {
            return capturedImage!
        }
        
    }

    
    

}


// Mark:- Handling table view data source and delegate

extension FLDetailsBusinessViewController : UITableViewDataSource, UITableViewDelegate {
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "customCell") as! customCell
        let contentArray = ["In navigation override func prepareLorem ipsum dolor sit ","ation override func prepareLorem ipsum dolor sit er elit lamet,consectetaur cillium adipis","cillium adipis"]
        
        
        cell.servicesLabel.text = contentArray[indexPath.row]
        return cell
    }
    
    
    
}


// Mark:- Handling collection view data source and delegate

extension FLDetailsBusinessViewController :  UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 10
    }
    
   
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cell.appearanceStyle = .twoDimentionalXAxis
    }
    
  
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let collectionCell = collectionView.dequeueReusableCell(withReuseIdentifier:"FLMediaCollectionViewCell", for: indexPath as IndexPath) as? FLMediaCollectionViewCell
        
        
        
        return collectionCell!
    }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(4 - 1))
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(4))
        return CGSize(width: size, height: size)
        
    }
    
    
}

class customCell: UITableViewCell{
    
    @IBOutlet var servicesLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

    
}



