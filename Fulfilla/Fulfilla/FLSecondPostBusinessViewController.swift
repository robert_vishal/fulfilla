//
//  FLSecondPostBusinessViewController.swift
//  Fulfilla
//
//  Created by Appzoc on 17/11/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit

class FLSecondPostBusinessViewController: UIViewController {

    @IBOutlet var categoryView: UIView!
    @IBOutlet var subcategoryView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpInterface()
        

    }
    
    func setUpInterface(){
        categoryView.setBorder(withcolor: UIColor.lightGray, width: 1, andCornerRadius: 4)
        subcategoryView.setBorder(withcolor: UIColor.lightGray, width: 1, andCornerRadius: 4)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func categoryAction(_ sender: Any) {
        
//        let categorySelectionViewController = self.storyboard?.instantiateViewController(withIdentifier: "FLCategorySelectionViewController") as! FLCategorySelectionViewController
        let categorySelectionViewController = self.storyboard?.instantiateViewController(withIdentifier: "FLCategorySelectionViewController") as! FLCategorySelectionViewController
        
        categorySelectionViewController.tableTag = 0
        let sideMenuController = UISideMenuNavigationController(rootViewController: categorySelectionViewController)
        
//        sideMenuController.leftSide = false
        sideMenuController.navigationBar.isHidden = true
        
        self.present(sideMenuController, animated: true, completion: nil)
    }
    
    @IBAction func subcategoryAction(_ sender: Any) {
        
        let subcategorySelectionViewController = self.storyboard?.instantiateViewController(withIdentifier: "FLCategorySelectionViewController") as! FLCategorySelectionViewController
        subcategorySelectionViewController.tableTag = 1

        let sideMenuController = UISideMenuNavigationController(rootViewController: subcategorySelectionViewController)
        
//        sideMenuController.leftSide = false
        sideMenuController.navigationBar.isHidden = true
        
        self.present(sideMenuController, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
