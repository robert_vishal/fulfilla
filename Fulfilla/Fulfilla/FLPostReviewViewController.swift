//
//  FLPostReviewViewController.swift
//  Fulfilla
//
//  Created by Appzoc on 09/11/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit

class FLPostReviewViewController: UIViewController {

    @IBOutlet var containerView: UIView!
    @IBOutlet var bodyView: UIView!
    @IBOutlet var ratingView: FloatRatingView!
    @IBOutlet var titleTextField: UITextField!
    @IBOutlet var messageTextView: UITextView!
    
    var messageString: String = ""
    var ratingCount: Double = 0.0
    let messagePlaceHolder = "  Message"
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        FLAnimations.popUpAnimation(applyToView: containerView, animationStyle: .zoomOut)
        setUpInterface()

    }
    
    
    func setUpInterface(){
        bodyView.setShadow(withColor: UIColor.black, opacity: 0.7, andShadowRadius: 4)
        
        titleTextField.layer.borderWidth = 1
        titleTextField.layer.borderColor = UIColor.lightGray.cgColor
        titleTextField.layer.cornerRadius = 4
        titleTextField.layer.masksToBounds = true
        
        messageTextView.layer.borderWidth = 1
        messageTextView.layer.borderColor = UIColor.lightGray.cgColor
        messageTextView.layer.cornerRadius = 4
        messageTextView.layer.masksToBounds = true
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    @IBAction func submitAction(_ sender: Any) {
        calculateRating()
        if ratingCount == 0.0 {
            FLAnimations.showAutoDismissAlert(withTitle: "", message: "Please do a rating", dismissTime: 1.0, andPresentOn: self)
        }else{
            self.dismiss(animated: false, completion: nil)

        }

    }
    
    func calculateRating(){
        ratingCount = ratingView.rating
        print("ratingCount", ratingCount)
    }

}


// Mark: - Recieving textview events

extension FLPostReviewViewController: UITextViewDelegate {

    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if messageTextView.text == messagePlaceHolder{
            messageTextView.text = ""
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if messageTextView.text == "" {
            messageTextView.textColor = .FLplaceHolderGray
            messageTextView.text = messagePlaceHolder
        }
        if messageTextView.text != messagePlaceHolder{
            messageString = FLUnwraper.unwrap(string: messageTextView.text)
        }
    }
    
}



