//
//  FLHomeViewController.swift
//  Fulfilla
//
//  Created by Appzoc on 10/11/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit
import AnimatedCollectionViewLayout
import CoreLocation
import Alamofire
import Kingfisher

class FLHomeViewController: UIViewController, CLLocationManagerDelegate {
    
    
    @IBOutlet weak var scrollViewRef: UIScrollView!
    
    @IBOutlet var listingCollection: UICollectionView!
    @IBOutlet var listingCollectionHeight: NSLayoutConstraint!
    @IBOutlet var containerViewHeight: NSLayoutConstraint!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var imageSlideCollection: UICollectionView!
    
    
    var userLocationLatitude:CLLocationDegrees?
    var userLocationLongitude:CLLocationDegrees?
    var userLocationSet:Bool = false
    let locationManager = CLLocationManager()
    var fetchedData:HomePageModel?
    
    var collectionViewList:[CategoryModel] = []
    var slidingImageResource:[String] = []
    var imageCollectionList:[HomePageModel.TrendingServices] = []
    
    
    var selectedLocationIndexpath: IndexPath?
    
    let serviceImagesDataSource = [#imageLiteral(resourceName: "wedding"),#imageLiteral(resourceName: "reaest"),#imageLiteral(resourceName: "spa"),#imageLiteral(resourceName: "food"),#imageLiteral(resourceName: "travel"),#imageLiteral(resourceName: "nurse"),#imageLiteral(resourceName: "house-keeping"),#imageLiteral(resourceName: "auto"),#imageLiteral(resourceName: "advertising"),#imageLiteral(resourceName: "maintanance"),#imageLiteral(resourceName: "educa"),#imageLiteral(resourceName: "24x7")]
    let serviceNamesDataSource = ["Wedding Services","Real Estate", "Spa & Beauty", "Food & Beverages","Travel & Tourism", "Health & Medical","House Keeping","Automobile","Advertising","Maintenance","Education & Training","24/7 Services"]
    
    let imageSlideCollectionLayout = AnimatedCollectionViewLayout()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpLocationManager()
        fetchedData = APIfetchAndSetupPage()
       // print(fetchedData?.email)
        listingCollection.register(UINib(nibName: "FLHomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FLHomeCollectionViewCell")
        
        imageSlideCollection.register(UINib(nibName: "FLImageSlideCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FLImageSlideCollectionViewCell")
        
        imageSlideCollection.isPagingEnabled = true
        imageSlideCollectionLayout.scrollDirection = .horizontal
        imageSlideCollectionLayout.animator = LinearCardAttributesAnimator(minAlpha: 0.5, itemSpacing: 0.199, scaleRate: 0.87, changeInScaleRateForX: 0.0, changeInScaleRateForY: 0.13)
        imageSlideCollection.collectionViewLayout = imageSlideCollectionLayout
        imageSlideCollection.showsVerticalScrollIndicator = false
        imageSlideCollection.showsHorizontalScrollIndicator = false
       // setUpCollectionAndScroll()
       // ImageDownloader.default.authenticationChallengeResponder = self
        setUpCollectionAndScroll()
    }
    
    
    func setUpCollectionAndScroll(){
        listingCollection.frame.size.width = self.view.frame.size.width
        
        listingCollection.frame.size.height = listingCollection.collectionViewLayout.collectionViewContentSize.height
        
        
        scrollViewRef.contentSize = CGSize(width: self.view.frame.size.width, height: 200 + listingCollection
            .collectionViewLayout.collectionViewContentSize.height+1000)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("viewDidAppear")
        changeHeightOfContainerViewAndCollectionView()
        //imageSlideCollection.reloadData()
        //listingCollection.reloadData()
    }
    
    override func viewDidLayoutSubviews(){
        
        print("viewDidLayoutSubviews")
        changeHeightOfContainerViewAndCollectionView()
    }

    func changeHeightOfContainerViewAndCollectionView(){
        
       // listingCollectionHeight.constant = listingCollection.contentSize.height
       // containerViewHeight.constant = listingCollectionHeight.constant + listingCollection.frame.origin.y
    }
    
    @IBAction func slideMenuAction(_ sender: Any) {
        
        let slideMenuViewController = self.storyboard?.instantiateViewController(withIdentifier: "FLSlideMenuViewController") as! FLSlideMenuViewController
        let sideMenuController = UISideMenuNavigationController(rootViewController: slideMenuViewController)
        sideMenuController.leftSide = true
        sideMenuController.navigationBar.isHidden = true
        
        self.present(sideMenuController, animated: true, completion: nil)
        
    }
    
    @IBAction func notificationAction(_ sender: Any) {
        let notificationViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLNotificationViewController") as! FLNotificationViewController
        self.navigationController?.pushViewController(notificationViewController, animated: true)
    }
    
    @IBAction func searchAction(_ sender: Any) {
        let searchViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLSearchViewController") as! FLSearchViewController
        self.navigationController?.pushViewController(searchViewController, animated: true)

    }

    @IBAction func callBackAction(_ sender: Any) {
    }
    
    @IBAction func quickFindsAction(_ sender: Any) {
        let quickFindsViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLQuickFindsViewController") as! FLQuickFindsViewController
        self.navigationController?.pushViewController(quickFindsViewController, animated: true)
    }
    
    
    @IBAction func postBusinessAction(_ sender: Any) {
        let postBusinessContainerViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLPostBusinessContainerViewController") as! FLPostBusinessContainerViewController
        self.navigationController?.pushViewController(postBusinessContainerViewController, animated: true)
    }
    
    
    @IBAction func selectLocationAction(_ sender: Any) {
        
       // locationManager.requestLocation()
        //locationManager.startUpdatingLocation()
        
//        while userLocationLongitude != nil && userLocationLatitude != nil {
//            locationManager.stopUpdatingLocation()
//            print(userLocationLatitude)
//            print(userLocationLongitude)
//        }
        if userLocationSet{
            print("Latitude ",userLocationLatitude!," Longitude ",userLocationLongitude!)
        }else{
            print("User location not set")
        }
        print("current location clicked")
        /*
        let locationListingViewController = self.storyboard?.instantiateViewController(withIdentifier: "FLLocationListingViewController") as! FLLocationListingViewController
        
        locationListingViewController.delegate = self
        
        if selectedLocationIndexpath != nil {
            locationListingViewController.selectedIndexPath = selectedLocationIndexpath
        }
        
        
        let sideMenuController = UISideMenuNavigationController(rootViewController: locationListingViewController)
        
        sideMenuController.leftSide = false
        sideMenuController.navigationBar.isHidden = true
        self.present(sideMenuController, animated: true, completion: nil)
        */
        
    }
    
    func setUpLocationManager(){
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.first
        userLocationLatitude = location?.coordinate.latitude
        userLocationLongitude = location?.coordinate.longitude
        userLocationSet = true
        //print(userLocationLongitude)
        //print(userLocationLatitude)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
        print("Failed to get user location")
    }
    
    func updateWithDataFromAPI(){
        collectionViewList = (fetchedData?.categories)!
        imageCollectionList = (fetchedData?.trendingServicesList)!
       // var numberOfAdditionalRows = (Double(collectionViewList.count)/2 - 3.5).rounded(.up)
      //  var newHeightForCollection:CGFloat = CGFloat(Float(listingCollection.frame.height) + Float(125 * numberOfAdditionalRows))
      //  listingCollection.frame = CGRect(x: listingCollection.frame.minX, y: listingCollection.frame.minY, width: listingCollection.frame.width, height: newHeightForCollection)
        //setUpCollectionAndScroll()
        imageSlideCollection.reloadData()
        viewDidLayoutSubviews()
        //viewDidAppear(true)
        listingCollection.reloadData()
        self.scrollViewRef.contentSize = CGSize(width: self.view.frame.size.width, height: self.listingCollection
            .collectionViewLayout.collectionViewContentSize.height+315)
        listingCollection.frame.size.height = listingCollection.collectionViewLayout.collectionViewContentSize.height
        
    }
    
    func APIfetchAndSetupPage() -> HomePageModel?{
        
        var dataResponse:[String:Any] = [:]
        
        
        JSONParser.parseData(url: "http://fulfilla.dev.webcastle.in/api/home-content", parameters: ["user_id":110],httpMethod: HTTPMethod.get, completion: {isComplete,jsonData in
            
            if jsonData != nil{
                dataResponse = jsonData!
                //print(dataResponse)
               // print(dataResponse["Data"])
                self.fetchedData = HomePageModel(with: dataResponse["Data"] as! [String : Any])
               // print(self.fetchedData!)
              //  print(self.fetchedData?.categories)
                //print(dataResponse)
                if let message = dataResponse["Message"] as? String{
                    //print(message)
                    
                 let resourceHolder = self.fetchedData?.trendingServicesList
                    let group = DispatchGroup()
                    group.enter()
                    for item in resourceHolder!{
                        self.slidingImageResource.append(item.imageLink)
                    }
                    group.leave()
                    group.wait()
                self.updateWithDataFromAPI()
                   
                }
            }
            }
        )
        
        //print(dataResponse)
        
        if let message = dataResponse["Message"] as? String{
            //print(message)
        let data = HomePageModel(with: dataResponse["Data"] as! [String : Any])
        //print(data)
            print("\n\n\n\n\n")
            print(data.name)
            print("\n\n\n\n\n")
        return data
        }else{
            return nil
        }
    }
    
}


// Mark: - Collection view data source and delegates

extension FLHomeViewController: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        print("Collection view loading")
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == imageSlideCollection {
            print("imagecol count \(slidingImageResource.count)")
            return slidingImageResource.count
            
        }else{
            print("col count",collectionViewList.count)
            return collectionViewList.count

        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if collectionView != imageSlideCollection{
            cell.appearanceStyle = .twoDimentionalYAxis
            
        }
        
       // collectionView.reloadData()
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        if collectionView == imageSlideCollection {
            let collectionCell = collectionView.dequeueReusableCell(withReuseIdentifier:"FLImageSlideCollectionViewCell", for: indexPath as IndexPath) as? FLImageSlideCollectionViewCell
            
            
           // print("\n\n\n")
//print(imageCollectionList[indexPath.row].imageLink)
           // print("\n\n\n")
            //collectionCell?.slidingImage.kf.setImage(with: URL(string: imageCollectionList[indexPath.row].imageLink ) )
            
//            let task = URLSession.shared.dataTask(with: URL(string: "\(fetchedData?.trendingServicesList[indexPath.row].imageLink)")!, completionHandler: { (data, response, err) in
//                if let image = UIImage(data: data!){
//                    collectionCell?.slidingImage.image = image
//                    //collectionView.reloadData()
//                }
//            }).resume()
            
            
            let username = "dev"
            let password = "dev"
            let loginString = String(format: "%@:%@", username, password)
            let loginData = loginString.data(using: String.Encoding.utf8)!
            let base64LoginString = loginData.base64EncodedString()
            let modifier = AnyModifier { request in
                var r = request
                r.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
                return r
            }
            
           // let resource = ImageResource(downloadURL: URL(string: "http://fulfilla.dev.webcastle.in/storage/app/category/no9IfthMs62INdzQVvhsI3RhwcOoxJRn51zkGBkm.png")!, cacheKey: "http://fulfilla.dev.webcastle.in/storage/app/category/no9IfthMs62INdzQVvhsI3RhwcOoxJRn51zkGBkm.png")
            //imageViewRef.kf.setImage(with: resource)
            
            if let url = URL(string: "\(slidingImageResource[indexPath.row])"){
            
            let resource = ImageResource(downloadURL: url, cacheKey: "\(slidingImageResource[indexPath.row])")
            
            collectionCell?.slidingImage.kf.setImage(with: resource, placeholder: nil, options: [.requestModifier(modifier)], progressBlock: nil, completionHandler: nil)
            }
            
           // collectionCell?.slidingImage.kf.setImage(with: URL(string: "http://fulfilla.dev.webcastle.in/storage/app/businesses/logos/download_1875.jpg"))
            
            collectionCell?.slidingTitle.text = imageCollectionList[indexPath.row].title
            collectionCell?.slidingSubtitle.text = imageCollectionList[indexPath.row].location
            
            
            return collectionCell!
        }else{
            let collectionCell = collectionView.dequeueReusableCell(withReuseIdentifier:"FLHomeCollectionViewCell", for: indexPath as IndexPath) as? FLHomeCollectionViewCell
            
            
            //collectionCell?.serviceImage.image = serviceImagesDataSource[indexPath.row]
           // collectionCell?.serviceName.text = serviceNamesDataSource[indexPath.row]
            
            let username = "dev"
            let password = "dev"
            let loginString = String(format: "%@:%@", username, password)
            let loginData = loginString.data(using: String.Encoding.utf8)!
            let base64LoginString = loginData.base64EncodedString()
            let modifier = AnyModifier { request in
                var r = request
                r.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
                return r
            }
            
            // let resource = ImageResource(downloadURL: URL(string: "http://fulfilla.dev.webcastle.in/storage/app/category/no9IfthMs62INdzQVvhsI3RhwcOoxJRn51zkGBkm.png")!, cacheKey: "http://fulfilla.dev.webcastle.in/storage/app/category/no9IfthMs62INdzQVvhsI3RhwcOoxJRn51zkGBkm.png")
            //imageViewRef.kf.setImage(with: resource)
            
            if let url = URL(string: collectionViewList[indexPath.row].imageLink){
                
                let resource = ImageResource(downloadURL: url, cacheKey: "\(collectionViewList[indexPath.row].imageLink)")
                
                collectionCell?.serviceImage.kf.setImage(with: resource, placeholder: nil, options: [.requestModifier(modifier)], progressBlock: nil, completionHandler: nil)
            }
            
            collectionCell?.serviceName.text = collectionViewList[indexPath.row].name
            //collectionCell?.serviceImage.kf.setImage(with: URL(string: collectionViewList[indexPath.row].imageLink))
            //print(collectionViewList[indexPath.row].imageLink)
            collectionCell?.layer.shadowColor = UIColor.lightGray.cgColor
            collectionCell?.layer.cornerRadius = 4
            collectionCell?.layer.shadowOffset = CGSize(width: 0, height: 0.0)
            collectionCell?.layer.shadowRadius = 2.0
            collectionCell?.layer.shadowOpacity = 1.0
            collectionCell?.layer.masksToBounds = false
            
            collectionCell?.layoutIfNeeded()
            
            return collectionCell!
            
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        if collectionView == imageSlideCollection{
            
            return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
            // * 1.14
        }else{
            
            let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
            let cellsCount = 2
            let totalSpace = flowLayout.sectionInset.left
                + flowLayout.sectionInset.right
                + (flowLayout.minimumInteritemSpacing * CGFloat(cellsCount - 1))
            let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(cellsCount))
            let height = Int(Double(size) * 0.825)
            
            return CGSize(width: size, height: height)
            
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView == imageSlideCollection{
            
        }else{
            let businessListingViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLBusinsessListingViewController") as! FLBusinsessListingViewController
            self.navigationController?.pushViewController(businessListingViewController, animated: true)

        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == imageSlideCollection{
            return 0
        }else{
            return 8
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == imageSlideCollection{
            return 0
        }else{
            return 6
        }
    }
    
    
    
    
}

//extension FLHomeViewController: AuthenticationChallengeResponsable {
//    func downloader(_ downloader: ImageDownloader,
//                    didReceive challenge: URLAuthenticationChallenge,
//                    completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void)
//    {
//        // Provide your `AuthChallengeDisposition` and `URLCredential`
//        let disposition: URLSession.AuthChallengeDisposition = URLSession.AuthChallengeDisposition.useCredential
//        let credential: URLCredential? = URLCredential(user: "dev", password: "dev", persistence: .forSession)
//            completionHandler(disposition, credential)
//    }
//    
//    
//    func downloader(_ downloader: ImageDownloader, task: URLSessionTask, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void){
//        
//    }
//}

extension FLHomeViewController: FLLocationSelectionDelegate{
    func didTappedLocation(selectedLocation: String, selectedIndexpaths: IndexPath) {
        self.selectedLocationIndexpath = selectedIndexpaths
        self.locationLabel.text = selectedLocation
    }
}

