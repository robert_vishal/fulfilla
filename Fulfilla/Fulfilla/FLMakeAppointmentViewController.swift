//
//  FLMakeAppointmentViewController.swift
//  Fulfilla
//
//  Created by Appzoc on 08/11/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit
import DTCalendarView

class FLMakeAppointmentViewController: UIViewController {
    
    enum PickerType{
        case date
        case time
    }
    
    @IBOutlet var containerView: UIView!
    @IBOutlet var bodyView: UIView!
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var phoneTextField: UITextField!
    @IBOutlet var messageTextView: UITextView!
    @IBOutlet var dateView: UIView!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var timeView: UIView!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var calendarContainer: UIView!
    @IBOutlet var datePicker: DTCalendarView! {
        didSet {
            datePicker.delegate = self
            datePicker.displayEndDate = kcalenderEndDate
            datePicker.previewDaysInPreviousAndMonth = false
            datePicker.paginateMonths = true
        }
    }
    @IBOutlet var timePicker: UIDatePicker!
    
    /// calculating calender end date -  calender of 5 years at a time
    let kcalenderEndDate = Date(timeIntervalSinceNow: 60 * 60 * 24 * 30 * 12 * 5)
    let currentDate = Date()
    let calendar = Calendar.current
    var calendarContentHeight: CGFloat = 28
    var calendarContainerOrigin: CGPoint = CGPoint(x: 44, y: 118)
    var selectedDateTime: Date?
    var selectedPicker: PickerType = PickerType.date
    var nameString: String = ""
    var emailString: String = ""
    var phoneString: String = ""
    var dateString: String = ""
    var timeString: String = ""
    var messageString: String = ""
    let messagePlaceHolder = "  Message"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        FLAnimations.popUpAnimation(applyToView: containerView, animationStyle: .zoomOut)
        setUpInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        calendarContentHeight = self.calendarContainer.frame.height * (7/63)
        calendarContainerOrigin = calendarContainer.frame.origin
    }
    
    func setUpInterface(){
        
        bodyView.setFLShadow()
        
        nameTextField.layer.borderWidth = 1
        nameTextField.layer.borderColor = UIColor.lightGray.cgColor
        nameTextField.layer.cornerRadius = 4
        nameTextField.layer.masksToBounds = true
        
        emailTextField.layer.borderWidth = 1
        emailTextField.layer.borderColor = UIColor.lightGray.cgColor
        emailTextField.layer.cornerRadius = 4
        emailTextField.layer.masksToBounds = true
        
        phoneTextField.layer.borderWidth = 1
        phoneTextField.layer.borderColor = UIColor.lightGray.cgColor
        phoneTextField.layer.cornerRadius = 4
        phoneTextField.layer.masksToBounds = true

        messageTextView.layer.borderWidth = 1
        messageTextView.layer.borderColor = UIColor.lightGray.cgColor
        messageTextView.layer.cornerRadius = 4
        messageTextView.layer.masksToBounds = true
        
        dateView.layer.borderWidth = 1
        dateView.layer.borderColor = UIColor.lightGray.cgColor
        dateView.layer.cornerRadius = 4
        dateView.layer.masksToBounds = true
        
        timeView.layer.borderWidth = 1
        timeView.layer.borderColor = UIColor.lightGray.cgColor
        timeView.layer.cornerRadius = 4
        timeView.layer.masksToBounds = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)

    }
    
    @IBAction func selectDateAction(_ sender: Any) {
        calendarContainer.isHidden = false
        selectedPicker = PickerType.date
        timePicker.isHidden = true
        datePicker.isHidden = false
        
        FLAnimations.presentView(view: calendarContainer, originOfView: calendarContainerOrigin, inView: containerView, direction: .bottom, isDismiss: false)
        
    }
    
    
    @IBAction func selectTimeAction(_ sender: Any) {
        calendarContainer.isHidden = false
        selectedPicker = PickerType.time
        timePicker.isHidden = false
        datePicker.isHidden = true
        FLAnimations.presentView(view: calendarContainer, originOfView: calendarContainerOrigin, inView: containerView, direction: .bottom, isDismiss: false)
    }
    
    
    @IBAction func submitAction(_ sender: Any) {

        printParams()
        
        if FLUnwraper.isNotEmpty(string: nameString) && FLUnwraper.isNotEmpty(string: phoneString) && FLUnwraper.isNotEmpty(string: dateString) && FLUnwraper.isNotEmpty(string: timeString){
            FLAnimations.activity.show(inView: self.view)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + FLAssistant.waitingTime) {
                FLAnimations.activity.hide()
                FLAnimations.showEscapingAlert(withTitle: "", message: "Submitted successfully.", andPresentOn: self, escapingFuncName: self.callBackFunction)
                
            }

        }else{
            FLAnimations.showAlert(withTitle: "", message: "Please fill all fields before submitting.", andPresentOn: self)
            
        }
        
    }
    
    
    func callBackFunction(){
        
        print("return_controllll")
    }
    
    
    
    func printParams(){
        print("Name:", nameString)
        print("email:", emailString)
        print("phone:", phoneString)
        print("message:", messageString)
        print("date:",dateString)
        print("time:",timeString)
    }
    
    
//    func showAlert(withTitle: String, message: String, dismissTime: Double){
//        let alert = UIAlertController(title: withTitle, message: message, preferredStyle: UIAlertControllerStyle.alert)
//        self.present(alert, animated: true, completion: nil)
//        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + dismissTime){
//            alert.dismiss(animated: true, completion: nil)
//        }
//    }
    
    
    

    
    
    @IBAction func calendarDoneAction(_ sender: Any) {
        
        FLAnimations.presentView(view: calendarContainer, originOfView: calendarContainerOrigin, inView: containerView, direction: .bottom, isDismiss: true)

        if selectedPicker == .date{
            guard let date = selectedDateTime else {
                dateLabel.textColor = .FLplaceHolderGray
                dateLabel.text = "Select Date"
                selectedDateTime = nil
                return
            }
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .medium
            dateLabel.textColor = .black
            dateLabel.text = dateFormatter.string(from: date)
            selectedDateTime = nil

        }else{
            let dateFormatter = DateFormatter()
            dateFormatter.timeStyle = .short

            guard let date = selectedDateTime else {
                timeLabel.textColor = .black
                timeLabel.text = dateFormatter.string(from: timePicker.date)
                selectedDateTime = nil
                return
            }
            timeLabel.text = dateFormatter.string(from: date)
            selectedDateTime = nil

        }

    }
    
    
    @IBAction func calendarCancelAction(_ sender: Any) {
        FLAnimations.presentView(view: calendarContainer, originOfView: calendarContainerOrigin, inView: containerView, direction: .bottom, isDismiss: true)
        selectedDateTime = nil

    }
    
    
    @IBAction func timePickerAction(_ sender: Any) {
        selectedDateTime = timePicker.date
    }

}



// Mark: - Recieving textfield and textview Events

extension FLMakeAppointmentViewController: UITextViewDelegate, UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == nameTextField {
            nameString = FLUnwraper.unwrap(string: nameTextField.text)
        }else if textField == emailTextField {
            emailString = FLUnwraper.unwrap(string: emailTextField.text)
            if !FLUnwraper.isValid(email: emailString){
                FLAnimations.showAutoDismissAlert(withTitle: "", message: "Please enter valid email", dismissTime: 1.5, andPresentOn: self)
            }
        }else {
            phoneString = FLUnwraper.unwrap(string: phoneTextField.text)
            if !FLUnwraper.isValid(phone: phoneString){

                FLAnimations.showAutoDismissAlert(withTitle: "", message: "Please enter valid phone number", dismissTime: 1.5, andPresentOn: self)

            }
        }
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        messageTextView.textColor = .black
        if textView.text == messagePlaceHolder{
            messageTextView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if messageTextView.text == "" {
            messageTextView.textColor = .FLplaceHolderGray
            messageTextView.text = messagePlaceHolder
        }
        if messageTextView.text != messagePlaceHolder{
            messageString = FLUnwraper.unwrap(string: messageTextView.text)
        }
    }
    
}




// Mark: - Recieving calendar events

extension FLMakeAppointmentViewController: DTCalendarViewDelegate{
    

    func calendarView(_ calendarView: DTCalendarView, dragFromDate fromDate: Date, toDate: Date){
        
    }
    
    func calendarView(_ calendarView: DTCalendarView, viewForMonth month: Date) -> UIView {
        
        let label = UILabel()
        label.text = DateFormatter().monthYearFormatter(date: month)
        label.textColor = UIColor.black
        label.textAlignment = .center
        label.backgroundColor = UIColor.white
        
        return label
    }
    
    func calendarView(_ calendarView: DTCalendarView, disabledDaysInMonth month: Date) -> [Int]? {
        
        if currentDate(matchesMonthAndYearOf: month) {
            var disabledDays = [Int]()
            let nowDay = Calendar.current.component(.day, from: currentDate)
            for day in 1 ..< nowDay {
                disabledDays.append(day)
            }
            return disabledDays
        }
        return nil
    }
    
    func currentDate(matchesMonthAndYearOf date: Date) -> Bool {
        let nowMonth = Calendar.current.component(.month, from: currentDate)
        let nowYear = Calendar.current.component(.year, from: currentDate)
        
        let askMonth = Calendar.current.component(.month, from: date)
        let askYear = Calendar.current.component(.year, from: date)
        
        if nowMonth == askMonth && nowYear == askYear {
            return true
        }
        
        return false
    }
    
    func calendarView(_ calendarView: DTCalendarView, didSelectDate date: Date) {
        print("didSelectDate",date)
        
        if let nowDayOfYear = Calendar.current.ordinality(of: .day, in: .year, for: currentDate),
            let selectDayOfYear = Calendar.current.ordinality(of :.day, in: .year, for: date),
            Calendar.current.component(.year, from: currentDate) == Calendar.current.component(.year, from: date),
            selectDayOfYear < nowDayOfYear {
            return
        }

        calendarView.selectionStartDate = date
        selectedDateTime = date
        
    }
    
    func calendarViewHeightForWeekRows(_ calendarView: DTCalendarView) -> CGFloat {

        return calendarContentHeight
    }
    
    func calendarViewHeightForWeekdayLabelRow(_ calendarView: DTCalendarView) -> CGFloat {
        return calendarContentHeight
    }
    
    func calendarViewHeightForMonthView(_ calendarView: DTCalendarView) -> CGFloat {
        return calendarContentHeight
    }

    
    
    
    
    
    
    
    
}
