//
//  FLNotificationTableViewCell.swift
//  Fulfilla
//
//  Created by Appzoc on 09/11/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit

class FLNotificationTableViewCell: UITableViewCell {

    @IBOutlet var profileImage: UIImageView!
    
    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet var timeLabel: UILabel!
    
    @IBOutlet var notificationButton: UIButton!
    @IBOutlet var baseView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpInterface()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpInterface(){
        profileImage.layer.cornerRadius = profileImage.frame.width / 2
        baseView.setFLShadow()
    }

    
}
