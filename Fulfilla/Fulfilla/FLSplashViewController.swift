//
//  FLSplashViewController.swift
//  Fulfilla
//
//  Created by Appzoc on 12/10/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit

class FLSplashViewController: UIViewController {

    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()

        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            
            let landingViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLLandingViewController") as! FLLandingViewController
            self.navigationController?.transitionStyle = .fade
            self.navigationController?.pushViewController(landingViewController, animated: false)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
