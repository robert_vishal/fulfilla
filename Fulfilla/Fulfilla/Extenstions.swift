//
//  Extenstions.swift
//  Fulfilla
//
//  Created by Appzoc on 11/10/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import Foundation
import UIKit

// Mark: - Enums for animations

public enum FLTrasitionStyle:Int{
    case leftToRight
    case rightToLeft
    case topToBottom
    case bottomToTop
    case fade
}

public enum FLAppearanceStyle:Int {
    case threeDimenstional
    case twoDimentionalYAxis
    case twoDimentionalXAxis
    case listingYAxis
}


// Mark: - Adding additional colors
extension UIColor {
    
    public class var FLlightBlue: UIColor { get { return UIColor(colorLiteralRed: 32/255, green: 38/255, blue: 96/255, alpha: 1.0)}}
    
    public class var FLblue: UIColor { get { return UIColor(colorLiteralRed: 18/255, green: 26/255, blue: 104/255, alpha: 1.0)}}
    
    public class var FLdarkBlue: UIColor { get { return UIColor(colorLiteralRed: 5/255, green: 13/255, blue: 100/255, alpha: 1.0)}}
    
    public class var FLyellow: UIColor { get { return UIColor(colorLiteralRed: 253/255, green: 194/255, blue: 16/255, alpha: 1.0)}}
    
    public class var FLorange: UIColor { get { return UIColor(colorLiteralRed: 228/255, green: 76/255, blue: 75/255, alpha: 1.0)}}
    /// For button orange color - button color change while clicking
    public class var FLbuttonOrange: UIColor { get { return UIColor(colorLiteralRed: 255/255, green: 102/255, blue: 102/255, alpha: 1.0)}}
    /// For button blue color - button color change while clicking
    public class var FLbuttonBlue: UIColor { get { return UIColor(colorLiteralRed: 54/255, green: 70/255, blue: 143/255, alpha: 1.0)}}
    
    public class var FLtableBackground: UIColor { get { return UIColor(colorLiteralRed: 204/255, green: 204/255, blue: 204/255, alpha: 1.0)}}
    
    public class var FLtableSelection: UIColor { get { return UIColor(colorLiteralRed: 0/255, green: 122/255, blue: 255/255, alpha: 0.03)}}

    public class var FLplaceHolderGray: UIColor { get { return UIColor(colorLiteralRed: 153/255, green: 153/255, blue: 153/255, alpha: 1.0)}}
    
    public class var FLpopupGray: UIColor { get { return UIColor(colorLiteralRed: 128/255, green: 128/255, blue: 128/255, alpha: 1.0)}}

    public class func hexColor(hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
}

//Mark: - Add corner radius to all views



extension UIView{

@IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    
    
    /// Apply shadow 
    func setShadow(withColor color: UIColor, opacity: Float ,andShadowRadius radius: CGFloat) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = radius
//        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
//        self.layer.shouldRasterize = true
//        self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
   
    
    func setFLShadow(){
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 0.7
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 4
    }
    
    
    func setBorder(withcolor color: UIColor, width: CGFloat, andCornerRadius radius: CGFloat){
        self.layer.borderWidth = width
        self.layer.borderColor = color.cgColor
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }

    func setFLBorder(){
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.cornerRadius = 4
        self.layer.masksToBounds = true
    }
    
    func setBorderOnEdge(edge: UIRectEdge, color: UIColor, andThickness thickness: CGFloat) {
        
        let border = self.layer
        
        switch edge {
        case UIRectEdge.top:
            border.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: thickness)
            break
        case UIRectEdge.bottom:
            border.frame = CGRect(x: 0, y: self.frame.height - thickness, width: self.frame.width, height: thickness)
            break
        case UIRectEdge.left:
            border.frame = CGRect(x: 0, y: 0, width: thickness, height: self.frame.height)
            break
        case UIRectEdge.right:
            border.frame = CGRect(x: self.frame.width - thickness, y: 0, width: thickness, height: self.frame.height)
            break
        default:
            break
        }
        
        border.backgroundColor = color.cgColor;
        
    }
}

// Mark: - Add custom Trasition style by CATransition

extension UIViewController{
    
    
    var transitionStyle: FLTrasitionStyle{
        get{
            return FLTrasitionStyle.leftToRight
        }
        set{
            let transition = CATransition()
            transition.duration = 0.3
            switch newValue {
            case .leftToRight:
                transition.type = kCATransitionPush
                transition.subtype = kCATransitionFromLeft
                apply(customTransition: transition)
            case .rightToLeft:
                transition.type = kCATransitionPush
                transition.subtype = kCATransitionFromRight
                apply(customTransition: transition)
            case .topToBottom:
                transition.duration = 0.4
                transition.type = kCATransitionPush
                transition.subtype = kCATransitionFromTop
                apply(customTransition: transition)
            case .bottomToTop:
                transition.type = kCATransitionPush
                transition.subtype = kCATransitionFromBottom
                apply(customTransition: transition)
            case .fade:
                transition.duration = 0.2
                transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                transition.type = kCATransitionFade
                apply(customTransition: transition)
            }
        }
    }
    func apply(customTransition: CATransition){
        self.view.window!.layer.add(customTransition, forKey: kCATransition)
    }
}




// Mark: - Add custom alert with OK button

//extension UIViewController{
//    func FLAlert(withTitle title: String, andMessage message: String)
//    {
//        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
//        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
//        self.present(alert, animated: true, completion: nil)
//        
//    }
//
//}



// Mark: - Add animated apearance to table view cell

extension UITableViewCell{
    
    var appearanceStyle: FLAppearanceStyle{
        get{
            return FLAppearanceStyle.twoDimentionalYAxis
        }
        set{
            switch newValue {
            case .threeDimenstional:
                self.layer.transform = CATransform3DMakeScale(0.1,0.1,1)
                UIView.animate(withDuration: 0.6, animations: {
                    self.layer.transform = CATransform3DMakeScale(1.02,1.02,1.02)
                },completion: { finished in
                    UIView.animate(withDuration: 0.1, animations: {
                        self.layer.transform = CATransform3DMakeScale(1,1,1)
                    })
                })
            case .listingYAxis:
                
                self.alpha = 0
                self.transform = CGAffineTransform(translationX: 0, y: -100)
                UIView.beginAnimations("rotation", context: nil)
                UIView.setAnimationDuration(0.45)
                self.transform = CGAffineTransform(translationX: 0, y: 0)
                self.alpha = 1
                self.layer.shadowOffset = CGSize(width: 0, height: 0)
                UIView.commitAnimations()


            case .twoDimentionalYAxis:
                
                self.alpha = 0
                self.transform = CGAffineTransform(translationX: 0, y: 30)
                UIView.beginAnimations("rotation", context: nil)
                UIView.setAnimationDuration(0.6)
                self.transform = CGAffineTransform(translationX: 0, y: 0)
                self.alpha = 1
                self.layer.shadowOffset = CGSize(width: 0, height: 0)
                UIView.commitAnimations()
                
            case .twoDimentionalXAxis:
                
                self.alpha = 0
                self.transform = CGAffineTransform(translationX: 50, y: 0)
                UIView.beginAnimations("rotation", context: nil)
                UIView.setAnimationDuration(0.4)
                self.transform = CGAffineTransform(translationX: 0, y: 0)
                self.alpha = 1
                self.layer.shadowOffset = CGSize(width: 0, height: 0)
                UIView.commitAnimations()
            }
        }
    }

}

// Mark: - Add animated apearance to table view cell

extension UICollectionViewCell{
    
    var appearanceStyle: FLAppearanceStyle{
        get{
            return FLAppearanceStyle.twoDimentionalYAxis
        }
        set{
            switch newValue {
            case .threeDimenstional:
                self.layer.transform = CATransform3DMakeScale(0.1,0.1,1)
                UIView.animate(withDuration: 0.6, animations: {
                    self.layer.transform = CATransform3DMakeScale(1.02,1.02,1.02)
                },completion: { finished in
                    UIView.animate(withDuration: 0.1, animations: {
                        self.layer.transform = CATransform3DMakeScale(1,1,1)
                    })
                })
                
            case .twoDimentionalYAxis:
                self.alpha = 0
                self.transform = CGAffineTransform(translationX: 0, y: 100)
                UIView.beginAnimations("rotation", context: nil)
                UIView.setAnimationDuration(0.5)
                self.transform = CGAffineTransform(translationX: 0, y: 0)
                self.alpha = 1
                self.layer.shadowOffset = CGSize(width: 0, height: 0)
                UIView.commitAnimations()
                
           case .twoDimentionalXAxis:
                self.alpha = 0.3
                self.transform = CGAffineTransform(translationX: 30, y: 0)
                UIView.beginAnimations("rotation", context: nil)
                UIView.setAnimationDuration(0.6)
                self.transform = CGAffineTransform(translationX: 0, y: 0)
                self.alpha = 1
                self.layer.shadowOffset = CGSize(width: 0, height: 0)
                UIView.commitAnimations()
                
           case .listingYAxis:
                self.layer.transform = CATransform3DIdentity
                
                UIView.animate(withDuration: 0.6, animations: {
                    self.layer.transform.m34 = 1.0 / -500
                },completion: { finished in
                    UIView.animate(withDuration: 0.1, animations: {
                        self.layer.transform = CATransform3DRotate(self.layer.transform, CGFloat(Double.pi / 2), 0, 1, 1)
                    })
                })

            }
        }
    }
    
}


extension DateFormatter{
    func monthYearFormatter(date: Date) -> String{
        let formatter = DateFormatter()
        formatter.timeStyle = .none
        formatter.dateFormat = "MMMM YYYY"
        return formatter.string(from: date)
    }
}




// Mark: - Add image to the annotation view

//extension MKPointAnnotation{
//    var pinCustomImageName:String {
//        get {
//            // round pushpin  Unicode: U+1F4CD, UTF-8: F0 9F 93 8D
//            return "📍"
//        }
//              set{
//                
//        }
//    }
//
//   
//    
//}




