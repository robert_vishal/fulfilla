//
//  MarkerWithImage.swift
//  Fulfilla
//
//  Created by Appzoc on 14/10/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import Foundation
import MapKit
class MarkerWithImage: MKPointAnnotation {
    var imageName:String!
}
