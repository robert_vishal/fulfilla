//
//  FLCategorySelectionViewController.swift
//  Fulfilla
//
//  Created by Appzoc on 18/11/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit


class FLCategorySelectionViewController: UIViewController {

    
    @IBOutlet var searchView: UIView!
    @IBOutlet var searchTextField: UITextField!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var listingTable: UITableView!
    
    @IBOutlet var clearButton: UIButton!
    @IBOutlet var applyFilterButton: UIButton!
    
    var tableTag: Int = 0
    
    
    
    let categoryDataSource = ["Medical Shops", "Hospital & Clinics", "Dental Clinic", "Ayurvedic Medicines", "Medical Supplies & Equipments","Homeopathy","Dental","Medical Shops", "Hospital & Clinics", "Dental Clinic", "Ayurvedic Medicines", "Medical Supplies & Equipments","Homeopathy","Dental"]
    
    let subCategoryDataSource = ["Medical Shops", "Hospital & Clinics", "Dental Clinic", "Ayurvedic Medicines", "Medical Supplies & Equipments","Homeopathy","Dental","Medical Shops", "Hospital & Clinics", "Dental Clinic", "Ayurvedic Medicines", "Medical Supplies & Equipments","Homeopathy","Dental"]


    var categorySelectedIndexPaths: [IndexPath] = []
    var subCategorySelectedIndexPaths: [IndexPath] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        listingTable.tag = tableTag
        setUpInterface()
    }
    
    func setUpInterface(){
        
        switch tableTag {
        case 0:
            self.titleLabel.text = "Choose Categories"

        case 1:
            self.titleLabel.text = "Choose Subcategories"

        default: break
        }
        searchView.setBorder(withcolor: UIColor.lightGray, width: 0.7, andCornerRadius: 4)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    
    @IBAction func searchAction(_ sender: Any) {
    }
    
    
    @IBAction func clearAction(_ sender: Any) {
    }
    
    @IBAction func applyFilterAction(_ sender: Any) {
    }
    
   
}



// Mark: - Table view data source and delegates

extension FLCategorySelectionViewController: UITableViewDataSource, UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView.tag == 0{
            return categoryDataSource.count
        }else{
            return subCategoryDataSource.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0
        
        UIView.animate(withDuration: 0.25) {
            cell.alpha = 1
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FLFilterTableViewCell", for: indexPath) as! FLFilterTableViewCell
        cell.selectionStyle = .none
        
        
        if tableView.tag == 0{
            cell.listingLabel.text = categoryDataSource[indexPath.row]
            if categorySelectedIndexPaths.contains(indexPath){
                cell.radioButtonImage.image = #imageLiteral(resourceName: "radiofilled")
            }else{
                cell.radioButtonImage.image = #imageLiteral(resourceName: "radiounfilled")
            }
        }else {
            cell.listingLabel.text = subCategoryDataSource[indexPath.row]
            if subCategorySelectedIndexPaths.contains(indexPath){
                cell.radioButtonImage.image = #imageLiteral(resourceName: "radiofilled")
            }else{
                cell.radioButtonImage.image = #imageLiteral(resourceName: "radiounfilled")
            }
            
        }
      
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! FLFilterTableViewCell
        
        if tableView.tag == 0{
            if !self.categorySelectedIndexPaths.contains(indexPath){
                self.categorySelectedIndexPaths.append(indexPath)
                cell.radioButtonImage.image = #imageLiteral(resourceName: "radiofilled")
            }
        }else {
            if !self.subCategorySelectedIndexPaths.contains(indexPath){
                self.subCategorySelectedIndexPaths.append(indexPath)
                cell.radioButtonImage.image = #imageLiteral(resourceName: "radiofilled")
            }
        }
        
        
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! FLFilterTableViewCell
        
        if tableView.tag == 0{
            if self.categorySelectedIndexPaths.contains(indexPath){
                cell.radioButtonImage.image = #imageLiteral(resourceName: "radiounfilled")
                self.categorySelectedIndexPaths.remove(at: self.categorySelectedIndexPaths.index(of: indexPath)!)
            }
        }else{
            if self.subCategorySelectedIndexPaths.contains(indexPath){
                cell.radioButtonImage.image = #imageLiteral(resourceName: "radiounfilled")
                self.subCategorySelectedIndexPaths.remove(at: self.subCategorySelectedIndexPaths.index(of: indexPath)!)
                
            }
        }
        
    }
    
    
}
