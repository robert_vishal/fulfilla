//
//  FLBusinsessListingViewController.swift
//  Fulfilla
//
//  Created by Appzoc on 13/10/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit
import Alamofire


class FLBusinsessListingViewController: UIViewController {
//FLListViewBusinessListingTableViewCell
    
    @IBOutlet var listingTableView: UITableView!
    
    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet var gridViewSelection: UIButton!
    
    @IBOutlet var listingCollectionView: UICollectionView!
    
    
    //MARK:- Properties
    var tableData:[FLProperty] = []
    
    var objectList:[BusinessListingModel] = []
    
    var externalDataSource = false
    
    override func viewDidLoad() {
       
        super.viewDidLoad()
        listingTableView.tag = 1
        // Do any additional setup after loading the view.

        loadDataFromDictionary()
        listingCollectionView.isHidden = true
        listingTableView.isHidden = false
        listingCollectionView.register(UINib(nibName: "FLGridViewCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FLGridViewCollectionViewCell")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if externalDataSource == false{
//        DispatchQueue.global().sync {
//            loadData()
//            listingTableView.reloadData()
//        }
            loadDataFromDictionary()
        }
        
    }

    @IBAction func backAction(_ sender: Any) {
        
//        let landingViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLLandingViewController") as! FLLandingViewController
//        
//        self.navigationController?.transitionStyle = .leftToRight
//        self.navigationController?.pushViewController(landingViewController, animated: false)
        
        
        let homeViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLHomeViewController") as! FLHomeViewController
        self.navigationController?.transitionStyle = .leftToRight
        self.navigationController?.pushViewController(homeViewController, animated: false)
        
//        self.navigationController?.popViewController(animated: false)
        
    }
    

    @IBAction func mapAction(_ sender: Any) {
        let mapListingViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLMapListingViewController") as! FLMapListingViewController
        mapListingViewController.segueType = .none
        self.navigationController?.pushViewController(mapListingViewController, animated: true)
    }
    
    @IBAction func notificationAction(_ sender: Any) {
        let notificationViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLNotificationViewController") as! FLNotificationViewController
        self.navigationController?.pushViewController(notificationViewController, animated: true)
    }
    
    @IBAction func gridViewAction(_ sender: Any) {
        gridViewSelection.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10)
        if listingTableView.isHidden{
            gridViewSelection.setImage(UIImage(named: "listview"), for: .normal)
            gridViewSelection.setTitle("  List View", for: .normal)
            self.listingCollectionView.isHidden = true
            self.listingTableView.isHidden = false
            
//            self.listingTableView.alpha = 0.0
//            self.listingCollectionView.alpha = 0.0
//            UIView.animate(withDuration: 0.5, animations: {
//                self.listingTableView.alpha = 1.0
//
//            })
            listingTableView.reloadData()

        }else{
            gridViewSelection.setImage(UIImage(named: "gridview"), for: .normal)
            gridViewSelection.setTitle("  Grid View", for: .normal)
            
            self.listingCollectionView.isHidden = false
            self.listingTableView.isHidden = true
//            self.listingTableView.alpha = 0.0
//            self.listingCollectionView.alpha = 0.0
//            UIView.animate(withDuration: 0.5, animations: {
//                self.listingCollectionView.alpha = 1.0
//            })
            listingCollectionView.reloadData()

        }
    }
    
    @IBAction func filterAction(_ sender: Any) {
        let filterViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLFilterViewController") as! FLFilterViewController
        self.navigationController?.pushViewController(filterViewController, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
  
    

    func webCall(withParameter parameter: [String: Any]){
        let searchURL = ""
        FLWebService.getCall(toURL: searchURL, withParameter: parameter){ (isFinished,responseOptional) in
            print("searchCall  webSERVICE")
            if isFinished{
                
            
                print("searchCall success web")
                if let jsonResponse = responseOptional{
                    print("result",jsonResponse.status,jsonResponse.object as Any)
                    if jsonResponse.status{
                        // Web call success - back end updated - updating UI like table reload
//                        let searchResult = FLAssistant.extractJSON(ofSearchResult: jsonResponse)
                        FLProperty.extractJSON(ofSearchResult: jsonResponse, completion: { (success, searchResult) in
                            
                            
                            
                        })
//                        print("searchResult:",searchResult)
                    }else{
                        // Web call success - but back end updation error - show error message as alert
                    }
                }
            }else{
                    // Web service call failure - show alert check internet connection or try again
            }
        }
    }
}


extension FLBusinsessListingViewController{
    
    //MARK: API Call method
    func loadData(){
        var jsonData:[String:Any] = [:]
        /*
         user_id,
         search_key,
         location_latitude,
         location_longitude,
         category,
         subcategory,
         isfulltime:boolean,
         favourite:boolean
 */
        let parameters:[String:Any?] = ["user_id":"110",
                                       "search_key":"",
                                       "location_latitude":"",
                                       "location_longitude":"",
                                       "category":"",
                                       "subcategory":"",
                                       "isfulltime":false,
                                       "favourite":false]
        
            JSONParser.parseData(url: "http://fulfilla.dev.webcastle.in/api/service-list", parameters: ["user_id": "133"], httpMethod: HTTPMethod.post, completion: {isComplete, jsonParsed in
                //print(jsonParsed)
                if let jsonData = jsonParsed{
                   // print(jsonData)
                
                    let errorCode = jsonData["ErrorCode"]
                    let message = jsonData["Message"]
                    let tempDataCover = jsonData["Data"]
                    
                    
                    if let data = tempDataCover as? [[String:String]] {
                
                    for element in data{
                        var modelObject = BusinessListingModel(id: element["service_id"]!, title: element["service_title"]!, location: element["service_location"]!, rating: element["service_rating"]!, city: element["service_city"]!, reviewCount: element["service_review_count"]!, contactPhone: element["service_contact_phone"]!, contactMessage: element["service_contact_Message"]!, latitude: element["service_latitude"]!, longitude: element["service_longitude"]!, profileImageLink: element["service_profile_image"]!)
                        self.objectList.append(modelObject)
                    }
                    print(self.objectList.first)
                    self.resfreshAfterAPICall()
                    }
                
                }
                
            })
    }
    
    func loadDataFromDictionary(){
        let parameters:[String:Any?] = ["user_id":"110",
                                        "search_key":"",
                                        "location_latitude":"",
                                        "location_longitude":"",
                                        "category":"",
                                        "subcategory":"",
                                        "isfulltime":false,
                                        "favourite":false]
        JSONParser.parseData(url: "http://fulfilla.dev.webcastle.in/api/service-list", parameters: ["user_id": "133"], httpMethod: HTTPMethod.post, completion: {
            isComplete, jsonParsed in
            var tempObjectList:[BusinessListingModel] = []
            if let jsonData = jsonParsed{
                let data = jsonData["Data"] as? [[String:Any]]
                for item in data!{
                    let object = BusinessListingModel(with: item)
                    tempObjectList.append(object)
                }
                self.objectList = tempObjectList
                self.resfreshAfterAPICall()
            }
            
        })
    }
    
    
    func resfreshAfterAPICall(){
        
        listingTableView.reloadData()
        listingCollectionView.reloadData()
    }
}



// Mark: -  table view data source and delegates

extension FLBusinsessListingViewController: UITableViewDataSource, UITableViewDelegate {

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objectList.count
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
            cell.appearanceStyle = .threeDimenstional
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return 106
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        /// list view
            
        var listViewCell = tableView.dequeueReusableCell(withIdentifier: "FLListViewBusinessListingTableViewCell") as? FLListViewBusinessListingTableViewCell
            
        if listViewCell == nil {
               tableView.register(UINib(nibName: "FLListViewBusinessListingTableViewCell", bundle: nil), forCellReuseIdentifier: "FLListViewBusinessListingTableViewCell")
               print("registering tableviewcell")
               listViewCell = tableView.dequeueReusableCell(withIdentifier: "FLListViewBusinessListingTableViewCell") as? FLListViewBusinessListingTableViewCell
            }
        listViewCell?.delegate = self
        listViewCell?.setUpCell(with: objectList[indexPath.row])
        listViewCell?.contactNumber = objectList[indexPath.row].contactPhone
        print(objectList[indexPath.row].contactPhone)
        listViewCell?.messageContact = objectList[indexPath.row].contactMessage
        
        listViewCell?.selectionStyle = .none
        return listViewCell!
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailsBusinessViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLDetailsBusinessViewController") as! FLDetailsBusinessViewController
        
        self.navigationController?.pushViewController(detailsBusinessViewController, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell  = tableView.cellForRow(at: indexPath as IndexPath)
        cell!.contentView.backgroundColor = UIColor.FLtableSelection
    }
    
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell  = tableView.cellForRow(at: indexPath as IndexPath)
        cell!.contentView.backgroundColor = .clear
    }
    
}


extension FLBusinsessListingViewController: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return objectList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cell.appearanceStyle = .twoDimentionalYAxis
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // var gridCell = collectionView.dequeueReusableCell(withReuseIdentifier:"FLGridViewCollectionViewCell", for: indexPath as IndexPath) as? FLGridViewCollectionViewCell
        
        let gridCell = collectionView.dequeueReusableCell(withReuseIdentifier:"FLGridViewCollectionViewCell", for: indexPath as IndexPath) as? FLGridViewCollectionViewCell
        
        gridCell?.setUpCell(with: objectList[indexPath.row])
        gridCell?.contentView.layer.borderWidth = 1.0
        gridCell?.contentView.layer.borderColor = UIColor.clear.cgColor
        gridCell?.contentView.layer.masksToBounds = true
        
        gridCell?.layer.shadowColor = UIColor.lightGray.cgColor
        gridCell?.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        gridCell?.layer.shadowRadius = 2.0
        gridCell?.layer.shadowOpacity = 1.0
        gridCell?.layer.masksToBounds = false
        gridCell?.layer.shadowPath = UIBezierPath(rect: (gridCell?.bounds)!).cgPath
            
        
        return gridCell!
    }
    
    


    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(2 - 1))
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(2))
        return CGSize(width: size, height: size)

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let detailsBusinessViewController = FLAssistant.storyBoard.instantiateViewController(withIdentifier: "FLDetailsBusinessViewController") as! FLDetailsBusinessViewController
        
        self.navigationController?.pushViewController(detailsBusinessViewController, animated: true)
        
    }

}

