//
//  FLHomeCollectionViewCell.swift
//  Fulfilla
//
//  Created by Appzoc on 10/11/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit

class FLHomeCollectionViewCell: UICollectionViewCell {
    @IBOutlet var baseView: UIView!

    @IBOutlet var serviceImage: UIImageView!
    
    @IBOutlet var serviceName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
//        setUpInterface()
    }

    func setUpInterface(){
//        baseView.setShadow(withColor: UIColor.gray, opacity: 0.7, andRadius: 4)
    }
}
