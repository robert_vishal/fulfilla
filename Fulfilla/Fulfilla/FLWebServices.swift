//
//  WebServices.swift
//  Fulfilla
//
//  Created by Appzoc on 18/10/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import Foundation
import UIKit
import Alamofire


enum FLAppendAPI: String{
    case search = "properties"
}

// Mark: - Webservice receiving object from alamofire response

public struct FLJsonResponds{
    
    public var status : Bool = false
    public var object : Any? = nil
    
}

extension FLAssistant{
    
//    static var baseURL:String = "http://beta.propertyexpo.com/api/"
    
//    static var baseURL:String = "https://www.propertyexpo.com/api/"
    
    static var errorMessage: String = "Please check the network connection and try again."

    static let waitingTime: DispatchTimeInterval = DispatchTimeInterval.seconds(2)
    
    class func getParams(withKeys keysArray: NSArray, andValues valueArray: NSArray)->[String: AnyObject] {
        let params:[String:AnyObject] = ["key":"value" as AnyObject]
        
        return params
    }
    class func setParams(withKeys keysArray: NSArray, andValues valueArray: NSArray)->[String: AnyObject] {
        let params:[String:AnyObject] = ["key":"value" as AnyObject]
        
        return params
    }
    
    class func extractJSON(ofSearchResult respondsObject: FLJsonResponds?, completion: @escaping (Bool, FLProperty) -> Void){
        
        let FLPropertyObject = FLProperty()
        
        enum JsonKeys: String {
            case kPropertyId = "property_id"
            case kPropertyTitle = ""
            case kPropertyReferenceNo = "a"
            case kPostedDate = "b"
            case kCity = "c"
            case kLocation = "d"
            case kLatitude = "e"
            case kLongitude = "f"
            case kEmail = "g"
            case kPhone = "h"
            case kMessage = "i"
            case kPrice = "j"
            case kPictures = "k"
            case kIconPicture = "l"
            case kVideos = "m"
            case kPhotoCount = "n"
            case kArea = "o"
            case kRatingCount = "p"
            case kReviewCount = "q"
            case kBathroomCount = "r"
            case kBedroomCount = "s"
        }
        
        if let jsonResult = respondsObject{
            print("FLProperty : jsonResult : ",jsonResult)
            
            if let resultObject: NSDictionary = jsonResult.object as? NSDictionary {
                
                FLPropertyObject.propertyId = FLUnwraper.unwrap(string: resultObject[JsonKeys.kPropertyId.rawValue] as? String)
                
                FLPropertyObject.propertyTitle = FLUnwraper.unwrap(string: resultObject[JsonKeys.kPropertyTitle.rawValue] as? String)
                
                FLPropertyObject.propertyReferenceNo = FLUnwraper.unwrap(string: resultObject[JsonKeys.kPropertyReferenceNo.rawValue] as? String)
                
                FLPropertyObject.postedDate = FLUnwraper.unwrap(string: resultObject[JsonKeys.kPostedDate.rawValue] as? String)
                
                FLPropertyObject.city = FLUnwraper.unwrap(string: resultObject[kSecPropertyTypeTitle] as? String)
                
                FLPropertyObject.location = FLUnwraper.unwrap(string: resultObject[JsonKeys.kLocation.rawValue] as? String)
                
                FLPropertyObject.latitude = FLUnwraper.unwrap(string: resultObject[JsonKeys.kLatitude.rawValue] as? String)
                
                FLPropertyObject.longitude = FLUnwraper.unwrap(string: resultObject[JsonKeys.kLongitude.rawValue] as? String)
                
                FLPropertyObject.email = FLUnwraper.unwrap(string: resultObject[JsonKeys.kEmail.rawValue] as? String)
                
                FLPropertyObject.phone = FLUnwraper.unwrap(string: resultObject[JsonKeys.kPhone.rawValue] as? String)
                
                FLPropertyObject.message = FLUnwraper.unwrap(string: resultObject[JsonKeys.kMessage.rawValue] as? String)
                
                FLPropertyObject.price = FLUnwraper.unwrap(string: resultObject[JsonKeys.kPrice.rawValue] as? String)
                
                FLPropertyObject.area = FLUnwraper.unwrap(string: resultObject[JsonKeys.kArea.rawValue] as? String)
                
                FLPropertyObject.iconPicture = FLUnwraper.unwrap(string: resultObject[JsonKeys.kIconPicture.rawValue] as? String)
                
                FLPropertyObject.ratingCount = FLUnwraper.unwrap(integer: resultObject[JsonKeys.kRatingCount.rawValue] as? Int)
                
                FLPropertyObject.reviewCount = FLUnwraper.unwrap(integer: resultObject[JsonKeys.kReviewCount.rawValue] as? Int)
                
                FLPropertyObject.bedroomCount = FLUnwraper.unwrap(string: resultObject[JsonKeys.kBedroomCount.rawValue] as? String)
                
                FLPropertyObject.bathroomCount = FLUnwraper.unwrap(string: resultObject[JsonKeys.kBathroomCount.rawValue] as? String)
                
                FLPropertyObject.pictures = resultObject[JsonKeys.kPictures.rawValue]  as! [String]
                
                FLPropertyObject.videos =  resultObject[JsonKeys.kVideos.rawValue]  as! [String]
                completion(true, FLPropertyObject)
            }else{
                completion(false, FLPropertyObject)
            }
        }else{
            completion(false, FLPropertyObject)
        }
        
    }
    
    
    
   
}





public class FLWebService{
    
    
    private init(){
        
    }
    
    
    // Mark: - Post method
    
    class func postCall(toURL callingURL: String, withParameter parameters: [String: Any], completion: @escaping (Bool,FLJsonResponds?) -> Void) {
        
        var FLRespondsObjectData = FLJsonResponds()
        print("PostCall","URL:",callingURL as Any,"Parameter:",parameters as Any)
        
        Alamofire.request(callingURL, method: .post,
                          parameters: parameters,
                          encoding: URLEncoding.default, headers: nil).responseJSON {                                                                                                                                    (response:DataResponse<Any>) in
                            
                            switch(response.result){
                            case .success(_):
                                if response.result.value != nil{
                                    
                                    if let respondsDictionary:NSDictionary = response.result.value as? NSDictionary {
                                        
                                        print("PostCall :  success:",respondsDictionary)
                                        if respondsDictionary["ErrorCode"] as! Int == 1 {
                                            
                                            print("PostCall: ErrorMessage : ",respondsDictionary["Message"] as Any)
                                            FLRespondsObjectData.status = false
                                            FLRespondsObjectData.object = respondsDictionary["Message"]
                                            completion(true,FLRespondsObjectData)
                                            
                                        }else{
                                            
                                            print("PostCall: Data: ",respondsDictionary)
                                            FLRespondsObjectData.status = true
                                            FLRespondsObjectData.object = respondsDictionary
                                            completion(true, FLRespondsObjectData)
                                        }
                                        
                                    }
                                    
                                }
                                break
                            case .failure(_):
                                print("PostCall : failure",response.result.error as Any)
                                completion(false,nil)
                                break
                                
                            }
                    }

    
    }
    
    
    
    // Mark: - Get method
    
    class func getCall(toURL callingURL: String, withParameter parameters: [String: Any], completion: @escaping (Bool,FLJsonResponds?) -> Void) {
        
        var FLRespondsObjectData = FLJsonResponds()
        print("getCall","URL:",callingURL as Any,"Parameter:",parameters as Any)
        
        Alamofire.request(callingURL, method: .post,
                          parameters: parameters,
                          encoding: URLEncoding.default, headers: nil).responseJSON {                                                                                                                                    (response:DataResponse<Any>) in
                            
                            switch(response.result){
                            case .success(_):
                                if response.result.value != nil{
                                    
                                    
                                    if let respondsDictionary:NSDictionary = response.result.value as? NSDictionary {
                                        
                                        print("getCall :  success:",respondsDictionary)
                                        if respondsDictionary["ErrorCode"] as! Int == 1 {
                                            
                                            print("getCall: ErrorMessage : ",respondsDictionary["Message"] as Any)
                                            FLRespondsObjectData.status = false
                                            FLRespondsObjectData.object = respondsDictionary["Message"]
                                            completion(true,FLRespondsObjectData)
                                            
                                        }else{
                                            
                                            print("getCall: Data: ",respondsDictionary)
                                            FLRespondsObjectData.status = true
                                            FLRespondsObjectData.object = respondsDictionary["Data"]
                                            completion(true, FLRespondsObjectData)
                                        }
                                        
                                    }
                                    
                                }
                                break
                            case .failure(_):
                                print("getCall : failure",response.result.error as Any)
                                completion(false,nil)
                                break
                                
                            }
                    }
        
    }
    
    
}


//public class FLWebService{
//
//    func sample(){
//
//
//    }
//}
//    class func searchCall(withParameter params: [String:AnyObject], completionHandler completion: @escaping (Bool,FLSearchResult?) -> Void) {
////        let object: FLRespondsObject = FLRespondsObject()
//
//        let searchURL = FLAssistant.baseURL + FLAppendAPI.search.rawValue
//
//        let parameter = ["current_page" : 0, "user_id" : 568,"type" : 1,"limit":400 ,"sort" : ""] as [String : Any]
//
//        let a =
//
////        let parameter = params as [String: Any]
//        let searchResultObject = FLSearchResult()
//        let messageString = "Unable to process.Please check the network connection and search again."
//
//        FLMakeWebCall.getCall(toURL: searchURL, withParameter: parameter){ (isFinished,resultObjectOptional) in
//            print("searchCall  webSERVICE")
//
//            if isFinished{
//                print("searchCall success web")
//                if let resultObject = resultObjectOptional{
//
//                    print("result",resultObject.status,resultObject.object as Any)
//                    if resultObject.status{
//
//                        searchResultObject.propertyName = resultObject.object as! String
//                        completion(true, searchResultObject)
//                    }else{
//                        searchResultObject.propertyName = resultObject.object as! String
//                        completion(false, searchResultObject)
//                    }
//                }
//
//            }else{
//                searchResultObject.message = messageString
//                completion(false,searchResultObject)
//            }
//
//        }
//
//}

//    class func loginCall(withParameter params: String, completionHandler completion: @escaping (Bool,FLJsonResponds?) -> Void) {
//        let object: FLJsonResponds = FLJsonResponds()
//
//
//
//
//        completion(true, object)
//    }}


