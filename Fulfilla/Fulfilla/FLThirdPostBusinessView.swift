//
//  FLThirdPostBusinessView.swift
//  Fulfilla
//
//  Created by Appzoc on 29/11/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit

class FLThirdPostBusinessView: UIView {

    class func instanceFromNib() -> UIView {
        return UINib(nibName: "FLThirdPostBusinessView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }

}
