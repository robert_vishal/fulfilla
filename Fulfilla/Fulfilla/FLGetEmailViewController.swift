//
//  FLGetEmailViewController.swift
//  Fulfilla
//
//  Created by Appzoc on 20/12/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit

class FLGetEmailViewController: UIViewController {

    @IBOutlet var containerView: UIView!
    @IBOutlet var emailContainer: UIView!
    @IBOutlet var bodyView: UIView!
    @IBOutlet var emailTextField: UITextField!
    
    var emailString: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        FLAnimations.popUpAnimation(applyToView: containerView, animationStyle: .zoomOut)
        setUpInterface()

    }
    
    

    func setUpInterface(){
        bodyView.setFLShadow()
        //self.view.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.1)
        emailContainer.layer.borderWidth = 1
        emailContainer.layer.borderColor = UIColor.lightGray.cgColor
        emailContainer.layer.cornerRadius = 4
        emailContainer.layer.masksToBounds = true
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     vidyab+119@webcastle.in
     password
     user_id 133
     arunjose+14@webcastle.in
 
 */
    
    @IBAction func submitAction(_ sender: Any) {
        if let emailEntry = emailTextField.text{
            if !FLUnwraper.isValid(email: emailEntry){
                FLAnimations.shakeView(view: emailTextField, isHorizontally: true)
            }else{
                var dataResponse:[String:Any] = [:]
                    JSONParser.parseData(url: "http://fulfilla.dev.webcastle.in/api/forgot-password", parameters: ["email_id":"\(emailEntry)"], completion: {isComplete, jsonData in
                        if let dataResponse = jsonData{
                            print(dataResponse)
                            self.dismiss(animated: true, completion: nil)
                        }
                    
                    })
                
            }
        }
    }
    

    @IBOutlet var closeAction: UIButton!


}



// Mark: - Handling textfield delegates

extension FLGetEmailViewController: UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        emailString = FLUnwraper.unwrap(string: emailTextField.text)

    }
}
