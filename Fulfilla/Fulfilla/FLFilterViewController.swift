//
//  FLFilterViewController.swift
//  Fulfilla
//
//  Created by Appzoc on 25/10/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit

class FLFilterViewController: UIViewController {

    @IBOutlet var searchContainerView: UIView!
    
    @IBOutlet var searchTextField: UITextField!
    @IBOutlet var listingTable: UITableView!
    @IBOutlet var listingTableContainer: UIView!
    @IBOutlet var searchTextFieldContainer: UIView!
    @IBOutlet var clearButton: UIButton!
    @IBOutlet var applyFilterButton: UIButton!
    
    @IBOutlet weak var categorySelectButton: UIButton!
    
    @IBOutlet weak var locationSelectButton: UIButton!
    
    
    
    //MARK:- Properties
    var categorySelected:Bool = true
    var locationSelected:Bool = false
    var fullTimeServicesSelected:Bool = false
    
    
    
    let locationDataSource = ["Medical Shops", "Hospital & Clinics", "Dental Clinic", "Ayurvedic Medicines", "Medical Supplies & Equipments","Homeopathy","Dental","Medical Shops2", "Hospital & Clinics2", "Dental Clinic2", "Ayurvedic Medicines2", "Medical Supplies & Equipments2","Homeopathy2","Dental2"]
    
    let cityDataSource = ["Medical Shops", "Hospital & Clinics", "Dental Clinic", "Ayurvedic Medicines", "Medical Supplies & Equipments","Homeopathy","Dental","Medical Shops2", "Hospital & Clinics2", "Dental Clinic2", "Ayurvedic Medicines2", "Medical Supplies & Equipments2","Homeopathy2","Dental2"]
    
    let categoryDataSource = ["Medical Shops", "Hospital & Clinics", "Dental Clinic", "Ayurvedic Medicines", "Medical Supplies & Equipments","Homeopathy","Dental","Medical Shops2", "Hospital & Clinics2", "Dental Clinic2", "Ayurvedic Medicines2", "Medical Supplies & Equipments2","Homeopathy2","Dental2"]
    
    let subCategoryDataSource = ["Medical Shops", "Hospital & Clinics", "Dental Clinic", "Ayurvedic Medicines", "Medical Supplies & Equipments","Homeopathy","Dental","Medical Shops2", "Hospital & Clinics2", "Dental Clinic2", "Ayurvedic Medicines2", "Medical Supplies & Equipments2","Homeopathy2","Dental2"]
    
    var locationSelectedIndexPaths: [IndexPath] = []
    var citySelectedIndexPaths: [IndexPath] = []
    var categorySelectedIndexPaths: [IndexPath] = []
    var subCategorySelectedIndexPaths: [IndexPath] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUserInterface()

    }

    func setUpUserInterface(){
/*
        /// setting counting label properties
        locationNotification.layer.cornerRadius = locationNotification.frame.width/2
        locationNotification.layer.masksToBounds = true
        locationNotification.text = ""
        locationNotification.isHidden = true

    //    cityNotification.layer.cornerRadius = cityNotification.frame.width/2
      //  cityNotification.layer.masksToBounds = true
        //cityNotification.text = ""
        //cityNotification.isHidden = true

        categoryNotification.layer.cornerRadius = categoryNotification.frame.width/2
        categoryNotification.layer.masksToBounds = true
        categoryNotification.text = ""
        categoryNotification.isHidden = true

        subCategoryNotification.layer.cornerRadius = subCategoryNotification.frame.width/2
        subCategoryNotification.layer.masksToBounds = true
        subCategoryNotification.text = ""
        subCategoryNotification.isHidden = true

        
        locationButton.backgroundColor = UIColor.clear
        cityButton.backgroundColor = UIColor.clear
        
        locationImageView.image = #imageLiteral(resourceName: "locationwhite")
        cityImageView.image = #imageLiteral(resourceName: "citygrey")
        
        /// isSelected property used for button interface and image
        locationButton.isSelected = true
        cityButton.isSelected = false
        categoryButton.isSelected = false
        subCategoryButton.isSelected = false
        
        locationButtonAction(Any.self)
        
        listingTable.clipsToBounds = true

        listingTableContainer.setShadow(withColor: UIColor.black, opacity: 0.2, andShadowRadius: 3)
        searchContainerView.setShadow(withColor: UIColor.black, opacity: 0.2, andShadowRadius: 3)
        searchTextFieldContainer.setFLBorder()
        */

    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func categorySelected(_ sender: UIButton) {
        categorySelected = true
        locationSelected = false
        locationSelectButton.setImage(UIImage(named: "radiounfilled"), for: [])
        categorySelectButton.setImage(UIImage(named: "radiofilled"), for: [])
    }
    
    @IBAction func locationSelected(_ sender: UIButton) {
        categorySelected = false
        locationSelected = true
        locationSelectButton.setImage(UIImage(named: "radiofilled"), for: [])
        categorySelectButton.setImage(UIImage(named: "radiounfilled"), for: [])    }
    
    @IBAction func fullTimeServicesSelected(_ sender: UIButton) {
        if fullTimeServicesSelected {
            fullTimeServicesSelected = false
            sender.setImage(UIImage(named: "radiounfilled"), for: [])
        }else{
            fullTimeServicesSelected = true
            sender.setImage(UIImage(named: "radiofilled"), for: [])
        }
        
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func changeButtonsInterface(){
      /*
        let buttonArray:[UIButton] = [locationButton,cityButton,categoryButton,subCategoryButton]
        let imageViewArray:[UIImageView] = [locationImageView,cityImageView,categoryImageView,subCategoryImageView]
        let whiteImageArray:[UIImage] = [#imageLiteral(resourceName: "locationwhite"),#imageLiteral(resourceName: "citywhite"),#imageLiteral(resourceName: "categorywhite"),#imageLiteral(resourceName: "subcategorywhite")]
        let grayImageArray: [UIImage] = [#imageLiteral(resourceName: "locationgrey"),#imageLiteral(resourceName: "citygrey"),#imageLiteral(resourceName: "categorygrey"),#imageLiteral(resourceName: "subcategorygrey")]
        var incrementor:Int = 0
        
        for button in buttonArray{
            
            if button.isSelected{
                button.backgroundColor = UIColor.gray
                imageViewArray[incrementor].image = whiteImageArray[incrementor]
                button.isSelected = false
            }else{
                button.backgroundColor = UIColor.clear
                imageViewArray[incrementor].image = grayImageArray[incrementor]
                button.isSelected = false
            }
            incrementor += 1
        }
 */
    }
    
    
    
    /*
    func changeVisibility(ofLabel selectedLabel: UILabel){
        
        if selectedLabel == locationNotification{
            if locationNotification.text != "0"{
                locationNotification.isHidden = false
            }else{
                locationNotification.isHidden = true
            }
           // cityNotification.isHidden = true
            categoryNotification.isHidden = true
            subCategoryNotification.isHidden = true
            
        }else if selectedLabel == cityNotification{
            if cityNotification.text != "0"{
                cityNotification.isHidden = false
            }else{
                cityNotification.isHidden = true
                
            }
            locationNotification.isHidden = true
            categoryNotification.isHidden = true
            subCategoryNotification.isHidden = true
            
        }else if selectedLabel == categoryNotification{
            if categoryNotification.text != "0"{
                categoryNotification.isHidden = false
            }else{
                categoryNotification.isHidden = true
                
            }
            locationNotification.isHidden = true
            cityNotification.isHidden = true
            subCategoryNotification.isHidden = true
            
        }else{
            if subCategoryNotification.text != "0"{
                subCategoryNotification.isHidden = false
            }else{
                subCategoryNotification.isHidden = true
                
            }
            locationNotification.isHidden = true
            cityNotification.isHidden = true
            categoryNotification.isHidden = true
            
        }
        
        
    }
 
    
    func setNotification(withCount count: Int, andTableTag tag: Int){
        
        if count != 0{
            if tag == 0{
                self.locationNotification.isHidden = false
                self.locationNotification.text = String(count)
            }else if tag == 1{
                self.cityNotification.isHidden = false
                self.cityNotification.text = String(count)
            }else if tag == 2{
                self.categoryNotification.isHidden = false
                self.categoryNotification.text = String(count)
            }else{
                self.subCategoryNotification.isHidden = false
                self.subCategoryNotification.text = String(count)
            }
        }
        
    }
 */

  
}



// Mark: - Table view data source and delegates

extension FLFilterViewController: UITableViewDataSource, UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView.tag == 0{
            return locationDataSource.count
        }else if tableView.tag == 1 {
            return cityDataSource.count
        }else if tableView.tag == 2{
            return categoryDataSource.count
        }else{
            return subCategoryDataSource.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0

        UIView.animate(withDuration: 0.25) {
            cell.alpha = 1
            
        }

        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FLFilterTableViewCell", for: indexPath) as! FLFilterTableViewCell
        cell.selectionStyle = .none
        
        
        if tableView.tag == 0{
            cell.listingLabel.text = locationDataSource[indexPath.row]
            if locationSelectedIndexPaths.contains(indexPath){
                cell.radioButtonImage.image = #imageLiteral(resourceName: "radiofilled")
            }else{
                cell.radioButtonImage.image = #imageLiteral(resourceName: "radiounfilled")
            }
        }else if tableView.tag == 1 {
            cell.listingLabel.text = cityDataSource[indexPath.row]
            cell.listingLabel.text = locationDataSource[indexPath.row]
            if citySelectedIndexPaths.contains(indexPath){
                cell.radioButtonImage.image = #imageLiteral(resourceName: "radiofilled")
            }else{
                cell.radioButtonImage.image = #imageLiteral(resourceName: "radiounfilled")
            }

        }else if tableView.tag == 2{
            cell.listingLabel.text = categoryDataSource[indexPath.row]
            cell.listingLabel.text = locationDataSource[indexPath.row]
            if categorySelectedIndexPaths.contains(indexPath){
                cell.radioButtonImage.image = #imageLiteral(resourceName: "radiofilled")
            }else{
                cell.radioButtonImage.image = #imageLiteral(resourceName: "radiounfilled")
            }

        }else{
            cell.listingLabel.text = subCategoryDataSource[indexPath.row]
            cell.listingLabel.text = locationDataSource[indexPath.row]
            if subCategorySelectedIndexPaths.contains(indexPath){
                cell.radioButtonImage.image = #imageLiteral(resourceName: "radiofilled")
            }else{
                cell.radioButtonImage.image = #imageLiteral(resourceName: "radiounfilled")
            }

        }
        
        
        
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! FLFilterTableViewCell
        
        if tableView.tag == 0{
            if !self.locationSelectedIndexPaths.contains(indexPath){
                self.locationSelectedIndexPaths.append(indexPath)
                cell.radioButtonImage.image = #imageLiteral(resourceName: "radiofilled")
                //setNotification(withCount: locationSelectedIndexPaths.count, andTableTag: 0)
            }
        }else if tableView.tag == 1 {
            if !self.citySelectedIndexPaths.contains(indexPath){
                self.citySelectedIndexPaths.append(indexPath)
                cell.radioButtonImage.image = #imageLiteral(resourceName: "radiofilled")
                //setNotification(withCount: citySelectedIndexPaths.count, andTableTag: 1)
            }
        }else if tableView.tag == 2{
            if !self.categorySelectedIndexPaths.contains(indexPath){
                self.categorySelectedIndexPaths.append(indexPath)
                cell.radioButtonImage.image = #imageLiteral(resourceName: "radiofilled")
              //  setNotification(withCount: categorySelectedIndexPaths.count, andTableTag: 2)
            }
        }else{
            if !self.subCategorySelectedIndexPaths.contains(indexPath){
                self.subCategorySelectedIndexPaths.append(indexPath)
                cell.radioButtonImage.image = #imageLiteral(resourceName: "radiofilled")
               // setNotification(withCount: subCategorySelectedIndexPaths.count, andTableTag: 3)
            }
        }
        
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! FLFilterTableViewCell
        
        if tableView.tag == 0{
            if self.locationSelectedIndexPaths.contains(indexPath){
                cell.radioButtonImage.image = #imageLiteral(resourceName: "radiounfilled")
                self.locationSelectedIndexPaths.remove(at: self.locationSelectedIndexPaths.index(of: indexPath)!)
                //setNotification(withCount: locationSelectedIndexPaths.count, andTableTag: 0)
                
            }
        }else if tableView.tag == 1 {
            if self.citySelectedIndexPaths.contains(indexPath){
                cell.radioButtonImage.image = #imageLiteral(resourceName: "radiounfilled")
                self.citySelectedIndexPaths.remove(at: self.citySelectedIndexPaths.index(of: indexPath)!)
                //setNotification(withCount: citySelectedIndexPaths.count, andTableTag: 1)
                
            }
        }else if tableView.tag == 2{
            if self.categorySelectedIndexPaths.contains(indexPath){
                cell.radioButtonImage.image = #imageLiteral(resourceName: "radiounfilled")
                self.categorySelectedIndexPaths.remove(at: self.categorySelectedIndexPaths.index(of: indexPath)!)
                //setNotification(withCount: categorySelectedIndexPaths.count, andTableTag: 2)
                
            }
        }else{
            if self.subCategorySelectedIndexPaths.contains(indexPath){
                cell.radioButtonImage.image = #imageLiteral(resourceName: "radiounfilled")
                self.subCategorySelectedIndexPaths.remove(at: self.subCategorySelectedIndexPaths.index(of: indexPath)!)
                //setNotification(withCount: subCategorySelectedIndexPaths.count, andTableTag: 3)
                
            }
        }
       
    }
    
    
}
