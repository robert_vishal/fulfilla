//
//  FLThirdPostBusinessViewController.swift
//  Fulfilla
//
//  Created by Appzoc on 17/11/17.
//  Copyright © 2017 Appzoc. All rights reserved.
//

import UIKit

class FLThirdPostBusinessViewController: UIViewController {

    @IBOutlet var phoneView: UIView!
    @IBOutlet var mobileView: UIView!
    @IBOutlet var emailView: UIView!
    @IBOutlet var websiteView: UIView!
    @IBOutlet var locationView: UIView!
    @IBOutlet var cityView: UIView!
    @IBOutlet var areaView: UIView!
    @IBOutlet var addressTextView: UITextView!
    @IBOutlet var facebookView: UIView!
    @IBOutlet var twitterView: UIView!
    @IBOutlet var linkedInView: UIView!
    @IBOutlet var youtubeView: UIView!
    
    @IBOutlet var saveButton: UIButton!
    @IBOutlet var submitButton: UIButton!
    @IBOutlet var phoneTextField: UITextField!
    @IBOutlet var mobileTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var websiteTextField: UITextField!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var cityLabel: UILabel!
    @IBOutlet var areaTextField: UITextField!
    @IBOutlet var facebookTextField: UITextField!
    @IBOutlet var twitterTextField: UITextField!
    @IBOutlet var linkedInTextField: UITextField!
    @IBOutlet var youtubeTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        setUpInterface()
    }

    
    func setUpInterface(){
        phoneView.setFLBorder()
        mobileView.setFLBorder()
        emailView.setFLBorder()
        websiteView.setFLBorder()
        locationView.setFLBorder()
        cityView.setFLBorder()
        areaView.setFLBorder()
        addressTextView.setFLBorder()
        facebookView.setFLBorder()
        twitterView.setFLBorder()
        linkedInView.setFLBorder()
        youtubeView.setFLBorder()
        
        saveButton.setShadow(withColor: UIColor.gray, opacity: 0.7, andShadowRadius: 3)
        submitButton.setShadow(withColor: UIColor.gray, opacity: 0.7, andShadowRadius: 3)

    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func selectCityAction(_ sender: Any) {
    }
    
    @IBAction func selectLocationAction(_ sender: Any) {
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
